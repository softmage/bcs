<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

if (isset($_POST['add_employee'])) {

  createEmployee();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

  <title>Add Employee | <?php echo APPNAME; ?> </title>

  <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <?php include_once 'includes/navigation.php'; ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <br />

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tile">
              <div class="x_title">
                <h2>Add New Employee</h2>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="col-md-12">
                  <!-- //Display Feedback Message -->
                  <div class="col-md-6"> <?php echo $msg->display(); ?></div>
                  
                </div>
                <div class="clearfix"></div>
                <div class="col-md-9"></div>
                  <form class="addEmployee form-horizontal form-label-left input_mask" method="POST" id="addEmployee" name="addEmployee" name="addEmployee">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Personal Details</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Contact Details</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Employment Details</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Remuneration Details</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="staff_no">Staff No :</label>
                              <input type="text" class="form-control" id="staff_no" name="staff_no" required="">
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="department">Last Name :</label>
                              <input type="text" class="form-control" id="lastname" name="lastname" required="">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="designation">First Name :</label>
                              <input type="text" class="form-control" id="firstname" name="firstname" required="">
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="emp_name">Other Name :</label>
                              <input type="text" class="form-control" id="other_name" name="other_name">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="gender">Gender :</label>
                              <select id="gender" class="form-control" name="gender" required="">
                                <option value="">Choose..</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="emp_name">Nationality:</label>
                              <input type="text" class="form-control" id="nationality" name="nationality" required="">
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="dob">Date of Birth:</label>
                              <input type="date" class="form-control" id="dob" name="dob" required="">
                            </div>
                          </div>

                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="marital_status">Marital Status:</label>
                              <select id="marital_status" class="form-control" name="marital_status" required="">
                                <option value="">Choose..</option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Divorced</option>
                                <option>Windowed</option>
                              </select>
                            </div>
                          </div>


                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="dob">No of Kids :</label>
                              <select id="no_of_kids" class="form-control" name="no_of_kids" required="">
                                <option value="">Choose..</option>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="qualification">Qualification :</label>
                              <input type="text" name="qualification" class="form-control" id="qualification" required>
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="discipline">Discipline :</label>
                              <input type="text" name="discipline" class="form-control" id="discipline" required>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="other_qual">Other Qualifications :</label>
                              <textarea name="other_qual" id="other_qual" class="form-control" rows="3" required></textarea>
                            </div>
                          </div>
                        </div>

                        <!-- Panel 2 -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="private_email">Email Address :</label>
                              <input type="text" name="private_email" id="private_email" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="private_phone">Private Phone No :</label>
                              <input type="text" name="private_phone" id="private_phone" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="home_address">Contact Address :</label>
                              <textarea name="home_address" id="home_address" class="form-control" rows="3" required></textarea>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="next_of_kin">Next of Kin :</label>
                              <input type="text" name="next_of_kin" id="next_of_kin" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="kin_relationship">Relationship :</label>
                              <input type="text" name="kin_relationship" id="kin_relationship" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="nok_phone">Next of Kin Phone :</label>
                              <input type="text" name="nok_phone" id="nok_phone" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="nok_address">Next of Kin Address :</label>
                              <textarea name="nok_address" id="nok_address" class="form-control" rows="3" required></textarea>
                            </div>
                          </div>
                        </div>
                        <!-- Panel 3 -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="hire_date">Date Employed:</label>
                              <input type="date" class="form-control" id="hire_date" name="hire_date" required="">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="official_email">Official Email:</label>
                              <input type="email" class="form-control" id="official_email" name="official_email">
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="cug_line">CUG Line:</label>
                              <input type="text" class="form-control" id="cug_line" name="cug_line">
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="branch">Branch:</label>
                              <select id="branch" class="form-control" name="branch" required="">
                                <option value="">Choose..</option>
                                <?php getallBranches(); ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="dob">Department:</label>
                              <select id="department" class="form-control" name="department" required="">
                                <option value="">Choose..</option>
                                <?php getallDepartments(); ?>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="job_level">Job Level:</label>
                              <select id="job_level" class="form-control" name="job_level" required="">
                                <option value="">Choose..</option>
                                <option>Level 1</option>
                                <option>Level 2</option>
                                <option>Level 3</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="designation">Designation:</label>
                              <select id="designation" class="form-control" name="designation" required="">
                                <option value="">Choose..</option>
                                <?php getallDesignation(); ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-4 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="isline_mgr ">Is LineMgr?:</label>
                              <select id="isline_mgr" class="form-control" name="isline_mgr" required="">
                                <option value="">Choose..</option>
                                <option>Yes</option>
                                <option>No</option>>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="employment_type">Employment Type:</label>
                              <select id="employment_type" class="form-control" name="employment_type" required="">
                                <option value="">Choose..</option>
                                <option>Contract</option>
                                <option>Intern/Trainee</option>
                                <option>Full Time</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 form-group form-group has-feedback">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label for="line_mgr">Line Manager:</label>
                              <select id="line_mgr" class="form-control" name="line_mgr" required="">
                                <option value="">Choose..</option>
                                <?php getLineManagers(); ?>
                              </select>
                            </div>
                          </div>
                        </div>
                        <!-- Panel 4 -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="annual_sal">Annual Gross Salary *:</label>
                              <input type="text" class="form-control" id="annual_sal" name="annual_sal" placeholder="Enter employee's Gross Anual Salary here" required="">
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="tin">Tax Identification Number (TIN) :</label>
                              <input type="text" class="form-control" id="tin" name="tin" placeholder="Enter TAX ID here">
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="bank_name">Bank *:</label>
                              <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Enter nae of Bank here" required="">
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="account_no">Account Number *:</label>
                              <input type="number" class="form-control" id="account_no" name="account_no" placeholder="Enter 10digits NUBAN number" required="">
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="pfa">Pension Fund Administrator :</label>
                              <select class="form-control" id="pfa" name="pfa" placeholder="Enter Pension Fund Administrator here" required="">
                                <option value="">Choose..</option>
                                <?php getPFA(); ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 form-group form-group has-feedback">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                              <label for="pfa_pin">Pension ID :</label>
                              <input type="text" class="form-control" id="pfa_pin" name="pfa_pin" placeholder="Enter Pension ID here" required="">
                            </div>
                          </div>

                          <div class="col-md-12 col-xs-12 form-group has-feedback">
                            <hr>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-12 col-xs-12 col-md-offset-3">
                        <button type="submit" name="add_employee" class="btn btn-primary">Add
                          Employee</button>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
        <div class="pull-right">
          <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
        </div>
        <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
    </div>
  </div>

  <?php include 'includes/scripts.php'; ?>

  <script>
    $(function() {
      // Initialize form validation on the registration form.
      // It has the name attribute "registration"
      $("#addEmployee").validate({
        ignore: "",
        // $("form[name='addEmployee']").validate({ ignore: "" 
        // Specify validation rules
        rules: {
          firstname: {
            required: true
          },
          lastname: {
            required: true
          },
          nationality: {
            required: true
          },
          dob: {
            required: true
          },
          gender: {
            required: true
          },
          marital_status: {
            required: true
          },
          annual_sal: {
            required: true,
            number: true
          },
          account_no: {
            required: true,
            number: true
          },
          cug_line: {
            required: true,
            number: true
          },
          private_phone: {
            required: true,
            number: true
          }
        },

        // Specify validation error messages
        messages: {
          firstname: "Please enter your firstname",
          lastname: "Please enter your lastname",
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
  </script>
  <script>
    new AutoNumeric('#annual_sal');
  </script>

</body>

</html>