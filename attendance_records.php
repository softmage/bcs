<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

if ($userlevel == 1) {
   $filter = "emp_id = $user_id";
} elseif ($userlevel == 100) {
   $filter = "branch = '$userbranch'";
} else {
   $filter = "1 = 1";
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Attendance Summary| <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Attendance Records</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div class="row"></div>
                     </div>

                     <div class="col-md-12">
                        <div class="col-md-6">
                           <!-- //Display Feedback Message -->
                           <?php echo $msg->display(); ?>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th> </th>
                                 <th>ID</td>
                                 <th>Branch</td>
                                 <th>Payroll Period</td>
                                 <th>Month</td>
                                 <th style='display:none;'>EmpID</td>
                                 <th>Employee</td>
                                 <th>ClockIn Time</td>
                                 <th>Date</td>
                                 <th style='display:none;'>Lat</td>
                                 <th style='display:none;'>Longt</td>
                                 <th>Address</td>
                                 <th>Status</td>
                                 <th style='display:none;'>Period ID</td>
                                 <th style='display:none;'>ClockIn Stamp</td>
                                 <th>Entry Type</td>
                                 <th style='display:none;'>Punct Count</td>
                                 <th style='display:none;'>Late Count</td>
                                 <th style='display:none;'>Absent Count</td>
                                 <th>Type</td>
                                 <th>Clockout Time</td>
                                 <th style='display:none;'>ClockOut Lat</td>
                                 <th style='display:none;'>ClockOut Longt</td>
                                 <th>ClockOut Address</td>
                                 <th style='display:none;'>ClockOut Stamp</td>
                                 <th>ClockIn Source</td>

                           </thead>

                           <tbody id="records_list">
                              <?php
                              listAttendanceRecords($filter);
                              ?>
                           </tbody>
                        </table>
                     </div>


                     <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-md">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                 </button>
                                 <h4 class="modal-title" id="myModalLabel2">View Record</h4>
                              </div>
                              <div class="modal-body">

                                 <form class="viewrecords form-horizontal form-label-left input_mask" method="POST" id="viewrecords" name="viewrecords" name="viewrecords">
                                    <table class="table table-bordered table-striped">
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">ID <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="id" name="id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payroll_period">PayrollPeriod <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="payroll_period" name="payroll_period" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="month_period">Month <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="month_period" name="month_period" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee">Employee <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="employee" name="employee" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date">Date <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="date" name="date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="clockin_time">Time <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="clockin_time" name="clockin_time" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <textarea name="address" id="address" class="form-control col-md-7 col-xs-12" rows="1" required readonly></textarea>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="source">Source <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="source" name="source" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                    </table>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         $("#addEmployee").validate({
            ignore: "",
            rules: {
               firstname: {
                  required: true
               },
               lastname: {
                  required: true
               },
               nationality: {
                  required: true
               },
               dob: {
                  required: true
               },
               gender: {
                  required: true
               },
               marital_status: {
                  required: true
               }

            },

            // Specify validation error messages
            messages: {
               firstname: "Please enter your firstname",
               lastname: "Please enter your lastname",
            },
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');

         $.ajax({

            url: 'records.php?action=attendance&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {

               $('.viewrecords #id').val(values.id);
               $('.viewrecords #branch').val(values.branch);
               $('.viewrecords #payroll_period').val(values.payroll_period);
               $('.viewrecords #month_period').val(values.month_period);
               $('.viewrecords #emp_id').val(values.emp_id);
               $('.viewrecords #employee').val(values.employee);
               $('.viewrecords #clockin_time').val(values.clockin_time);
               $('.viewrecords #date').val(values.date);
               $('.viewrecords #lat').val(values.lat);
               $('.viewrecords #longt').val(values.longt);
               $('.viewrecords #address').val(values.address);
               $('.viewrecords #status').val(values.status);
               $('.viewrecords #period_id').val(values.period_id);
               $('.viewrecords #timestamp').val(values.timestamp);
               $('.viewrecords #clockin_stamp').val(values.clockin_stamp);
               $('.viewrecords #entry_type').val(values.entry_type);
               $('.viewrecords #punct_count').val(values.punct_count);
               $('.viewrecords #late_count').val(values.late_count);
               $('.viewrecords #absent_count').val(values.absent_count);
               $('.viewrecords #clockin_type').val(values.clockin_type);
               $('.viewrecords #clockout_time').val(values.clockout_time);
               $('.viewrecords #clockout_lat').val(values.clockout_lat);
               $('.viewrecords #clockout_longt').val(values.clockout_longt);
               $('.viewrecords #clockout_distance').val(values.clockout_distance);
               $('.viewrecords #clockout_address').val(values.clockout_address);
               $('.viewrecords #clockout_stamp').val(values.clockout_stamp);
               $('.viewrecords #source').val(values.source);
            }
         });
      });
   </script>

   <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
   <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
   <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

   <script>
      $(document).ready(function() {
         $('#leaverecords').DataTable({
            dom: 'Bfrtip',
            buttons: [
               'copy', 'csv', 'excel', 'pdf'
            ],
            "scrollX": true
         });
      });
   </script>
   <!-- <script>
      $(document).ready(function() {
         var table = $('#leaverecords').DataTable({
            dom: 'Bfrtip',
            buttons: [
               'excel'
            ],
            "scrollX": true,
            "ordering": false,
            fixedColumns: {
               leftColumns: 0,
               rightColumns: 1
            },
            columnDefs: [{
               "width": "60px",
               "targets": [0]
            }]
         });
      });
   </script> -->

</body>

</html>