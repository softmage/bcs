<?php

session_start();

//Check if Session already exists
if (empty($_SESSION['user_id'])) {
    header("Location:index.php");
}

// require_once 'core.php';
require_once 'config/config.php';
require_once 'functions/General.php';
require_once 'functions/Users.php';
require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_POST['current_pwd'])) {
   changePwd($user_id, $user_email);
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Change Password | ACE! HRM </title>
    <?php include 'includes/stylesheets.php'; ?>

</head>

<body class="login">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <h1>ACE! HRM</h1>
                    <!-- <h1><?php echo APPNAME ?></h1> -->
                     <h3>Change Password</h3>
                    <div class="col-md-9">
                        <?php echo $msg->display(); ?>
                    </div>

                    <form method="POST" action="">
                       
                        <div>
                            <input type="password" class="form-control" placeholder="Please enter current password" required="" name="current_pwd" required/>
                            <hr>
                            <input type="password" class="form-control" placeholder="Please enter your new password" required="" name="password_1" required />
                            <input type="password" class="form-control" placeholder="Confirm your new password" required="" name="password_2" required/>
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit" name="change_password">Change Password</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <p>&copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>

</html>