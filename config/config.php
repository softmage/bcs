<?php

define('APPNAME', 'AceHRM');

//App Root
define('APPROOT', dirname(dirname(__FILE__)));

//URL Root
define('URLROOT', 'http://localhost/bcs');

//DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'password');
define('DB_NAME', 'bcs');

$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);