<?php
require_once 'config.php';

class Database
{

  public $connection;

  // public function __construct()
  //  {
  //   $this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  //   if (mysqli_connect_errno()) {
  //     die("Database connection failed badly" . mysqli_  error($this->connection));
  //   } //else {
  //     //echo "Sonnection is valid!";
  //  // }
  // }

  public function __construct()
  {
    $this->open_db_connection();
  }

  public function open_db_connection()
  {
    $this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if (mysqli_connect_errno()) {
      die("Database connection failed badly" . mysqli_error($this->connection));
    } 
    
  }

  // Database Query Method
  public function query($sql)
  {
    $result = mysqli_query($this->connection, $sql);
  }


  //Confirm query passed thruogh
  private function confirm_query($result)
  {

    if (!result) {
      die("Query failed" . mysqli_error($this->connection));

      return $result;
    }
  }


  //Escaping String
  public function escape_string($string)
  {

    $escaped_string = mysqli_real_escape_string($this->connection, $string);

    return $escaped_string;
  }




}

$database = new Database();
