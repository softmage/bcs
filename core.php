<?php

if (session_id() == "") session_start();
ob_start();


require_once 'config/config.php';
require_once 'functions/Dashboard.php';
require_once 'functions/Employees.php';
require_once 'functions/General.php';
require_once 'functions/Leave.php';
require_once 'functions/Users.php';
require_once 'functions/Attendance.php';
require_once 'functions/Payroll.php';

//Load RBAC file only when User Session exists
if (!empty($_SESSION['user_id'])) {
require_once 'functions/RBAC.php';

}




