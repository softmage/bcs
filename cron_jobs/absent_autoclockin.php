<?php

include '../config/config.php';

//Get Current Period ID
function getCurrentPeriod()
{

   global $conn;

   $sql = "SELECT * FROM period WHERE status = 'Open' LIMIT 1";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   return $results;
}


$period_data = getCurrentPeriod();

foreach ($period_data as $period_data) {
   $current_period_id = $period_data['id'];
   $current_period_name = $period_data['period_name'];
}


//ClockIn Absent employees
function staffAbsent()
{
   $datelist = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri');

   if (in_array(date('D'), $datelist)) {

      global $conn;
      $sql = "INSERT INTO attendance (branch, emp_id, month_period, date, status, entry_type,absent_count)
			SELECT
			  branch,
			  emp_id,
			  month_period,
			  date,
			  status,
			  entry_type,
			  absent_count
			FROM absent_list";
      $result = mysqli_query($conn, $sql);
      $affect = mysqli_affected_rows($conn);

      echo "Records inserted! - Absent Updated<br> $affect rows changed   <br>";
   }
}
staffAbsent();

//Add Clockin Stamp and Period ID to automatic ClockIns
function setClockinStamp()
{
   global $conn;
   global $current_period_id;

   $sql = "UPDATE attendance SET clockin_stamp = CONCAT(`emp_id`,'-',CURDATE()), period_id = $current_period_id WHERE date = curdate() AND status = 'Absent' ";
   $result = mysqli_query($conn, $sql);
   $affect = mysqli_affected_rows($conn);

   echo "StampClockin & Current Period set<br> $affect rows changed   <br>";
}
setClockinStamp();
