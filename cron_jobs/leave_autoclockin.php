<?php

include '../config/config.php';

//Get Current Period ID
function getCurrentPeriod()
{
   global $conn;

   $sql = "SELECT * FROM period WHERE status = 'Open' LIMIT 1";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   return $results;
}

$period_data = getCurrentPeriod();
foreach ($period_data as $period_data) {
   $current_period_id = $period_data['id'];
   $current_period_name = $period_data['period_name'];
}

//ClockIn  employees currently on leave
function staffonLeave()
{
   $datelist = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri');

   if (in_array(date('D'), $datelist)) {

      global $conn;
      //Update Staff on Leave
      $sql = "INSERT INTO attendance (branch, emp_id, month_period, date, status, entry_type,punct_count) 
			SELECT 
			branch, 
			emp_id,
			month_period,
			date,
			status,
			entry_type,
			punct_count	FROM staff_on_leave";
      $result = mysqli_query($conn, $sql);
      $affect = mysqli_affected_rows($conn);

      echo "Records inserted! - Leave Updated<br> $affect rows changed   <br> ";
   }
}

staffonLeave();

//Add Clockin Stamp and Period ID to automatic ClockIns
function setClockinStamp()
{
   global $conn;
   global $current_period_id;

   $sql = "UPDATE attendance SET clockin_stamp = CONCAT(`emp_id`,'-',CURDATE()), period_id = $current_period_id WHERE date = curdate() AND status = 'On Leave' ";
   $result = mysqli_query($conn, $sql);
   $affect = mysqli_affected_rows($conn);

   echo "StampClockin & Current Period set<br> $affect rows changed   <br>";
}
setClockinStamp();


//Close Leave Request Cycle to enable EMployee raise new eave request. 
function restartleaveCycle()
{
   global $conn;
   $sql = " UPDATE employees, leave_master SET leave_cycle = 'Completed'
			WHERE leave_master.emp_id = employees.emp_id
		AND leave_master.hr_approval = 'Confirmed'  AND leave_master.hr_resume_date = curdate()";
   $result = mysqli_query($conn, $sql);
   $affect = mysqli_affected_rows($conn);

   echo "Leave Cycle closed<br> $affect rows changed  ";
}
restartleaveCycle();
