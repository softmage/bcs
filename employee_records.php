<?php

if (session_id() == "") session_start();
ob_start();


require_once 'core.php';

if (isset($_POST['add_employee'])) {

   createEmployee();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Employees | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Employee Records</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                           <a href="add_employees.php" class="pull-right btn btn-primary"> Add Employee</a>
                        </div>
                     </div>

                     <div class="col-md-12">

                        <!-- //Display Feedback Message -->
                        <div class="col-md-6"> <?php echo $msg->display(); ?> </div>

                        <table id="datatable-buttons" class="table table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th> </th>
                                 
                                 <th>Staff No</th>
                                 <th>Employee</th>
                                 <th>Sex</th>
                                 <th>Nationality</th>
                                 <th style='display:none;'>DoB</th>
                                 <th style='display:none;'>Marital Status</th>
                                 <th style='display:none;'>No of Kids</th>
                                 <th style='display:none;'>Contact Address</th>
                                 <th style='display:none;'>Private Phone</th>
                                 <th style='display:none;'>Email</th>
                                 <th style='display:none;'>Next of Kin (NOK)</th>
                                 <th style='display:none;'>Relationship</th>
                                 <th style='display:none;'>NOK Phone</th>
                                 <th style='display:none;'>NOK Address</th>
                                 <th>Highest Qualification</th>
                                 <th>Displine/Field</th>
                                 <th style='display:none;'>Other Qualifications</th>
                                 <th style='display:none;'>Employment Status</th>
                                 <th>Date of Employment</th>
                                 <th>Official Email</th>
                                 <th>CUG Line</th>
                                 <th>Department</th>
                                 <th>Designation</th>
                                 <th>Job Level</th>
                                 <th style='display:none;'>Is LineMgr?</th>
                                 <th style='display:none;'>Line Mgr</th>
                                 <th>Branch</th>
                                 <th>Employment Type</th>
                                 <th style='display:none;'>Confirmation Status</th>
                                 <th style='display:none;'>Confirmation Date</th>
                                 <th>Annual Gross Salary</th>
                                 <th style='display:none;'>Tax ID NO</th>
                                 <th style='display:none;'>Bank Name</th>
                                 <th style='display:none;'>Account No</th>
                                 <th style='display:none;'>Pension Admin</th>
                                 <th style='display:none;'>PFA Pin</th>
                                 <th style='display:none;'>Leave Status</th>
                                 <th style='display:none;'>Exit Type</th>
                                 <th style='display:none;'>Exit Date</th>
                                 <th>Date Added</th>
                                 <th style='display:none;'>Annual Leave taken</th> 
                           </thead>

                           <tbody id="records_list">
                              <?php
                              listEmployees();
                              ?>
                           </tbody>
                        </table>
                     </div>


                     <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                 </button>
                                 <h4 class="modal-title" id="myModalLabel2">View Records</h4>
                              </div>
                              <div class="modal-body">

                                 <form class="editEmployee form-horizontal form-label-left input_mask" method="POST" id="editEmployee" name="editEmployee" name="editEmployee">
                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                       <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Personal Details</a>
                                          </li>
                                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Contact Details</a>
                                          </li>
                                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Employment Details</a>
                                          </li>
                                          <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Remuneration Details</a>
                                          </li>
                                       </ul>
                                       <div id="myTabContent" class="tab-content">
                                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                             <input type="hidden" id="emp_id" name="emp_id" required="required" class="form-control col-md-7 col-xs-12" readonly>


                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Staff No <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="staff_no" name="staff_no" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="lastname" name="lastname" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="firstname" name="firstname" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gender">Gender <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="gender" name="gender" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality">Nationality <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="nationality" name="nationality" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dob">Date of Birth <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="date" id="dob" name="dob" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="marital_status">Marital Status <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="marital_status" class="form-control" name="marital_status" required="" readonly />

                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_of_kids">No of Kids <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="no_of_kids" class="form-control" class="form-control" required="" readonly />
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification">Qualification <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="qualification" name="qualification" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discipline">Discipline <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="discipline" name="discipline" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="other_qual">Other Qualifications <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="other_qual" id="other_qual" class="form-control col-md-7 col-xs-12" rows="3" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                          </div>

                                          <!-- Panel 2 -->
                                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="private_email">Personal Email <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="private_email" id="private_email" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="private_phone">Personal Phone <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="private_phone" id="private_phone" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="home_address">Contact Address <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="home_address" id="home_address" class="form-control col-md-7 col-xs-12" rows="3" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="next_of_kin">Next of Kin <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="next_of_kin" id="next_of_kin" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kin_relationship">Next of Kin Relationship <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="kin_relationship" id="kin_relationship" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nok_phone">Next of Kin Phone<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="nok_phone" id="nok_phone" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nok_address">Next of Kin Address <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="nok_address" id="nok_address" class="form-control col-md-7 col-xs-12" rows="3" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                          </div>
                                          <!-- Panel 3 -->
                                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employ_status">Employment Status <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="employ_status" name="employ_status" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hire_date">Date Employed <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="date" name="hire_date" id="hire_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="official_email">Official Email <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="official_email" id="official_email" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cug_line">CUG Line <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="cug_line" id="cug_line" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cug_line">Location/Branch <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="branch" name="branch" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Departments <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="department" name="department" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="job_level">Job Level <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="job_level" name="job_level" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="designation">Designation <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="designation" name="designation" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="isline_mgr">Is Line Mgr? <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="isline_mgr" name="isline_mgr" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employment_type">Employment Type <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="employment_type" name="employment_type" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_mgr">Line Mgr<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="line_mgr" name="line_mgr" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                          </div>
                                          <!-- Panel 4 -->
                                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">

                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="annual_sal">Annual Salary <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="annual_sal" id="annual_sal" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tin">Tax ID Number <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="tin" id="tin" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bank_name">Bank Name <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="bank_name" id="bank_name" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="account_no">Account Number <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="account_no" id="account_no" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pfa">Pension Administrator<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="pfa" name="pfa" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pfa_pin">Pension ID<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="pfa_pin" id="pfa_pin" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                             <br>
                                             <div class="col-md-12 col-xs-12 form-group has-feedback">
                                                <hr>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>




                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         $("#addEmployee").validate({
            ignore: "",
            // $("form[name='addEmployee']").validate({ ignore: "" 
            // Specify validation rules
            rules: {
               firstname: {
                  required: true
               },
               lastname: {
                  required: true
               },
               nationality: {
                  required: true
               },
               dob: {
                  required: true
               },
               gender: {
                  required: true
               },
               marital_status: {
                  required: true
               }

            },

            // Specify validation error messages
            messages: {
               firstname: "Please enter firstname",
               lastname: "Please enter  lastname",
            },
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');


         $.ajax({

            url: 'records.php?action=view&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {
               $('.editEmployee #emp_id').val(values.emp_id);
               $('.editEmployee #staff_no').val(values.staff_no);
               $('.editEmployee #lastname').val(values.lastname);
               $('.editEmployee #firstname').val(values.firstname);
               $('.editEmployee #gender').val(values.gender);
               $('.editEmployee #nationality').val(values.nationality);
               $('.editEmployee #dob').val(values.dob);
               $('.editEmployee #marital_status').val(values.marital_status);
               $('.editEmployee #no_of_kids').val(values.no_of_kids);
               $('.editEmployee #home_address').val(values.home_address);
               $('.editEmployee #private_phone').val(values.private_phone);
               $('.editEmployee #private_email').val(values.private_email);
               $('.editEmployee #next_of_kin').val(values.next_of_kin);
               $('.editEmployee #kin_relationship').val(values.kin_relationship);
               $('.editEmployee #nok_phone').val(values.nok_phone);
               $('.editEmployee #nok_address').val(values.nok_address);
               $('.editEmployee #qualification').val(values.qualification);
               $('.editEmployee #discipline').val(values.discipline);
               $('.editEmployee #other_qual').val(values.other_qual);
               $('.editEmployee #employ_status').val(values.employ_status);
               $('.editEmployee #hire_date').val(values.hire_date);
               $('.editEmployee #official_email').val(values.official_email);
               $('.editEmployee #cug_line').val(values.cug_line);
               $('.editEmployee #department').val(values.department);
               $('.editEmployee #designation').val(values.designation);
               $('.editEmployee #job_level').val(values.job_level);
               $('.editEmployee #isline_mgr').val(values.isline_mgr);
               $('.editEmployee #line_mgr').val(values.line_mgr);
               $('.editEmployee #branch').val(values.branch);
               $('.editEmployee #employment_type').val(values.employment_type);
               $('.editEmployee #confirmation_status').val(values.confirmation_status);
               $('.editEmployee #confirmation_date').val(values.confirmation_date);
               $('.editEmployee #annual_sal').val(values.annual_sal);
               $('.editEmployee #tin').val(values.tin);
               $('.editEmployee #bank_name').val(values.bank_name);
               $('.editEmployee #account_no').val(values.account_no);
               $('.editEmployee #pfa').val(values.pfa);
               $('.editEmployee #pfa_pin').val(values.pfa_pin);
               $('.editEmployee #leave_status').val(values.leave_status);
               $('.editEmployee #exit_type').val(values.exit_type);
               $('.editEmployee #exit_date').val(values.exit_date);
               $('.editEmployee #date_added').val(values.date_added);
               $('.editEmployee #annual_leave_taken').val(values.annual_leave_taken);
            }
         });
      });
   </script>

   <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
   <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
   <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
</body>

</html>