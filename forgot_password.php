<?php

session_start();

//Check if Session already exists
if (!empty($_SESSION['user_id'])) {
    header("Location:index.php");
}

// require_once 'core.php';
require_once 'config/config.php';
require_once 'functions/Users.php';
require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_POST['reset_password'])) {
    resetPwd();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Reset Password | ACE! HRM </title>
    <?php include 'includes/stylesheets.php'; ?>

</head>

<body class="login">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <h1>ACE! HRM</h1>
                    <!-- <h1><?php echo APPNAME ?></h1> -->
                     <h3>Reset Password</h3>
                    <div class="col-md-9">
                        <?php echo $msg->display(); ?>
                    </div>

                    <form method="POST" action="">
                       
                        <div>
                            <input type="text" class="form-control" placeholder="Email Address" required="" name="reset_email" />
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit" name="reset_password">Reset Password</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <p>&copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>

</html>