<?php


require_once 'FlashMessages.php';

function listAttendanceRecords($filter)
{
   global $conn;

   $sql = "SELECT * from attendance_summary WHERE $filter";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $id =  $row['id'];
         $branch =  $row['branch'];
         $payroll_period =  $row['payroll_period'];
         $month_period =  $row['month_period'];
         $emp_id =  $row['emp_id'];
         $employee =  $row['employee'];
         $clockin_time =  $row['clockin_time'];
         $date =  $row['date'];
         $lat =  $row['lat'];
         $longt =  $row['longt'];
         $address =  $row['address'];
         $status =  $row['status'];
         $period_id =  $row['period_id'];
         $timestamp =  $row['timestamp'];
         $clockin_stamp =  $row['clockin_stamp'];
         $entry_type =  $row['entry_type'];
         $punct_count =  $row['punct_count'];
         $late_count =  $row['late_count'];
         $absent_count =  $row['absent_count'];
         $clockin_type =  $row['clockin_type'];
         $clockout_time =  $row['clockout_time'];
         $clockout_lat =  $row['clockout_lat'];
         $clockout_longt =  $row['clockout_longt'];
         $clockout_distance =  $row['clockout_distance'];
         $clockout_address =  $row['clockout_address'];
         $clockout_stamp =  $row['clockout_stamp'];
         $source =  $row['source'];

         echo "<tr>
                  <td>
                     <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$id><i class='fa fa-search'></i></button>
                  </td>               
                  <td>$id</td>
                  <td>$branch</td>
                  <td>$payroll_period</td>
                  <td>$month_period</td>
                  <td style='display:none;'>$emp_id</td>
                  <td>$employee</td>
                  <td>$clockin_time</td>
                  <td>$date</td>
                  <td style='display:none;'>$lat</td>
                  <td style='display:none;'$longt</td>
                  <td>$address</td>
                  <td>$status</td>
                  <td style='display:none;'>$period_id</td>
                  <td style='display:none;'>$clockin_stamp</td>
                  <td>$entry_type</td>
                  <td style='display:none;'>$punct_count</td>
                  <td style='display:none;'>$late_count</td>
                  <td style='display:none;'>$absent_count</td>
                  <td>$clockin_type</td>
                  <td>$clockout_time</td>
                  <td style='display:none;'>$clockout_lat</td>
                  <td style='display:none;'>$clockout_longt</td>
                  <td>$clockout_address</td>
                  <td style='display:none;'>$clockout_stamp</td>
                  <td>$source</td>

   
               </tr>";
      }
   } else {
      echo "<tr> <td>No records available </td></tr>";
   }
}

function monthlyAttendance()
{
   global $conn;

   $sql = "SELECT * from monthly_attendance_report";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $id =  $row['id'];
         $period_id =  $row['period_id'];
         $emp_id =  $row['emp_id'];
         $payroll_period =  $row['payroll_period'];
         $month_period =  $row['month_period'];
         $branch =  $row['branch'];
         $staff_no =  $row['staff_no'];
         $employee =  $row['employee'];
         $department =  $row['department'];
         $designation =  $row['designation'];
         $job_level =  $row['job_level'];
         $employment_type =  $row['employment_type'];
         $confirmation_status =  $row['confirmation_status'];
         $times_present =  $row['times_present'];
         $times_punctual =  $row['times_punctual'];
         $times_late =  $row['times_late'];
         $times_absent =  $row['times_absent'];         

         echo "<tr>                   
                  <td style='display:none;'>$period_id</td>  
                  <td style='display:none;'>$emp_id</td>
                  <td>$payroll_period</td>
                  <td>$month_period</td>
                  <td>$branch</td>
                  <td>$staff_no</td>
                  <td>$employee</td>
                  <td>$department</td>
                  <td>$designation</td>
                  <td>$job_level</td>
                  <td>$employment_type</td>
                  <td>$confirmation_status</td>
                  <td>$times_present</td>
                  <td>$times_punctual</td>
                  <td>$times_late</td>
                  <td>$times_absent</td> 
               </tr>";
      }
   } else {
      echo "<tr> <td>No records available </td></tr>";
   }
}




//Load leave details
function viewSingleAttendance($id)
{

   global $conn;
   $sql = "SELECT * from attendance_summary WHERE id = $id";
   $result =  mysqli_query($conn, $sql);
   $row = mysqli_fetch_assoc($result);

   $values = array();

   if ($row) {

      $values['id'] = $row['id'];
      $values['branch'] = $row['branch'];
      $values['payroll_period'] = $row['payroll_period'];
      $values['month_period'] = $row['month_period'];
      $values['emp_id'] = $row['emp_id'];
      $values['employee'] = $row['employee'];
      $values['clockin_time'] = $row['clockin_time'];
      $values['date'] = $row['date'];
      $values['lat'] = $row['lat'];
      $values['longt'] = $row['longt'];
      $values['address'] = $row['address'];
      $values['status'] = $row['status'];
      $values['period_id'] = $row['period_id'];
      $values['timestamp'] = $row['timestamp'];
      $values['clockin_stamp'] = $row['clockin_stamp'];
      $values['entry_type'] = $row['entry_type'];
      $values['punct_count'] = $row['punct_count'];
      $values['late_count'] = $row['late_count'];
      $values['absent_count'] = $row['absent_count'];
      $values['clockin_type'] = $row['clockin_type'];
      $values['clockout_time'] = $row['clockout_time'];
      $values['clockout_lat'] = $row['clockout_lat'];
      $values['clockout_longt'] = $row['clockout_longt'];
      $values['clockout_distance'] = $row['clockout_distance'];
      $values['clockout_address'] = $row['clockout_address'];
      $values['clockout_stamp'] = $row['clockout_stamp'];
      $values['source'] = $row['source'];
   }
   echo json_encode($values);
}






function clockIn()
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;
   global $current_period_id;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   $branch = trim($_POST['branch']);
   $emp_id = trim($_POST["emp_id"]);
   $clockin_time = date("H:i:s");
   $date = date('Y-m-d');
   $lat = trim($_POST["lat"]);
   $longt = trim($_POST["longt"]);

   $resumption_time = '09:00:00';

   $clockin_coords = $lat . "," . $longt;

   $clockin_stamp = $emp_id . "-" . $date;

   //SET puntual, absent and Status Values
   $status = 'Punctual';
   $punct_count = '1';
   $late_count = '0';

   if ($clockin_time > $resumption_time) {
      $status = 'Late';
      $late_count = 1;
      $punct_count = 0;
   }

   //Fecth Clockin Stamp if it exists
   $check_stamp = "SELECT clockin_stamp FROM attendance where emp_id = $emp_id AND date = '$date'";
   $resulta = mysqli_query($conn, $check_stamp);
   $row = mysqli_fetch_assoc($resulta);
   $stampcheck = $row['clockin_stamp'];


   //Set Default Coordinates
   $get_coords = "SELECT CONCAT(default_lat,',',default_longt) AS default_coords FROM branches WHERE branch = '$branch'";
   $resultb = mysqli_query($conn, $get_coords);
   $row = mysqli_fetch_assoc($resultb);
   $default_coords = $row['default_coords'];


   /* PRELIMINARY VALIDATION */

   //Check if User has already ClockedIn by checking if the computed clockin_stamp exists on the DB. f it exists, terminate the function
   if ($clockin_stamp == $stampcheck) {
      $msg->error('Oops! You cannot clock in twice. You already clocked in earlier today.');
      return FALSE;
   }

   //Check if GPS was turned on
   if ($_POST["lat"] == '') {
      $msg->error('Oops! Pls kindly turn on your GPS and grant ACE HRM permission to use it.');
      return FALSE;
   }


   // $current_period_id = getCurrentPeriod();


   /*GOOGLE MAPS API CALLS **/

   //Calculate Distance
   $api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$default_coords&destinations=" . $clockin_coords . "&key=AIzaSyCjTonyep677Tbb40aTbqhcdiO4SytoApA");
   $data = json_decode($api);
   $current_distance = ((int) $data->rows[0]->elements[0]->distance->value);
   $geofence = 30;

   if ($current_distance > $geofence) {

      $msg->error('Oops! You are tying to ClockIn from more than ' . $geofence . ' metres. You need to get to the Office and Clock-In again.');
      return FALSE;
   }


   //Get Human-readable address
   $address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$clockin_coords&key=AIzaSyCjTonyep677Tbb40aTbqhcdiO4SytoApA&sensor=true");
   $json_data = json_decode($address);
   $full_address = $json_data->results[0]->formatted_address;
   $full_address = mysqli_real_escape_string($conn, $full_address);



   /** WRITE TO DATABASE */
   $sql = "INSERT INTO attendance(emp_id, date, lat, longt, clockin_time, branch, address, month_period,clockin_stamp,entry_type, clockin_type, status, late_count, punct_count, period_id, source) VALUES(" . $emp_id . ", '" . $date . "','" . $lat . "', '" . $longt . "', '$clockin_time', '" . $branch . "', '" . $full_address . "', DATE_FORMAT(date,'%b %Y'), CONCAT(emp_id,'-',date),'Manual ClockIn', 'On Site', '$status', $late_count, $punct_count, $current_period_id, 'website' ) ";
   $result = mysqli_query($conn, $sql);

   if ($result) {
      $last_id = mysqli_insert_id($conn);
      echo "<script type='text/javascript'>alert('Great! Form submitted! Your coordinates are $clockin_coords and you clocked in from ' . $full_address)</script>";
      $msg->success('Great! You have clocked in From <b>' . $full_address. '</b>', 'my_dashboard.php');
   } else {
      $errormsg = mysqli_error($conn);
      $msg->error('Oops! an error occured. Please try again - ' . $errormsg . '<br>' . $sql);
      echo "<script>alert('Oops! an error occured. Please try again - $errormsg')</script>";
   }
}



function clockOut()
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   $emp_id = trim($_POST["emp_id"]);
   $clockout_time = date("H:i:s");
   $date = date('Y-m-d');
   $lat = trim($_POST["clockout_lat"]);
   $longt = trim($_POST["clockout_longt"]);

   $clockout_coords = $lat. "," . $longt;
   $clockout_stamp = $emp_id . "-" . $date;


   //Fecth ClockOut Stamp if it exists
   $check_stamp = "SELECT clockout_stamp FROM attendance where emp_id = $emp_id AND date = '$date'";
   $resulta = mysqli_query($conn, $check_stamp);
   $row = mysqli_fetch_assoc($resulta);
   $stampcheck = $row['clockout_stamp'];



   /* PRELIMINARY VALIDATION */

   //Check if User has already ClockedIn by checking if the computed clockin_stamp exists on the DB. f it exists, terminate the function
   if ($clockout_stamp == $stampcheck) {
      $msg->error('Oops! You cannot Clock Out twice. You already Clocked Out earlier today.');
      return FALSE;
   }

   //Check if GPS was turned on
   if ($_POST["clockout_lat"] == '') {
      $msg->error('Oops! Pls kindly turn on your GPS and grant ACE HRM permission to use it. Lat is ' . $lat . 'Longt is' . $longt . '');
      return FALSE;
   }


   /*GOOGLE MAPS API CALLS **/

   //Get Human-readable address
   $address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$clockout_coords&key=AIzaSyCjTonyep677Tbb40aTbqhcdiO4SytoApA&sensor=true");
   $json_data = json_decode($address);
   $full_address = $json_data->results[0]->formatted_address;
   $full_address = mysqli_real_escape_string($conn, $full_address);



   /** WRITE TO DATABASE */
   $sql = " UPDATE attendance SET clockout_time = '$clockout_time', clockout_lat = '" . $lat . "', clockout_longt = '" . $longt . "', clockout_address = '" . $full_address . "', clockout_stamp = '" . $clockout_stamp. "'WHERE date = curdate() AND clockout_time IS NULL AND emp_id = '" . $emp_id . "' ";   
   $result = mysqli_query($conn, $sql);

   if ($result) {
     
      $msg->success('Great! You have ClockedOut successfully from ' . $full_address . '', 'my_dashboard.php');
   } else {
      $errormsg = mysqli_error($conn);
      $msg->error('Oops! an error occured. Please try again - ' . $errormsg . '<br>' . $sql);
   }
}


function remoteClockIn()
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;
   global $current_period_id;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
   
   $branch = trim($_POST['branch']);
   $emp_id = trim($_POST["emp_id"]);
   $clockin_time = date("H:i:s");
   $date = date('Y-m-d');
   $lat = trim($_POST["lat"]);
   $longt = trim($_POST["longt"]);

   $resumption_time = '09:00:00';

   $clockin_coords = $lat . "," . $longt;
   $clockin_stamp = $emp_id . "-" . $date;

   //SET puntual, absent and Status Values
   $status = 'Punctual';
   $punct_count = '1';
   $late_count = '0';

   if ($clockin_time > $resumption_time) {
      $status = 'Late';
      $late_count = 1;
      $punct_count = 0;
   }

   //Fecth Clockin Stamp if it exists
   $check_stamp = "SELECT clockin_stamp FROM attendance where emp_id = $emp_id AND date = '$date'";
   $resulta = mysqli_query($conn, $check_stamp);
   $row = mysqli_fetch_assoc($resulta);
   $stampcheck = $row['clockin_stamp'];


   /* PRELIMINARY VALIDATION */

   //Check if User has already ClockedIn by checking if the computed clockin_stamp exists on the DB. f it exists, terminate the function
   if ($clockin_stamp == $stampcheck) {
      $msg->error('Oops! You cannot clock in twice. You already clocked in earlier today.');
      return FALSE;
   }

   //Check if GPS was turned on
   if ($_POST["lat"] == '') {
      $msg->error('Oops! Pls kindly turn on your GPS and grant ACE HRM permission to use it.');
      return FALSE;
   }


   // $current_period_id = getCurrentPeriod();


   /*GOOGLE MAPS API CALLS **/

   //Get Human-readable address
   $address = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$clockin_coords&key=AIzaSyCjTonyep677Tbb40aTbqhcdiO4SytoApA&sensor=true");
   $json_data = json_decode($address);
   $full_address = $json_data->results[0]->formatted_address;
   $full_address = mysqli_real_escape_string($conn, $full_address);



   /** WRITE TO DATABASE */
   $sql = "INSERT INTO attendance(emp_id, date, lat, longt, clockin_time, branch, address, month_period,clockin_stamp,entry_type, clockin_type, status, late_count, punct_count, period_id, source) VALUES(" . $emp_id . ", '" . $date . "','" . $lat . "', '" . $longt . "', '$clockin_time', '" . $branch . "', '" . $full_address . "', DATE_FORMAT(date,'%b %Y'), CONCAT(emp_id,'-',date),'Manual ClockIn', 'Remote ClockIn', '$status', $late_count, $punct_count, $current_period_id, 'website' ) ";
   $result = mysqli_query($conn, $sql);

   if ($result) {
      $last_id = mysqli_insert_id($conn);
      echo "<script type='text/javascript'>alert('Great! Form submitted! Your coordinates are $clockin_coords and you clocked in from ' . $full_address)</script>";
      $msg->success('Great! You have clocked in From ' . $full_address . '', 'my_dashboard.php');
   } else {
      $errormsg = mysqli_error($conn);
      $msg->error('Oops! an error occured. Please try again - ' . $errormsg . '<br>' . $sql);
      echo "<script>alert('Oops! an error occured. Please try again - $errormsg')</script>";
   }
}



function getUserClockInStatus($user_id){

   global $conn;

   $sql = "SELECT id FROM attendance WHERE date = curdate() AND emp_id = $user_id AND clockin_time IS NOT NULL LIMIT 1";
   $result = mysqli_query($conn, $sql);
   $rowcount = mysqli_num_rows($result);

   if($rowcount < 1){
      $has_clockedin = 'No';
   }else {
      $has_clockedin = 'Yes';
   }

   return $has_clockedin;
}

function getUserClockOutStatus($user_id){

   global $conn;

   $sql = "SELECT id FROM attendance WHERE date = curdate() AND emp_id = $user_id AND clockout_time IS NOT NULL LIMIT 1";
   $result = mysqli_query($conn, $sql);
   $rowcount = mysqli_num_rows($result);

   if($rowcount < 1){
      $has_clockedout= 'No';
   }else {
      $has_clockedout = 'Yes';
   }
   return $has_clockedout;
}