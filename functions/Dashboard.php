<?php 

global $userlevel;
global $user_id;

//Fetch Daily Attendance SUmmary based on attendance status
function countryAttendance(){
	global $conn;
	$sql = mysqli_query($conn, "SELECT SUM(punct_count) AS c_punctual, SUM(late_count) As c_late, SUM(absent_count) AS c_absent FROM attendance WHERE date = curdate()");

	while($row = mysqli_fetch_array($sql)){
		$c_late = number_format($row['c_late']);$c_punctual = number_format($row['c_punctual']);$c_absent = number_format($row['c_absent']);
	}
	return;  
}

//Fetch number of male employees
function maleCount(){
	global $conn;
	$sql = mysqli_query($conn, "SELECT count(emp_id) AS male FROM employees WHERE gender = 'Male'");

	while($row = mysqli_fetch_array($sql)){
		$malecount = $row['male'];
	}
	return $malecount;
}

//Fetch number of Female employees
function femaleCount(){
	global $conn;
	$sql = mysqli_query($conn, "SELECT count(emp_id) AS female FROM employees WHERE gender = 'Female'");
	
	while($row = mysqli_fetch_array($sql)){
		$femalecount = $row['female'];
	}
	return $femalecount;
}

//Get number of pending leave reviews WHERE current User is the Line Manager
function leaveReview($id){
	global $conn;
	$sql = mysqli_query($conn, "SELECT count(leave_id) AS leave_review FROM leave_summary WHERE linemgr_review = 'Pending Review' AND line_mgr_id = $id");

	while($row = mysqli_fetch_array($sql)) {       
		$leave_review = $row['leave_review'];
	}
	return $leave_review;
}

//Get the number if unconfirmed Leave Requests
function confirmationReview(){
	global $country;

	global $conn;
	$sql = mysqli_query($conn, "SELECT count(leave_id) AS unconfirmed FROM leave_summary WHERE leave_status = 'Pending HR Confirmation'");

	while($row = mysqli_fetch_array($sql)) {       
		$unconfirmed = $row['unconfirmed'];
	}
	return $unconfirmed;
}

//Get number of unused Leave
function unusedLeave($id){
 
	global $conn;
	$sql = mysqli_query($conn, "SELECT (20 - annual_leave_taken) AS unusedleave FROM employees WHERE emp_id = $id ");

	while($row = mysqli_fetch_array($sql)){
		$unusedleave = $row['unusedleave'];
	}
	return $unusedleave;
}

//Get number of days Absent
function daysAbsent($id, $current_period){
 
	global $conn;
	$sql = mysqli_query($conn, "SELECT count(emp_id) AS days_absent FROM attendance WHERE status = 'Absent' AND emp_id = $id AND period_id = $current_period ");

	while($row = mysqli_fetch_array($sql)){
		$days_absent = $row['days_absent'];
	}
	return $days_absent;
}

 
//Get number of pending Certitifcation SUbmission Review
function certificationStatus(){

   global $conn;
	$sql = mysqli_query($conn, "SELECT count(emp_id) AS certificationstatus FROM skills_certifications WHERE approval_status = 'Pending Approval'");

	while($row = mysqli_fetch_array($sql)){
		$certificationstatus = $row['certificationstatus'];
	}
	return $certificationstatus;
}


//Get Weekly birthday 
function getBirthdays(){
	global $conn;  
	$sql = "SELECT emp_id, concat(firstname,' ',lastname) AS empname, branch, official_email AS email,cug_line AS cug, DATE_FORMAT(`dob`, '%D %b') AS dob, designation, DATE_FORMAT(`hire_date`, '%D %b %Y') AS hire_date FROM employees WHERE WEEK(dob) = WEEK(curdate()) ORDER BY dob AND employ_status = 'Active'";	
	$result = mysqli_query($conn, $sql);	      	
	$birthday = mysqli_fetch_all($result, MYSQLI_ASSOC);
		
	return $birthday;

}

//GET DYNAMIC DATA FOR CHARTS

//Get Attendance Data for HR Bar Chart
if ($userlevel == 100) {
	$month_name = array();
	$late_bar = array();
	$punctual_bar = array();
	$absent_bar = array();
	$sql = mysqli_query($conn, "SELECT CONCAT('WEEK ',WEEK(date,3)) AS month_period,
	sum(punct_count) AS punctual_bar, SUM(late_count) As late_bar, SUM(absent_count) AS absent_bar FROM attendance GROUP BY WEEK(date,3) ORDER by date DESC LIMIT 4 ");
	while ($row = mysqli_fetch_array($sql)) {
		$month_name[] = "'" . $row['month_period'] . "'";
		$late_bar[] = "" . $row['late_bar'] . "";
		$punctual_bar[] = "" . $row['punctual_bar'] . "";
		$absent_bar[] = "" . $row['punctual_bar'] . "";
	}
	$month_name = implode(",", $month_name);
	$late_bar = implode(",", $late_bar);
	$punctual_bar = implode(",", $punctual_bar);
	$absent_bar = implode(",", $absent_bar);
} elseif ($userlevel > 100) {

	$month_name = array();
	$late_bar = array();
	$punctual_bar = array();
	$absent_bar = array();
	$sql = mysqli_query($conn, "SELECT CONCAT('WEEK ',WEEK(date,3)) AS month_period,
	sum(punct_count) AS punctual_bar, SUM(late_count) As late_bar, SUM(absent_count) AS absent_bar FROM attendance GROUP BY WEEK(date,3) ORDER by date DESC LIMIT 4 ");
	while ($row = mysqli_fetch_array($sql)) {
		$month_name[] = "'" . $row['month_period'] . "'";
		$late_bar[] = "" . $row['late_bar'] . "";
		$punctual_bar[] = "" . $row['punctual_bar'] . "";
		$absent_bar[] = "" . $row['punctual_bar'] . "";
	}

	$month_name = implode(",", $month_name);
	$late_bar = implode(",", $late_bar);
	$punctual_bar = implode(",", $punctual_bar);
	$absent_bar = implode(",", $absent_bar);
} elseif ($userlevel = 1) {

	$month_name = array();
	$late_bar = array();
	$punctual_bar = array();
	$absent_bar = array();
	$sql = mysqli_query($conn, "SELECT CONCAT('WEEK ',WEEK(date,3)) AS month_period,
	sum(punct_count) AS punctual_bar, SUM(late_count) As late_bar, SUM(absent_count) AS absent_bar FROM attendance WHERE emp_id = 39 GROUP BY WEEK(date,3) ORDER by date DESC LIMIT 4 ");
	while ($row = mysqli_fetch_array($sql)) {
		$month_name[] = "'" . $row['month_period'] . "'";
		$late_bar[] = "" . $row['late_bar'] . "";
		$punctual_bar[] = "" . $row['punctual_bar'] . "";
		$absent_bar[] = "" . $row['punctual_bar'] . "";
	}

	$month_name = implode(",", $month_name);
	$late_bar = implode(",", $late_bar);
	$punctual_bar = implode(",", $punctual_bar);
	$absent_bar = implode(",", $absent_bar);
}


//Get Individual Attendance Data for Bar Chart
$emp_monthname = array();
$emp_late_bar = array();
$emp_punctual_bar = array();
$absent_bar = array();
$sql = mysqli_query($conn, "SELECT CONCAT('WEEK ',WEEK(date,3)) AS emp_monthname,
	sum(punct_count) AS emp_punctual_bar, SUM(late_count) As emp_late_bar, SUM(absent_count) AS absent_bar FROM attendance  GROUP BY WEEK(date,3) ORDER by date DESC LIMIT 4");
while ($row = mysqli_fetch_array($sql)) {
	$emp_monthname[] = "'" . $row['emp_monthname'] . "'";
	$emp_late_bar[] = "" . $row['emp_late_bar'] . "";
	$emp_punctual_bar[] = "" . $row['emp_punctual_bar'] . "";
	$absent_bar[] = "" . $row['absent_bar'] . "";
}

$emp_monthname = implode(",", $emp_monthname);
$emp_late_bar = implode(",", $emp_late_bar);
$emp_punctual_bar = implode(",", $emp_punctual_bar);
$absent_bar = implode(",", $absent_bar);

