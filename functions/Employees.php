<?php

require_once 'FlashMessages.php';
require_once 'Emailer.php';



function generateRandomPassword($length = 20)
{
  $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
  $charsLength = strlen($chars);

  $randomString = '';

  for ($i = 0; $i < $length; $i++) {
    $randomString .= $chars[rand(0, $charsLength - 1)];
  }


  return $randomString;
}


function archiveEmployee($id)
{

  $msg = new \Plasticbrain\FlashMessages\FlashMessages();

  global $conn;

  $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

  $exit_type = mysqli_real_escape_string($conn, $_POST['exit_type']);
  $exit_date = mysqli_real_escape_string($conn, $_POST['exit_date']);

  $sql = "UPDATE employees SET exit_type = '', exit_date = '' WHERE emp_id = $id";
  $result = mysqli_query($conn, $sql);

  if ($result) {
    $msg->success('Great! Employee has been archived.', 'employee_records.php');
  } else {
    $errormsg = mysqli_error($conn);
    echo "Oops! An error occured. <br> $errormsg";
  }
}


//Fecth All employees and display in tabular format

function listEmployees()
{
  global $conn;

  $sql = "SELECT * from employees WHERE employ_status = 'Active'";
  $result = mysqli_query($conn, $sql);
  $rowCount = mysqli_num_rows($result);

  //return $emp_objs;

  if ($rowCount > 0) {
    while ($row = mysqli_fetch_array($result)) {

      $emp_id = $row['emp_id'];
      $staff_no = $row['staff_no'];
      $lastname = $row['lastname'];
      $firstname = $row['firstname'];
      $gender = $row['gender'];
      $nationality = $row['nationality'];
      $dob = $row['dob'];
      $marital_status = $row['marital_status'];
      $no_of_kids = $row['no_of_kids'];
      $home_address = $row['home_address'];
      $private_phone = $row['private_phone'];
      $private_email = $row['private_email'];
      $next_of_kin = $row['next_of_kin'];
      $kin_relationship = $row['kin_relationship'];
      $nok_phone = $row['nok_phone'];
      $nok_address = $row['nok_address'];
      $qualification = $row['qualification'];
      $discipline = $row['discipline'];
      $other_qual = $row['other_qual'];
      $employ_status = $row['employ_status'];
      $hire_date = $row['hire_date'];
      $official_email = $row['official_email'];
      $cug_line = $row['cug_line'];
      $department = $row['department'];
      $designation = $row['designation'];
      $job_level = $row['job_level'];
      $isline_mgr = $row['isline_mgr'];
      $line_mgr = $row['line_mgr'];
      $branch = $row['branch'];
      $employment_type = $row['employment_type'];
      $confirmation_status = $row['confirmation_status'];
      $confirmation_date = $row['confirmation_date'];
      $annual_sal = number_format($row['annual_sal'], 2);
      $tin = $row['tin'];
      $bank_name = $row['bank_name'];
      $account_no = $row['account_no'];
      $pfa = $row['pfa'];
      $pfa_pin = $row['pfa_pin'];
      $leave_status = $row['leave_status'];
      $exit_type = $row['exit_type'];
      $exit_date = $row['exit_date'];
      $date_added = $row['date_added'];
      $annual_leave_taken = $row['annual_leave_taken'];

      $employee_name = $lastname . " " . $firstname;

      echo "<tr>
                    <td>
                        <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$emp_id><i class='fa fa-search'></i></button>                             
                        <a href='editemployee.php?id=$emp_id' class='btn btn-danger btn-xs'><i class='fa fa-edit'></i></a>
                    </td>                     
                    <td>$staff_no</td>
                    <td>$employee_name</td> 
                    <td>$gender</td>
                    <td>$nationality</td>
                    <td style='display:none;'>$dob</td>
                    <td style='display:none;'>$marital_status</td>
                    <td style='display:none;'>$no_of_kids</td>
                    <td style='display:none;'>$home_address</td>
                    <td style='display:none;'>$private_phone</td>
                    <td style='display:none;'>$private_email</td>
                    <td style='display:none;'>$next_of_kin</td>
                    <td style='display:none;'>$kin_relationship</td>
                    <td style='display:none;'>$nok_phone</td>
                    <td style='display:none;'>$nok_address</td>
                    <td>$qualification</td>
                    <td>$discipline</td>
                    <td style='display:none;'>$other_qual</td>
                    <td style='display:none;'>$employ_status</td>
                    <td>$hire_date</td>
                    <td>$official_email</td>
                    <td>$cug_line</td>
                    <td>$department</td>
                    <td>$designation</td>
                    <td>$job_level</td>
                    <td style='display:none;'>$isline_mgr</td>
                    <td style='display:none;'>$line_mgr</td>
                    <td>$branch</td>
                    <td>$employment_type</td>
                    <td style='display:none;'>$confirmation_status</td>
                    <td style='display:none;'>$confirmation_date</td>
                    <td style='text-align:right'>$annual_sal</td>
                    <td style='display:none;'>$tin</td>
                    <td style='display:none;'>$bank_name</td>
                    <td style='display:none;'>$account_no</td>
                    <td style='display:none;'>$pfa</td>
                    <td style='display:none;'>$pfa_pin</td>
                    <td style='display:none;'>$leave_status</td>
                    <td style='display:none;'>$exit_type</td>
                    <td style='display:none;'>$exit_date</td>
                    <td>$date_added</td>
                    <td style='display:none;'>$annual_leave_taken</td>                                         
                  </tr>";
    }
  } else {
    echo "<tr> <td>No records available </td></tr>";
  }
}

//Fetch Archived EMployees
function listArchivedEmployees()
{
  global $conn;

  $sql = "SELECT * from employees WHERE employ_status <> 'Active'";
  $result = mysqli_query($conn, $sql);
  $rowCount = mysqli_num_rows($result);

  if ($rowCount > 0) {
    while ($row = mysqli_fetch_array($result)) {

      $emp_id = $row['emp_id'];
      $staff_no = $row['staff_no'];
      $lastname = $row['lastname'];
      $firstname = $row['firstname'];
      $gender = $row['gender'];
      $nationality = $row['nationality'];
      $dob = $row['dob'];
      $marital_status = $row['marital_status'];
      $no_of_kids = $row['no_of_kids'];
      $home_address = $row['home_address'];
      $private_phone = $row['private_phone'];
      $private_email = $row['private_email'];
      $next_of_kin = $row['next_of_kin'];
      $kin_relationship = $row['kin_relationship'];
      $nok_phone = $row['nok_phone'];
      $nok_address = $row['nok_address'];
      $qualification = $row['qualification'];
      $discipline = $row['discipline'];
      $other_qual = $row['other_qual'];
      $employ_status = $row['employ_status'];
      $hire_date = $row['hire_date'];
      $official_email = $row['official_email'];
      $cug_line = $row['cug_line'];
      $department = $row['department'];
      $designation = $row['designation'];
      $job_level = $row['job_level'];
      $isline_mgr = $row['isline_mgr'];
      $line_mgr = $row['line_mgr'];
      $branch = $row['branch'];
      $employment_type = $row['employment_type'];
      $confirmation_status = $row['confirmation_status'];
      $confirmation_date = $row['confirmation_date'];
      $annual_sal = number_format($row['annual_sal'], 2);
      $tin = $row['tin'];
      $bank_name = $row['bank_name'];
      $account_no = $row['account_no'];
      $pfa = $row['pfa'];
      $pfa_pin = $row['pfa_pin'];
      $leave_status = $row['leave_status'];
      $exit_type = $row['exit_type'];
      $exit_date = $row['exit_date'];
      $date_added = $row['date_added'];
      $annual_leave_taken = $row['annual_leave_taken'];

      $employee_name = $lastname . " " . $firstname;

      echo "<tr>
                    <td>
                        <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$emp_id><i class='fa fa-search'></i></button>
                    </td>                     
                    <td>$staff_no</td>
                    <td>$employee_name</td> 
                    <td>$gender</td>
                    <td>$nationality</td>
                    <td style='display:none;'>$dob</td>
                    <td style='display:none;'>$marital_status</td>
                    <td style='display:none;'>$no_of_kids</td>
                    <td style='display:none;'>$home_address</td>
                    <td style='display:none;'>$private_phone</td>
                    <td style='display:none;'>$private_email</td>
                    <td style='display:none;'>$next_of_kin</td>
                    <td style='display:none;'>$kin_relationship</td>
                    <td style='display:none;'>$nok_phone</td>
                    <td style='display:none;'>$nok_address</td>
                    <td>$qualification</td>
                    <td>$discipline</td>
                    <td style='display:none;'>$other_qual</td>
                    <td style='display:none;'>$employ_status</td>
                    <td>$hire_date</td>
                    <td>$official_email</td>
                    <td>$cug_line</td>
                    <td>$department</td>
                    <td>$designation</td>
                    <td>$job_level</td>
                    <td style='display:none;'>$isline_mgr</td>
                    <td style='display:none;'>$line_mgr</td>
                    <td>$branch</td>
                    <td>$employment_type</td>
                    <td style='display:none;'>$confirmation_status</td>
                    <td style='display:none;'>$confirmation_date</td>
                    <td style='text-align:right'>$annual_sal</td>
                    <td style='display:none;'>$tin</td>
                    <td style='display:none;'>$bank_name</td>
                    <td style='display:none;'>$account_no</td>
                    <td style='display:none;'>$pfa</td>
                    <td style='display:none;'>$pfa_pin</td>
                    <td style='display:none;'>$leave_status</td>
                    <td style='display:none;'>$exit_type</td>
                    <td style='display:none;'>$exit_date</td>
                    <td>$date_added</td>
                    <td style='display:none;'>$annual_leave_taken</td>                                         
                  </tr>";
    }
  } else {
    echo "<tr> <td>No records available </td></tr>";
  }
}

//This function creates new employee
function createEmployee()
{

  $msg = new \Plasticbrain\FlashMessages\FlashMessages();

  global $conn;

  $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

  $staff_no = mysqli_real_escape_string($conn, $_POST['staff_no']);
  $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
  $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
  $other_names = '';
  // $other_names = mysqli_real_escape_string($conn, $_POST['other_names']);
  $gender = mysqli_real_escape_string($conn, $_POST['gender']);
  $nationality = mysqli_real_escape_string($conn, $_POST['nationality']);
  $dob = mysqli_real_escape_string($conn, $_POST['dob']);
  $marital_status = mysqli_real_escape_string($conn, $_POST['marital_status']);
  $no_of_kids = mysqli_real_escape_string($conn, $_POST['no_of_kids']);
  $home_address = mysqli_real_escape_string($conn, $_POST['home_address']);
  $private_phone = mysqli_real_escape_string($conn, $_POST['private_phone']);
  $private_email = mysqli_real_escape_string($conn, $_POST['private_email']);
  $next_of_kin = mysqli_real_escape_string($conn, $_POST['next_of_kin']);
  $kin_relationship = mysqli_real_escape_string($conn, $_POST['kin_relationship']);
  $nok_phone = mysqli_real_escape_string($conn, $_POST['nok_phone']);
  $nok_address = mysqli_real_escape_string($conn, $_POST['nok_address']);
  $qualification = mysqli_real_escape_string($conn, $_POST['qualification']);
  $discipline = mysqli_real_escape_string($conn, $_POST['discipline']);
  $other_qual = mysqli_real_escape_string($conn, $_POST['other_qual']);
  $employ_status = 'Active';
  $hire_date = mysqli_real_escape_string($conn, $_POST['hire_date']);
  $department = mysqli_real_escape_string($conn, $_POST['department']);
  $official_email = mysqli_real_escape_string($conn, $_POST['official_email']);
  $cug_line = mysqli_real_escape_string($conn, $_POST['cug_line']);
  $designation = mysqli_real_escape_string($conn, $_POST['designation']);
  $job_level = mysqli_real_escape_string($conn, $_POST['job_level']);
  $line_mgr = mysqli_real_escape_string($conn, $_POST['line_mgr']);
  $branch = mysqli_real_escape_string($conn, $_POST['branch']);
  $employment_type = mysqli_real_escape_string($conn, $_POST['employment_type']);
  $annual_sal = mysqli_real_escape_string($conn, $_POST['annual_sal']);
  $tin = mysqli_real_escape_string($conn, $_POST['tin']);
  $bank_name = mysqli_real_escape_string($conn, $_POST['bank_name']);
  $account_no = mysqli_real_escape_string($conn, $_POST['account_no']);
  $pfa = mysqli_real_escape_string($conn, $_POST['pfa']);
  $pfa_pin = mysqli_real_escape_string($conn, $_POST['pfa_pin']);
  $isline_mgr = mysqli_real_escape_string($conn, $_POST['isline_mgr']);
  $annual_sal = str_replace(",", "", $annual_sal);


  //Begin Transaction Mode
  mysqli_autocommit($conn, false);
  $flag = true;

  //Insert Records into Employees Table
  $sql = "INSERT INTO employees(staff_no,lastname, firstname, other_names, gender, nationality, dob, marital_status, no_of_kids, home_address, private_phone, private_email, next_of_kin, kin_relationship, nok_phone, nok_address, qualification, discipline, other_qual, employ_status, hire_date, department, official_email, cug_line, designation, job_level, line_mgr, branch, employment_type, annual_sal, tin, bank_name, account_no, pfa, pfa_pin, date_added,
  isline_mgr) VALUES ('$staff_no','$lastname','$firstname','$other_names','$gender','$nationality','$dob','$marital_status','$no_of_kids','$home_address','$private_phone','$private_email','$next_of_kin','$kin_relationship','$nok_phone','$nok_address','$qualification','$discipline','$other_qual','$employ_status','$hire_date','$department','$official_email','$cug_line','$designation','$job_level','$line_mgr','$branch','$employment_type','$annual_sal','$tin','$bank_name','$account_no','$pfa','$pfa_pin',curdate(),'$isline_mgr')";
  $result = mysqli_query($conn, $sql);

  //Set Flag to false if Insert action fails
  if (!$result) {
    $flag = false;
    // echo "Error details: " . mysqli_error($conn) . ".";
    // $msg->error('Oops! an error occured. Please try again later' . mysqli_error($conn) . '');
  } else {
    $last_id = mysqli_insert_id($conn);
    $random_password = generateRandomPassword(10);    
    $password =  password_hash($random_password, PASSWORD_DEFAULT);

    //Compose Email Message
    $mailMessage = "Your account has been created on Ace!HRM. Your username is $official_email and your password is <b>$random_password</b>. You will be required to change your password upon your first login. Click on the button below to login";
    $mailSubject = "Welcome! Your account has ben created on Ace! HRM";
  }


  //Create User account for employee
  $sql2 = "INSERT INTO users(emp_id,lastname, firstname,email, password, user_role, level_id, activation_status) VALUES ('$last_id','$lastname','$firstname','$official_email','$password','Staff',1,'Y')";
  $result2 = mysqli_query($conn, $sql2);

  //Set Flag to false if Update action fails
  if (!$result2) {
    $flag = false;
    // echo "Error details 2: " . mysqli_error($conn) . ".";
  }


  //Commit Changes to Database if no errors occured
  if ($flag) {
    //Sve to Database
    mysqli_commit($conn);
    //Send Email Notification
    sendMailerMe($official_email, $mailSubject, $lastname . ' ' . $firstname, $mailMessage);
    //Display Success Notification on screen
    $msg->success('Great! New Employee added', 'employee_records.php');
    unset($_POST);
    $_POST = array();
  } else {
    mysqli_rollback($conn);
    $msg->error('Oops! an error occured. Please try again later');
  }
}


//Update an employee's records
function editEmployee($id)
{
  $msg = new \Plasticbrain\FlashMessages\FlashMessages();

  global $conn;
  
  $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

  $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
  $staff_no = mysqli_real_escape_string($conn, $_POST['staff_no']);
  $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
  $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
  $gender = mysqli_real_escape_string($conn, $_POST['gender']);
  $nationality = mysqli_real_escape_string($conn, $_POST['nationality']);
  $dob = mysqli_real_escape_string($conn, $_POST['dob']);
  $marital_status = mysqli_real_escape_string($conn, $_POST['marital_status']);
  $no_of_kids = mysqli_real_escape_string($conn, $_POST['no_of_kids']);
  $home_address = mysqli_real_escape_string($conn, $_POST['home_address']);
  $private_phone = mysqli_real_escape_string($conn, $_POST['private_phone']);
  $private_email = mysqli_real_escape_string($conn, $_POST['private_email']);
  $next_of_kin = mysqli_real_escape_string($conn, $_POST['next_of_kin']);
  $kin_relationship = mysqli_real_escape_string($conn, $_POST['kin_relationship']);
  $nok_phone = mysqli_real_escape_string($conn, $_POST['nok_phone']);
  $nok_address = mysqli_real_escape_string($conn, $_POST['nok_address']);
  $qualification = mysqli_real_escape_string($conn, $_POST['qualification']);
  $discipline = mysqli_real_escape_string($conn, $_POST['discipline']);
  $other_qual = mysqli_real_escape_string($conn, $_POST['other_qual']);
  $employ_status = mysqli_real_escape_string($conn, $_POST['employ_status']);
  $hire_date = mysqli_real_escape_string($conn, $_POST['hire_date']);
  $department = mysqli_real_escape_string($conn, $_POST['department']);
  $official_email = mysqli_real_escape_string($conn, $_POST['official_email']);
  $cug_line = mysqli_real_escape_string($conn, $_POST['cug_line']);
  $designation = mysqli_real_escape_string($conn, $_POST['designation']);
  $job_level = mysqli_real_escape_string($conn, $_POST['job_level']);
  $line_mgr = mysqli_real_escape_string($conn, $_POST['line_mgr']);
  $branch = mysqli_real_escape_string($conn, $_POST['branch']);
  $employment_type = mysqli_real_escape_string($conn, $_POST['employment_type']);
  $annual_sal = mysqli_real_escape_string($conn, $_POST['annual_sal']);
  $tin = mysqli_real_escape_string($conn, $_POST['tin']);
  $bank_name = mysqli_real_escape_string($conn, $_POST['bank_name']);
  $account_no = mysqli_real_escape_string($conn, $_POST['account_no']);
  $pfa = mysqli_real_escape_string($conn, $_POST['pfa']);
  $pfa_pin = mysqli_real_escape_string($conn, $_POST['pfa_pin']);
  $isline_mgr = mysqli_real_escape_string($conn, $_POST['isline_mgr']);

  $annual_sal = str_replace(",", "", $annual_sal);

  $sql = "UPDATE employees SET staff_no = '$staff_no', lastname = '$lastname', firstname = '$firstname', gender = '$gender', nationality = '$nationality', dob = '$dob', marital_status = '$marital_status', no_of_kids = '$no_of_kids', home_address = '$home_address', private_phone = '$private_phone', private_email = '$private_email', next_of_kin = '$next_of_kin', kin_relationship = '$kin_relationship', nok_phone = '$nok_phone', nok_address = '$nok_address', qualification = '$qualification', discipline = '$discipline', other_qual = '$other_qual', employ_status = '$employ_status', hire_date = '$hire_date', official_email = '$official_email', cug_line = '$cug_line', department = '$department', designation = '$designation', job_level = '$job_level', isline_mgr = '$isline_mgr', line_mgr = '$line_mgr', branch = '$branch', employment_type = '$employment_type', annual_sal = $annual_sal, tin = '$tin', bank_name = '$bank_name', account_no = '$account_no', pfa = '$pfa', pfa_pin = '$pfa_pin' WHERE emp_id = $id";
  $result = mysqli_query($conn, $sql);



  if ($result) {

    if ($employ_status <> 'Active') {
      $sql2 = "UPDATE users SET activation_status = 'N' WHERE emp_id = $emp_id";
      $result2 = mysqli_query($conn, $sql2);
    }
    $msg->success('Great! Record updated successfully!', 'employee_records.php');
  } else {
    $msg->error('Oops! An error occured.');
  }
}


//Load an employee's record
function viewEmployee($id)
{

  global $conn;

  $sql = "SELECT * from employees WHERE emp_id = $id";
  $result =  mysqli_query($conn, $sql);
  $row = mysqli_fetch_assoc($result);

  $values = array();

  if ($row) {

    $values['emp_id'] = $row['emp_id'];
    $values['staff_no'] = $row['staff_no'];
    $values['lastname'] = $row['lastname'];
    $values['firstname'] = $row['firstname'];
    $values['gender'] = $row['gender'];
    $values['nationality'] = $row['nationality'];
    $values['dob'] = $row['dob'];
    $values['marital_status'] = $row['marital_status'];
    $values['no_of_kids'] = $row['no_of_kids'];
    $values['home_address'] = $row['home_address'];
    $values['private_phone'] = $row['private_phone'];
    $values['private_email'] = $row['private_email'];
    $values['next_of_kin'] = $row['next_of_kin'];
    $values['kin_relationship'] = $row['kin_relationship'];
    $values['nok_phone'] = $row['nok_phone'];
    $values['nok_address'] = $row['nok_address'];
    $values['qualification'] = $row['qualification'];
    $values['discipline'] = $row['discipline'];
    $values['other_qual'] = $row['other_qual'];
    $values['employ_status'] = $row['employ_status'];
    $values['hire_date'] = $row['hire_date'];
    $values['official_email'] = $row['official_email'];
    $values['cug_line'] = $row['cug_line'];
    $values['department'] = $row['department'];
    $values['designation'] = $row['designation'];
    $values['job_level'] = $row['job_level'];
    $values['isline_mgr'] = $row['isline_mgr'];
    $values['line_mgr'] = $row['line_mgr'];
    $values['branch'] = $row['branch'];
    $values['employment_type'] = $row['employment_type'];
    $values['confirmation_status'] = $row['confirmation_status'];
    $values['confirmation_date'] = $row['confirmation_date'];
    $values['annual_sal'] = number_format($row['annual_sal'], 2);
    $values['annual_sal'] = number_format($row['annual_sal'], 2);
    $values['tin'] = $row['tin'];
    $values['bank_name'] = $row['bank_name'];
    $values['account_no'] = $row['account_no'];
    $values['pfa'] = $row['pfa'];
    $values['pfa_pin'] = $row['pfa_pin'];
    $values['leave_status'] = $row['leave_status'];
    $values['exit_type'] = $row['exit_type'];
    $values['exit_date'] = $row['exit_date'];
    $values['date_added'] = $row['date_added'];
    $values['annual_leave_taken'] = $row['annual_leave_taken'];
  }
  echo json_encode($values);
}
