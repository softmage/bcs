<?php

if (session_id() == "") session_start();
ob_start();

//Redirect User to Login Page if session has not started
if (empty($_SESSION['user_id'])) {
   header("Location:login.php");
}


//Set Global Variables from Session Values
if (isset($_SESSION['user_id'])) {

   $user_id = $_SESSION['user_id'];
   $user_fullname = $_SESSION['full_name'];
   $userlevel = $_SESSION['user_level'];
   $user_email = $_SESSION['email'];
   $user_profpic = $_SESSION['profpic'];
   $userbranch = $_SESSION['branch'];
   $userfirstname = $_SESSION['firstname'];
}


//Get List of PFAs
function getPFA()
{

   global $conn;

   $sql = "SELECT * from pfa ORDER BY fund_admin ASC";
   $result = mysqli_query($conn, $sql);

   $pfas = mysqli_fetch_all($result, MYSQLI_ASSOC);

   foreach ($pfas as $pfa) {

      echo "<option>" . $pfa['fund_admin'] . "</option>";
   }
}


//Get List of Line Managers
function getLineManagers()
{

   global $conn;

   $sql = "SELECT emp_id, CONCAT(lastname, ' ', firstname) AS mgr_name from employees WHERE isline_mgr = 'Yes' ORDER BY lastname ASC";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   foreach ($results as $result) {
      $mgr_id = $result['emp_id'];
      $mgr_name = $result['mgr_name'];
      echo "<option value='$mgr_id'> $mgr_name </option>";
   }
}


//Get List of Departments
function getallDepartments()
{

   global $conn;

   $sql = "SELECT dept from departments ORDER BY dept ASC";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   foreach ($results as $result) {
      $dept = $result['dept'];
      echo "<option> $dept </option>";
   }
}


//Get List of Designations
function getallDesignation()
{

   global $conn;

   $sql = "SELECT designation from designation ORDER BY designation ASC";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   foreach ($results as $result) {
      $designation = $result['designation'];
      echo "<option> $designation </option>";
   }
}


//Get List of Branches
function getallBranches()
{

   global $conn;

   $sql = "SELECT branch from branches ORDER BY branch ASC";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   foreach ($results as $result) {
      $branch = $result['branch'];
      echo "<option> $branch </option>";
   }
}


//Get Current Open Period used in Payroll Generation and Attendance Management
function getCurrentPeriod()
{

   global $conn;

   $sql = "SELECT * FROM period WHERE status = 'Open' LIMIT 1";
   $result = mysqli_query($conn, $sql);
   $results = mysqli_fetch_all($result, MYSQLI_ASSOC);

   return $results;
}

 
$period_data = getCurrentPeriod();

foreach ($period_data as $period_data) {
      $current_period_id = $period_data['id'];
      $current_period_name = $period_data['period_name'];
   }


//Get List of All Periods
function getAllPeriods()
{
    $output = '';
    global $conn;
 
    $sql = "SELECT * FROM period";
    $result = mysqli_query($conn, $sql);
    $rowCount = mysqli_num_rows($result);

    if ($rowCount > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $output  .= '<option value="'.$row['id'].'">' . $row['period_name'] . '</option>';
        }
    }  

    echo $output;
}