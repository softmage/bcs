<?php

require_once 'FlashMessages.php';
require_once 'Emailer.php';

$msg = new \Plasticbrain\FlashMessages\FlashMessages();

/* LEAVE REQUEST HELPERS */

//Check if User has pending Leave Request
function hasPendingLeave($conn, $emp_id)
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   $query = "SELECT emp_id FROM employees WHERE emp_id = $emp_id AND leave_cycle <> 'Completed' LIMIT 1";
   $res = mysqli_query($conn, $query);
   $rowcount  = mysqli_num_rows($res);

   if ($rowcount > 0) {
      $msg->error('Oops! You have pending leave');
      return FALSE;
   } else {
      return TRUE;
   }
}

//Function to get recipient's name
function getRecipientName($emp_id)
{
    global $conn;

    $sql = "SELECT CONCAT(last_name,' ',first_name) AS recipient_name FROM users WHERE emp_id = $emp_id";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($result)) {
        $res = $row['recipient_name'];
    }

    return $res;
}

//Function to get recipient's email address from Users table
function getRecipientEmail($emp_id)
{
    global $conn;

    $sql = "SELECT email FROM users WHERE emp_id = $emp_id";
    $result = mysqli_query($conn, $sql);
    while ($row = mysqli_fetch_array($result)) {
        $recipient_email = $row['email'];
    }

    return $recipient_email;
}


//Validate leave request entries
function validateLeave($commence_date, $end_date, $resumption_date, $emp_id, $line_mgr_id, $relief_officer)
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   //Leave Validation
   if (strtotime($commence_date) > strtotime($end_date)) {
      $msg->error('Leave End date MUST be greater than the Leave Start Date');
      return FALSE;
   } elseif (strtotime($end_date) > strtotime($resumption_date)) {
      $msg->error('Resumption Date must be greater than Leave End date');
      return FALSE;
   } elseif ($emp_id == $line_mgr_id) {
      $msg->error('You cannot choose yourself as your Line Manager');
      return FALSE;
   } elseif ($emp_id == $relief_officer) {
      $msg->error('You cannot choose yourself as your Relief Officer');
      return FALSE;
   } else {
      return TRUE;
   }
}

//Check if User has available leave days
function checkAvailableLeave($conn, $emp_id, $requested_days)
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   $query = "SELECT annual_leave_taken FROM employees WHERE emp_id = $emp_id LIMIT 1";
   $res = mysqli_query($conn, $query);
   $result = mysqli_fetch_all($res, MYSQLI_ASSOC);

   foreach ($result as $result) {
      $usedleave = $result['annual_leave_taken'];
      $newrequest = number_format($usedleave + $requested_days);
   }

   if ($usedleave > 19) {
      $msg->error('Oops! You have exhausted your Annual Leave. Kindly request for a Casual Leave');
      $error_state = true;

      return FALSE;
   } elseif ($newrequest > 20) {
      $leave_balance = (20 - $usedleave);
      $msg->error('Oops! You only have ' . $leave_balance . ' leave days left');
      return FALSE;
   } else {
      return TRUE;
   }
}

//Load leave details
function viewLeaveRequest($id)
{

   global $conn;
   $sql = "SELECT * FROM leave_summary WHERE leave_id = $id";
   $result =  mysqli_query($conn, $sql);
   $row = mysqli_fetch_assoc($result);

   $values = array();

   if ($row) {

      $values['leave_id'] = $row['leave_id'];
      $values['emp_id'] = $row['emp_id'];
      $values['employee_name'] = $row['employee_name'];
      $values['leave_status'] = $row['leave_status'];
      $values['leave_type'] = $row['leave_type'];
      $values['requested_days'] = $row['requested_days'];
      $values['commence_date'] = $row['commence_date'];
      $values['end_date'] = $row['end_date'];
      $values['resumption_date'] = $row['resumption_date'];
      $values['remarks'] = $row['remarks'];
      $values['relief_officer'] = $row['relief_officer'];
      $values['relief_officer_name'] = $row['relief_officer_name'];
      $values['request_date'] = $row['request_date'];
      $values['leave_upload'] = $row['leave_upload'];
      $values['line_mgr_id'] = $row['line_mgr_id'];
      $values['linemgr_name'] = $row['linemgr_name'];
      $values['linemgr_review'] = $row['linemgr_review'];
      $values['linemgr_days'] = $row['linemgr_days'];
      $values['line_mgr_rmks'] = $row['line_mgr_rmks'];
      $values['leave_reviewer'] = $row['leave_reviewer'];
      $values['linemgr_review_date'] = $row['linemgr_review_date'];
      $values['hr_approval'] = $row['hr_approval'];
      $values['hr_days'] = $row['hr_days'];
      $values['hr_leavecommence_date'] = $row['hr_leavecommence_date'];
      $values['hr_resume_date'] = $row['hr_resume_date'];
      $values['hr_remarks'] = $row['hr_remarks'];
      $values['hr_approval_date'] = $row['hr_approval_date'];
      $values['cancellation_reasons'] = $row['cancellation_reasons'];
   }
   echo json_encode($values);
}

//Cancel Leave Request
function  cancelLeaveRequest()
{

   global $conn;

   // $error_state = false;

   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   //Assign form details to variables 
   $leave_id = mysqli_real_escape_string($conn, $_POST['leave_id']);
   $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
   $hr_approval = mysqli_real_escape_string($conn, $_POST['hr_approval']);
   $cancellation_reasons = mysqli_real_escape_string($conn, $_POST['cancellation_reasons']);
   $leave_status = 'Cancelled by HR';
   $hr_approval_date = date('Y-m-d');

 
   $recipientEmail = getRecipientEmail($emp_id);
   $recipientName = getRecipientName($emp_id);

   //Compose Email Message
   $mailMessage = "Your Leave Request has been cancelled by the HR Dept. For further information, kindly reach out to your HR Manager or log on to Ace!HRM. Click on the button below to login";
   $mailSubject = "Leave Request has been cancelled by your HR Manager";


   //INSERT record in Leave Table
   $sql = "UPDATE leave_master, employees SET hr_approval = '$hr_approval', cancellation_reasons = '$cancellation_reasons', leave_master.leave_status = '$leave_status', hr_approval_date = '$hr_approval_date', employees.leave_cycle = 'Completed' WHERE leave_id = $leave_id AND employees.emp_id = $emp_id";
   $result = mysqli_query($conn, $sql);

   if ($result) {
      //Send Email Notification to Employee
      sendMailerMe($recipientEmail, $mailSubject, $recipientName, $mailMessage);
      $msg->success('Great! Leave Request Cancelled successfully', 'unconfirmed_leave.php');
      unset($_POST);
      $_POST = array();
   } else {
      $msg->error('Oops! an error occured. Please try again later' . mysqli_error($conn));
   }
}

/* LEAVE REQUEST */

//Request Annual Leave

function requestLeave()
{

   global $conn;

   // $error_state = false;
   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   // $leave_id = mysqli_real_escape_string($conn, $_POST['leave_id']);
   $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
   // $leave_status = mysqli_real_escape_string($conn, $_POST['leave_status']);
   $leave_status = 'Pending Review';
   $leave_type = 'Annual Leave     ';
   // $leave_type = mysqli_real_escape_string($conn, $_POST['leave_type']);
   $requested_days = mysqli_real_escape_string($conn, $_POST['requested_days']);
   $commence_date = mysqli_real_escape_string($conn, $_POST['commence_date']);
   $end_date = mysqli_real_escape_string($conn, $_POST['end_date']);
   $resumption_date = mysqli_real_escape_string($conn, $_POST['resumption_date']);
   $remarks = mysqli_real_escape_string($conn, $_POST['remarks']);
   $relief_officer = mysqli_real_escape_string($conn, $_POST['relief_officer']);
   $line_mgr_id = mysqli_real_escape_string($conn, $_POST['line_mgr_id']);
   $request_date = date('Y-m-d');


   if (
      hasPendingLeave($conn, $emp_id) && checkAvailableLeave($conn, $emp_id, $requested_days)
      && validateLeave($commence_date, $end_date, $resumption_date, $emp_id, $line_mgr_id, $relief_officer)
   ) {


      $LineMgrEmail = getRecipientEmail($line_mgr_id);
      $LineMgrName = getRecipientName($line_mgr_id);
      $empName = getRecipientName($emp_id);
   
      //Compose Email Message
      $mailMessage = "A Leave Request has been submitted by $empName and is awaiting your approval. Kindly log on to Ace! HRM to review this request. Click on the button below to login";
      $mailSubject = "You have a Leave Request to Review";   

      //Begin Transaction Mode
      mysqli_autocommit($conn, false);
      $flag = true;

      //INSERT record in Leave Table
      $sql = "INSERT INTO leave_master(emp_id,leave_status,leave_type,requested_days,commence_date,end_date,resumption_date,remarks,relief_officer,line_mgr_id, request_date) VALUES ('$emp_id','$leave_status','$leave_type','$requested_days','$commence_date','$end_date','$resumption_date', '$remarks','$relief_officer','$line_mgr_id','$request_date')";

      $result = mysqli_query($conn, $sql);

      //Set Flag to false if Insert action fails
      if (!$result) {
         $flag = false;
         echo "Error details: " . mysqli_error($conn) . ".";
      }


      //UPDATE Leave Cycle on Employee's Table
      $sql2 = "UPDATE employees SET leave_cycle = 'Initiated' WHERE emp_id = $emp_id";
      $result2 = mysqli_query($conn, $sql2);

      //Set Flag to false if Update action fails
      if (!$result2) {
         $flag = false;
         echo "Error details 2: " . mysqli_error($conn) . ".";
      }


      //Commit Changes to Database if no errors occured
      if ($flag) {
         //Sve to Database
         mysqli_commit($conn);
         //Send Email Notification
         sendMailerMe($LineMgrEmail, $mailSubject, $LineMgrName, $mailMessage);
         //Display Success Notification on screen
         $msg->success('Great! Leave Request Submitted', 'myleave_summary.php');
         unset($_POST);
         $_POST = array();
      } else {
         mysqli_rollback($conn);
         $msg->error('Oops! an error occured. Please try again later');
      }
   }
}

function myLeaveSummary($id)
{
   global $conn;

   $sql = "SELECT * FROM leave_summary WHERE emp_id = $id";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $leave_id = $row['leave_id'];
         $emp_id = $row['emp_id'];
         $employee_name = $row['employee_name'];
         $leave_status = $row['leave_status'];
         $leave_type = $row['leave_type'];
         $requested_days = $row['requested_days'];
         $commence_date = $row['commence_date'];
         $end_date = $row['end_date'];
         $resumption_date = $row['resumption_date'];
         $remarks = $row['remarks'];
         $relief_officer = $row['relief_officer'];
         $relief_officer_name = $row['relief_officer_name'];
         $request_date = $row['request_date'];
         $leave_upload = $row['leave_upload'];
         $line_mgr_id = $row['line_mgr_id'];
         $linemgr_name = $row['linemgr_name'];
         $linemgr_review = $row['linemgr_review'];
         $linemgr_days = $row['linemgr_days'];
         $line_mgr_rmks = $row['line_mgr_rmks'];
         $leave_reviewer = $row['leave_reviewer'];
         $linemgr_review_date = $row['linemgr_review_date'];
         $hr_approval = $row['hr_approval'];
         $hr_days = $row['hr_days'];
         $hr_leavecommence_date = $row['hr_leavecommence_date'];
         $hr_resume_date = $row['hr_resume_date'];
         $hr_remarks = $row['hr_remarks'];
         $hr_approval_date = $row['hr_approval_date'];
         $cancellation_reasons = $row['cancellation_reasons'];

         echo "<tr>
                  <td>
                     <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$leave_id><i class='fa fa-search'></i></button>
                  </td>                  
                  <td>$leave_id</td>
                  <td>$employee_name</td>
                  <td>$leave_status</td>
                  <td>$leave_type</td>
                  <td>$requested_days</td>
                  <td>$commence_date</td>
                  <td style='display:none;'>$end_date</td>
                  <td>$resumption_date</td>
                  <td style='display:none;'>$remarks</td>
                  <td>$relief_officer_name</td>
                  <td>$request_date</td>
                  <td style='display:none;'>$leave_upload</td>
                  <td>$linemgr_name</td>
                  <td>$linemgr_review</td>
                  <td>$linemgr_days</td>
                  <td style='display:none;'>$line_mgr_rmks</td>
                  <td>$leave_reviewer</td>
                  <td>$linemgr_review_date</td>
                  <td>$hr_approval</td>
                  <td>$hr_days</td>
                  <td style='display:none;'>$hr_leavecommence_date</td>
                  <td style='display:none;'$hr_resume_date</td>
                  <td style='display:none;'>$hr_remarks</td>
                  <td>$hr_approval_date</td>
                  <td style='display:none;'>$cancellation_reasons</td>                                       
               </tr>";
      }
   } else {
      echo "<tr> <td>No records available </td></tr>";
   }
}


/* LEAVE REQUEST APPROVAL */

function getUnreviewedLeave($id)
{
   global $conn;
   $sql = "SELECT * FROM leave_summary WHERE linemgr_review = 'Pending Review' AND line_mgr_id = $id ";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);


   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $leave_id = $row['leave_id'];
         $emp_id = $row['emp_id'];
         $employee_name = $row['employee_name'];
         $leave_status = $row['leave_status'];
         $leave_type = $row['leave_type'];
         $requested_days = $row['requested_days'];
         $commence_date = $row['commence_date'];
         $end_date = $row['end_date'];
         $resumption_date = $row['resumption_date'];
         $remarks = $row['remarks'];
         $relief_officer = $row['relief_officer'];
         $relief_officer_name = $row['relief_officer_name'];
         $request_date = $row['request_date'];
         $leave_upload = $row['leave_upload'];
         $line_mgr_id = $row['line_mgr_id'];
         $linemgr_name = $row['linemgr_name'];

         echo "<tr>
                  <td>                    
                     <button class='view btn btn-xs btn-danger' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$leave_id><i class='fa fa-edit'></i></button>
                  </td>
                  <td>$leave_id</td>
                  <td>$emp_id</td>
                  <td>$employee_name</td>
                  <td>$leave_status</td>
                  <td>$leave_type</td>
                  <td>$requested_days</td>
                  <td>$commence_date</td>
                  <td>$end_date</td>
                  <td>$resumption_date</td>
                  <td style='display:none;'>$remarks</td>
                  <td>$relief_officer_name</td>
                  <td>$request_date</td>
                  <td style='display:none;'>$leave_upload</td>
                  <td>$linemgr_name</td> 
               </tr>";
      }
   } else {
      echo "<tr>
            <td>No records available </td>
         </tr>";
   }
}


function reviewLeaveRequest()
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;
   global $user_fullname;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   /* This block fetches values from the FORM and escapes them  */
   $leave_id = mysqli_real_escape_string($conn, $_POST['leave_id']);
   $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
   $leave_status = mysqli_real_escape_string($conn, $_POST['leave_status']);
   $requested_days = mysqli_real_escape_string($conn, $_POST['requested_days']);
   $linemgr_review = mysqli_real_escape_string($conn, $_POST['linemgr_review']);
   $linemgr_days = mysqli_real_escape_string($conn, $_POST['linemgr_days']);
   $line_mgr_rmks = mysqli_real_escape_string($conn, $_POST['line_mgr_rmks']);
   $leave_reviewer = $user_fullname;
   $linemgr_review_date = date('Y-m-d');

   /* This block sets the Leave Status field based on the Line Manager's Approval  */
   if ($linemgr_review == 'Approved') {
      $leave_status = 'Pending HR Confirmation';
   } elseif ($linemgr_review == 'Rejected') {
      $leave_status = 'Rejected by LineMgr';
   }

   /* This block validates the form inputs.  */
   if ($linemgr_days > $requested_days) {
      $msg->error('Oops! You cannot approve more days than what was requested');
      return FALSE;
   } elseif (($linemgr_review == 'Rejected') && $linemgr_days > 0) {
      $msg->error('Oops! Please set appoved Days field to 0 if leave is not approved');
      return FALSE;
   } elseif (($linemgr_review == 'Approved') && $linemgr_days < 1) {
      $msg->error('Oops! Please enter value for Appoved Days.');
      return FALSE;
   }


   $recipientEmail = getRecipientEmail($emp_id);
   $recipientName = getRecipientName($emp_id);

   //Compose Email Message
   $mailMessage = "Your Leave Request has been $linemgr_review. Kindly log on to Ace! HRM to view more details about this request. Click on the button below to login";
   $mailSubject = "Your Leave Request has been Reviewed by your LineMgr";   


   /* This block writes the data to the dabase if there are no validation errors  */

   //Begin Transaction Mode
   mysqli_autocommit($conn, false);
   $flag = true;

   //Update Line Mgr review to database table
   $sql = "UPDATE leave_master SET leave_status = '$leave_status', linemgr_review = '$linemgr_review', linemgr_days = '$linemgr_days', line_mgr_rmks = '$line_mgr_rmks', leave_reviewer = '$leave_reviewer', linemgr_review_date = '$linemgr_review_date' WHERE leave_id = $leave_id AND emp_id = $emp_id";
   $result = mysqli_query($conn, $sql);

   //Set Flag to false if Update action fails
   if (!$result) {
      $flag = false;
   }


   //Clear Leave Cycle if Leave Request was rejected. This enables employee to raise another request
   if ($linemgr_review == 'Rejected') {
      $sql2 = "UPDATE employees SET leave_cycle = 'Completed' WHERE emp_id = $emp_id";
      $result2 = mysqli_query($conn, $sql);

      //Set Flag to false if Update action fails
      if (!$result2) {
         $flag = false;
      }
   }


   //Commit Changes to Database if no errors occured
   if ($flag) {
      //Save to Database
      mysqli_commit($conn);

       //Send Email Notification
       sendMailerMe($recipientEmail, $mailSubject, $recipientName, $mailMessage);
       
      //Display Notification Message on screen
      $msg->success('Great! Update succeded', 'leave_review.php');
      unset($_POST);
      $_POST = array();
   } else {
      mysqli_rollback($conn);
      $msg->error('Oops! Something went wrong. Please try again');
   }
}



/* LEAVE CONFIRMATION */

function getUnconfirmedLeave()
{
   global $conn;

   $sql = "SELECT * FROM leave_summary WHERE leave_status = 'Pending HR Confirmation'";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);


   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $leave_id = $row['leave_id'];
         $emp_id = $row['emp_id'];
         $employee_name = $row['employee_name'];
         $leave_status = $row['leave_status'];
         $leave_type = $row['leave_type'];
         $requested_days = $row['requested_days'];
         $commence_date = $row['commence_date'];
         $end_date = $row['end_date'];
         $resumption_date = $row['resumption_date'];
         $remarks = $row['remarks'];
         $relief_officer = $row['relief_officer'];
         $relief_officer_name = $row['relief_officer_name'];
         $request_date = $row['request_date'];
         $leave_upload = $row['leave_upload'];
         $line_mgr_id = $row['line_mgr_id'];
         $linemgr_name = $row['linemgr_name'];
         $linemgr_review = $row['linemgr_review'];
         $linemgr_days = $row['linemgr_days'];
         $line_mgr_rmks = $row['line_mgr_rmks'];
         $leave_reviewer = $row['leave_reviewer'];
         $linemgr_review_date = $row['linemgr_review_date'];

         echo "<tr>
                  <td style='width: 20%'>
                     <a href='leave_confirmation.php?id=$leave_id' class='btn btn-primary btn-xs'><i class='fa fa-check'></i> Confirm </a>
                     <button class='view btn btn-xs btn-danger' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$leave_id><i class='fa fa-trash'></i>&nbsp; Cancel</button>
                  </td>
                  <td>$leave_id</td>
                  <td>$emp_id</td>
                  <td>$employee_name</td>
                  <td>$leave_status</td>
                  <td>$leave_type</td>
                  <td>$requested_days</td>
                  <td>$commence_date</td>
                  <td style='display:none;'>$end_date</td>
                  <td>$resumption_date</td>
                  <td style='display:none;'>$remarks</td>
                  <td>$relief_officer_name</td>
                  <td>$request_date</td>
                  <td style='display:none;'>$leave_upload</td>
                  <td>$linemgr_name</td>
                  <td>$linemgr_review</td>
                  <td>$linemgr_days</td>
                  <td style='display:none;'>$line_mgr_rmks</td>
                  <td>$leave_reviewer</td>
                  <td>$linemgr_review_date</td>
               </tr>";
      }
   } else {
      echo "<tr>
            <td>No records available </td>
         </tr>";
   }
}


function confirmLeaveRequest($id)
{
   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   /* This block fetches values from the FORM and escapes them */
   $leave_id = mysqli_real_escape_string($conn, $_POST['leave_id']);
   $emp_id = mysqli_real_escape_string($conn, $_POST['emp_id']);
   $linemgr_days = mysqli_real_escape_string($conn, $_POST['linemgr_days']);
   $hr_approval = mysqli_real_escape_string($conn, $_POST['hr_approval']);
   $hr_days = mysqli_real_escape_string($conn, $_POST['hr_days']);
   $hr_leavecommence_date = mysqli_real_escape_string($conn, $_POST['hr_leavecommence_date']);
   $hr_resume_date = mysqli_real_escape_string($conn, $_POST['hr_resume_date']);
   $hr_remarks = mysqli_real_escape_string($conn, $_POST['hr_remarks']);
   $hr_approval_date = date('Y-m-d');


   /* This block sets the Leave Status field based on HR's Confirmation Status */
   if ($hr_approval == 'Confirmed') {
      $leave_status = 'Confirmed by HR';
   } elseif ($hr_approval == 'Rejected') {
      $leave_status = 'Rejected by HR';
   }

   /* This block validates the form inputs. */
   if ($hr_days > $linemgr_days) {
      $msg->error('Oops! You cannot confirm above the ' . $linemgr_days . 'days that was approved by the LineMgr');
      return FALSE;
   } elseif ($hr_days < 1) {
      $msg->error('Oops! Please enter value for Confirmed Days.');
      return FALSE;
   } elseif (strtotime($hr_leavecommence_date) > strtotime($hr_resume_date)) {
      $msg->error('Confirmed ResumeDate MUST be greater than Confirmed StartDate');
      return FALSE;
   }


   $recipientEmail = getRecipientEmail($emp_id);
   $recipientName = getRecipientName($emp_id);

   //Compose Email Message
   $mailMessage = "Your Leave Request has been confirmed by the HR Department. Kindly log on to Ace! HRM to view more details about this request. Click on the button below to login";
   $mailSubject = "Your Leave Request has been Confirmed by your HR";   


   /* This block writes the data to the dabase if there are no errors */
   $sql = "UPDATE leave_master, employees SET employees.annual_leave_taken = annual_leave_taken + $hr_days, leave_master.leave_status = '$leave_status', hr_approval = '$hr_approval', hr_days = '$hr_days', hr_leavecommence_date = '$hr_leavecommence_date', hr_resume_date = '$hr_resume_date', hr_remarks = '$hr_remarks', hr_approval_date = '$hr_approval_date' WHERE leave_id = $leave_id AND employees.emp_id = $emp_id";
   $result = mysqli_query($conn, $sql);

   //Commit Changes to Database if no errors occured
   if ($result) {
  
       //Send Email Notification
       sendMailerMe($recipientEmail, $mailSubject, $recipientName, $mailMessage);
       
      //Display Notification Message on screen
      $msg->success('Great! Leave Request Confirmed', 'unconfirmed_leave.php');
      unset($_POST);
      $_POST = array();
   } else {
      $error_msg = mysqli_error($conn);
      $msg->error('Oops! Something went wrong. Please try again' . $error_msg);
   }
}


function listLeaveRecords()
{
   global $conn;

   $sql = "SELECT * from leave_summary";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $leave_id = $row['leave_id'];
         $emp_id = $row['emp_id'];
         $employee_name = $row['employee_name'];
         $leave_status = $row['leave_status'];
         $leave_type = $row['leave_type'];
         $requested_days = $row['requested_days'];
         $commence_date = $row['commence_date'];
         $end_date = $row['end_date'];
         $resumption_date = $row['resumption_date'];
         $remarks = $row['remarks'];
         $relief_officer = $row['relief_officer'];
         $relief_officer_name = $row['relief_officer_name'];
         $request_date = $row['request_date'];
         $leave_upload = $row['leave_upload'];
         $line_mgr_id = $row['line_mgr_id'];
         $linemgr_name = $row['linemgr_name'];
         $linemgr_review = $row['linemgr_review'];
         $linemgr_days = $row['linemgr_days'];
         $line_mgr_rmks = $row['line_mgr_rmks'];
         $leave_reviewer = $row['leave_reviewer'];
         $linemgr_review_date = $row['linemgr_review_date'];
         $hr_approval = $row['hr_approval'];
         $hr_days = $row['hr_days'];
         $hr_leavecommence_date = $row['hr_leavecommence_date'];
         $hr_resume_date = $row['hr_resume_date'];
         $hr_remarks = $row['hr_remarks'];
         $hr_approval_date = $row['hr_approval_date'];
         $cancellation_reasons = $row['cancellation_reasons'];

         echo "<tr>
               <td>
                  <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$leave_id><i class='fa fa-search'></i></button>
               </td>               
                  <td>$leave_id</td>
                  <td>$employee_name</td>
                  <td>$leave_status</td>
                  <td>$leave_type</td>
                  <td>$requested_days</td>
                  <td>$commence_date</td>
                  <td style='display:none;'>$end_date</td>
                  <td>$resumption_date</td>
                  <td style='display:none;'>$remarks</td>
                  <td>$relief_officer_name</td>
                  <td>$request_date</td>
                  <td style='display:none;'>$leave_upload</td>
                  <td>$linemgr_name</td>
                  <td>$linemgr_review</td>
                  <td>$linemgr_days</td>
                  <td style='display:none;'>$line_mgr_rmks</td>
                  <td>$leave_reviewer</td>
                  <td>$linemgr_review_date</td>
                  <td>$hr_approval</td>
                  <td>$hr_days</td>
                  <td style='display:none;'>$hr_leavecommence_date</td>
                  <td style='display:none;'$hr_resume_date</td>
                  <td style='display:none;'>$hr_remarks</td>
                  <td>$hr_approval_date</td>
                  <td style='display:none;'>$cancellation_reasons</td>                                       
               </tr>";
      }
   } else {
      echo "<tr> <td>No records available </td></tr>";
   }
}
