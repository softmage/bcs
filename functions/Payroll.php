<?php

require_once 'FlashMessages.php';


function generatePayroll()
{

   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;
   global $user_fullname;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
   
   //Get Values from FORM
   $period_id = mysqli_real_escape_string($conn, $_POST['current_period_id']);
   $total_workdays = mysqli_real_escape_string($conn, $_POST['total_workdays']);


   //Begin Transaction Mode
   mysqli_autocommit($conn, false);
   $flag = true;


   //A. Close all existing payroll Schedules
   $close_open_payroll = "UPDATE payroll_schedule SET	payroll_cycle = 'Archived' WHERE payroll_cycle IS NULL";
   $result = mysqli_query($conn, $close_open_payroll);

   //Set Flag to false if Insert action fails
   if (!$result) {
      $flag = false;
      echo "Error details: " . mysqli_error($conn) . ".";
      $msg->error('Oops! Something went wrong in Result <br>' . $close_open_payroll . '<br>' . mysqli_error($conn));
   }


   //B. Generate new Payroll - Step One 
   $sql1 = "INSERT INTO payroll_generator(period_id, date_generated, total_workdays) VALUES($period_id, curdate(), $total_workdays)";
   $result1 = mysqli_query($conn, $sql1);

   // Get the last Inserted ID is successfuly or Flag to false if Insert action fails
   if ($result1) {
      $gen_id = mysqli_insert_id($conn);
   } else {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result1 <br>' . $sql1 . '<br>' . mysqli_error($conn));
   }


   //C. Insert into Payroll Schedule Table for ONLY active employees- Step Two
   $sql2 = "INSERT into payroll_schedule (emp_id,monthly_gross) SELECT emp_id, (annual_sal)/12 FROM employees where employ_status = 'Active'";
   $result2 = mysqli_query($conn, $sql2);

   //Set Flag to false if Insert action fails
   if (!$result2) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result2 <br>' . $sql2 . '<br>' . mysqli_error($conn));
   }

   //D. Insert into Payroll Schedule Table for ONLY active employees- Step Three
   $sql3 = "UPDATE payroll_schedule s, payroll_generator, computation_matrix c SET
               monthly_cra 			=  (monthly_gross * 20)/100 +  (200000/12),	
               employer_contrib 		=  (((c.basic + c.housing + c.transport)/100) * monthly_gross) * (employer_matrix/100),	
               monthly_pension  		=  (((c.basic + c.housing + c.transport)/100) * monthly_gross) * (staff_matrix/100),
               s.basic				   = (monthly_gross * c.basic)/100,
               s.transport			   = (monthly_gross * c.transport)/100,
               s.housing			   = (monthly_gross * c.housing)/100,
               s.entertainment	   = (monthly_gross * c.entertainment)/100,
               s.welfare			   = (monthly_gross * c.welfare)/100,
               s.others				   = (monthly_gross * c.others)/100,
               s.leave_allowance    = (monthly_gross * c.leave_allowance)/100,	
               s.generated_id		   = $gen_id,	 
               daily_rate		      = (monthly_gross/total_workdays),
               s.period_id          = payroll_generator.period_id,
               s.date_created      = curdate(),
               s.prepared_by        = '$user_fullname'
            WHERE s.review_status = 'Uninitialised' AND payroll_generator.dump_status = 'Open' AND s.generated_id IS NULL";
   $result3 = mysqli_query($conn, $sql3);

   //Set Flag to false if Insert action fails
   if (!$result3) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result3 <br>' . $sql3 . '<br>' . mysqli_error($conn));
   }


   //E. Calculate Taxable and Non Taxable Income + Deductions for latness and absenteeism - Step Four
   $sql4 = "UPDATE payroll_schedule s, payroll_generator, computation_matrix c SET
               monthly_non_taxable = monthly_pension + monthly_cra,
               monthly_taxable = (monthly_gross + overtime) - (monthly_pension + monthly_cra)
            WHERE s.review_status = 'Uninitialised' AND payroll_generator.dump_status = 'Open' AND s.generated_id = $gen_id";
   $result4 = mysqli_query($conn, $sql4);

   //Set Flag to false if Insert action fails
   if (!$result4) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result4 <br>' . $sql4 . '<br>' . mysqli_error($conn));
   }

   //F. Calculate PAYE - Step Five
   $sql5 = "UPDATE payroll_schedule SET
               monthly_paye = IF(monthly_taxable < (300000/12), ((monthly_gross * 1)/100), (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable = (300000/12), (((300000/12) * 7)/100), (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable > (300000/12) AND monthly_taxable <= (600000/12), ((21000/12) + ((monthly_taxable - (300000/12))*11)/100),  (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable > (600000/12) AND monthly_taxable <= (1100000/12), ((54000/12) + ((monthly_taxable - (600000/12))*15)/100), (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable > (1100000/12) AND monthly_taxable <= (1600000/12), ((129000/12) + ((monthly_taxable - (1100000/12))*19)/100), (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable > (1600000/12) AND monthly_taxable <= (3200000/12), ((224000/12) + ((monthly_taxable - (1600000/12))*21)/100), (monthly_paye + 0)),
               monthly_paye = IF(monthly_taxable > (3200000/12), ((560000/12) + ((monthly_taxable - (3200000/12))*24)/100), (`monthly_paye` + 0))
            WHERE review_status = 'Uninitialised' AND generated_id = $gen_id";
   $result5 = mysqli_query($conn, $sql5);

   //Set Flag to false if Insert action fails
   if (!$result5) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result5 <br>' . $sql5 . '<br>' . mysqli_error($conn));
   }

   //G. Update Certain Fields on the Schedule Table - Step Six
   $sql6 = "UPDATE payroll_schedule p, monthly_attendance_report m SET 
               p.days_late   = m.times_late,
               p.days_absent = m.times_absent,
               p.days_worked = m.times_present, 
               absenteeism   = daily_rate * p.days_absent
            WHERE p.period_id = m.period_id AND p.emp_id = m.emp_id AND p.review_status = 'Uninitialised' AND p.generated_id = $gen_id";
   $result6 = mysqli_query($conn, $sql6);

   //Set Flag to false if Insert action fails
   if (!$result6) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result6 <br>' . $sql6 . '<br>' . mysqli_error($conn));
   }


   //H. Close Open items on Payroll Generator Table - Step Five
   $sql7 = "UPDATE payroll_generator SET payroll_generator.dump_status = 'Closed' WHERE payroll_generator.dump_status = 'Open'";
   $result7 = mysqli_query($conn, $sql7);

   //Set Flag to false if Insert action fails
   if (!$result7) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result7 <br>' . $sql7 . '<br>' . mysqli_error($conn));
   }

   //I. Close current Period
   $sql8 = "UPDATE period SET status = 'Closed' WHERE status = 'Open'";
   $result8 = mysqli_query($conn, $sql8);

   //Set Flag to false if Insert action fails
   if (!$result8) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result8 <br>' . $sql8 . '<br>' . mysqli_error($conn));
   }

   //J. Initiate new Payroll Period
   $sql9 = "UPDATE period SET status = 'Open' WHERE status = 'Pending'";
   $result9 = mysqli_query($conn, $sql9);

   //Set Flag to false if Insert action fails
   if (!$result9) {
      $flag = false;
      $msg->error('Oops! Something went wrong in Result9 <br>' . $sql9 . '<br>' . mysqli_error($conn));
   }


   //Commit Changes to Database if no errors occured
   if ($flag) {
      mysqli_commit($conn);
      $msg->success('Great! Payroll Generated', 'payroll_summary.php');
   } else {
      mysqli_rollback($conn);
      $msg->error('Oops! an error occured. Please try again later');
   }
}


//Load all Payroll Records based on filter. Accepts User_id as a criteria
function getAllPayrollSummary($filter)
{

   global $conn;

   $sql = "SELECT * from payroll_summary WHERE $filter";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $payroll_id = $row['payroll_id'];
         $generated_id = $row['generated_id'];
         $period_id = $row['period_id'];
         $period_name = $row['period_name'];
         $total_workdays = $row['total_workdays'];
         $emp_id = $row['emp_id'];
         $lastname = $row['lastname'];
         $firstname = $row['firstname'];
         $daily_rate = number_format($row['daily_rate'], 2);
         $days_worked = $row['days_worked'];
         $days_absent = $row['days_absent'];
         $monthly_gross = number_format($row['monthly_gross'], 2);
         $employer_contrib = number_format($row['employer_contrib'], 2);
         $monthly_paye = number_format($row['monthly_paye'], 2);
         $monthly_pension = number_format($row['monthly_pension'], 2);
         $monthly_cra = number_format($row['monthly_cra'], 2);
         $monthly_non_taxable = number_format($row['monthly_non_taxable'], 2);
         $monthly_taxable = number_format($row['monthly_taxable'], 2);
         $iou = number_format($row['iou'], 2);
         $loan = number_format($row['loan'], 2);
         $lateness = number_format($row['lateness'], 2);
         $absenteeism = number_format($row['absenteeism'], 2);
         $suspension = number_format($row['suspension'], 2);
         $overtime = number_format($row['overtime'], 2);
         $overtime_days = number_format($row['overtime_days'], 2);
         $other_deduction = number_format($row['other_deduction'], 2);
         $total_stat_deductions = number_format($row['total_stat_deductions'], 2);
         $total_other_deduc = number_format($row['total_other_deduc'], 2);
         $total_salary = number_format($row['total_salary'], 2);
         $date_created = $row['date_created'];
         $prepared_by = $row['prepared_by'];
         $review_status = $row['review_status'];
         $review_date = $row['review_date'];
         $reviewed_by = $row['reviewed_by'];
         $payroll_cycle = $row['payroll_cycle'];
         $days_late = $row['days_late'];
         $basic = number_format($row['basic'], 2);
         $housing = number_format($row['housing'], 2);
         $transport = number_format($row['transport'], 2);
         $entertainment = number_format($row['entertainment'], 2);
         $welfare = number_format($row['welfare'], 2);
         $others = number_format($row['others'], 2);
         $leave_allowance = number_format($row['leave_allowance'], 2);

         $employee_name = $lastname . " " . $firstname;

         echo "<tr>
                    <td style='text-align: right;'>
                        <button class='view btn btn-xs btn-primary' type='button' data-placement='top' data-toggle='modal' data-target='.view_report' id=$payroll_id><i class='fa fa-search'></i></button>
                        <button class='btn btn-xs' type='button' data-placement='top' data-toggle='title'><a href='print_slip.php?id=$payroll_id'><i class='fa fa-print'></i></a></button>                        
                    </td> 
                    <td style='text-align: right;'>$payroll_id</td>
                    <td style='text-align: right;'>$period_name</td>
                    <td style='text-align: right;'>$total_workdays</td>
                    <td>$employee_name</td>                    
                    <td style='text-align: right;'>$daily_rate</td>
                    <td style='text-align: right;'>$days_worked</td>
                    <td style='text-align: right;'>$days_absent</td>
                    <td style='text-align: right;'>$monthly_gross</td>
                    <td style='display:none;'>$monthly_cra</td>
                    <td style='display:none;'>$monthly_non_taxable</td>
                    <td style='display:none;'>$monthly_taxable</td>
                    <td style='display:none;'>$employer_contrib</td>
                    <td style='text-align: right;'>$monthly_paye</td>
                    <td style='text-align: right;'>$monthly_pension</td>                    
                    <td style='display:none;text-align: right;'>$iou</td>
                    <td style='display:none;text-align: right;'>$loan</td>
                    <td style='display:none;text-align: right;'>$lateness</td>
                    <td style='display:none;text-align: right;'>$absenteeism</td>
                    <td style='display:none;text-align: right;'>$suspension</td>
                    <td style='display:none;text-align: right;'>$overtime</td>
                    <td style='display:none;text-align: right;'>$overtime_days</td>
                    <td style='display:none;text-align: right;'>$other_deduction</td>
                    <td style='text-align: right;'>$total_stat_deductions</td>
                    <td style='text-align: right;'>$total_other_deduc</td>
                    <td style='text-align: right; font-weight: bold'>$total_salary</td>
                    <td>$date_created</td>
                    <td>$prepared_by</td>
                    <td>$review_status</td>
                    <td>$review_date</td>
                    <td>$reviewed_by</td>
                    <td style='display:none;'>$payroll_cycle</td>
                    <td style='display:none;'>$basic</td>
                    <td style='display:none;'>$housing</td>
                    <td style='display:none;'>$transport</td>
                    <td style='display:none;'>$entertainment</td>
                    <td style='display:none;'>$welfare</td>
                    <td style='display:none;'>$others</td>
                    <td style='display:none;'>$leave_allowance</td>                                         
                  </tr>";
      }
   } else {
      echo "<tr> <td style='text-align: right;'>No records available </td></tr>";
   }
}


//Load an employee's Payroll record
function getSinglePayrollData($id)
{

   global $conn;

   $sql = "SELECT * from payroll_summary WHERE payroll_id = $id";
   $result =  mysqli_query($conn, $sql);
   $row = mysqli_fetch_assoc($result);

   $values = array();

   if ($row) {

      $values['payroll_id'] = $row['payroll_id'];
      $values['generated_id'] = $row['generated_id'];
      $values['period_id'] = $row['period_id'];
      $values['period_name'] = $row['period_name'];
      $values['total_workdays'] = $row['total_workdays'];
      $values['emp_id'] = $row['emp_id'];
      $values['employee_name'] = $row['lastname'] . ' ' . $row['firstname'];
      $values['daily_rate'] = $row['daily_rate'];
      $values['days_worked'] = $row['days_worked'];
      $values['days_absent'] = $row['days_absent'];
      $values['monthly_gross'] = $row['monthly_gross'];
      $values['employer_contrib'] = $row['employer_contrib'];
      $values['monthly_paye'] = $row['monthly_paye'];
      $values['monthly_pension'] = $row['monthly_pension'];
      $values['monthly_cra'] = $row['monthly_cra'];
      $values['monthly_non_taxable'] = $row['monthly_non_taxable'];
      $values['monthly_taxable'] = $row['monthly_taxable'];
      $values['iou'] = $row['iou'];
      $values['loan'] = $row['loan'];
      $values['lateness'] = $row['lateness'];
      $values['absenteeism'] = $row['absenteeism'];
      $values['suspension'] = $row['suspension'];
      $values['overtime'] = $row['overtime'];
      $values['overtime_days'] = $row['overtime_days'];
      $values['other_deduction'] = $row['other_deduction'];
      $values['total_stat_deductions'] = $row['total_stat_deductions'];
      $values['total_other_deduc'] = $row['total_other_deduc'];
      $values['total_salary'] = $row['total_salary'];
      $values['date_created'] = $row['date_created'];
      $values['prepared_by'] = $row['prepared_by'];
      $values['review_status'] = $row['review_status'];
      $values['review_date'] = $row['review_date'];
      $values['reviewed_by'] = $row['reviewed_by'];
      $values['payroll_cycle'] = $row['payroll_cycle'];
      $values['days_late'] = $row['days_late'];
      $values['basic'] = $row['basic'];
      $values['housing'] = $row['housing'];
      $values['transport'] = $row['transport'];
      $values['entertainment'] = $row['entertainment'];
      $values['welfare'] = $row['welfare'];
      $values['others'] = $row['others'];
      $values['leave_allowance'] = $row['leave_allowance'];
   }
   echo json_encode($values);
}



//Load PAYE Remittance
function getPayeRemittance($filter)
{

   global $conn;

   $sql = "SELECT * from paye_remittance WHERE $filter";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $generated_id = $row['generated_id'];
         $period_name = $row['period_name'];
         $employee_name = $row['employee_name'];
         $monthly_gross = number_format($row['monthly_gross'], 2);
         $monthly_pension = number_format($row['monthly_pension'], 2);
         $monthly_cra = number_format($row['monthly_cra'], 2);
         $monthly_non_taxable = number_format($row['monthly_non_taxable'], 2);
         $monthly_taxable = number_format($row['monthly_taxable'], 2);
         $monthly_paye = number_format($row['monthly_paye'], 2);
         $date_created = $row['date_created'];
         $prepared_by = $row['prepared_by'];

         echo "<tr>
                  <td style='display:none;'>$generated_id</td>
                  <td>$period_name</td>
                  <td>$employee_name</td>
                  <td style='text-align: right;'>$monthly_gross</td>
                  <td style='text-align: right;'>$monthly_pension</td>
                  <td style='text-align: right;'>$monthly_cra</td>
                  <td style='text-align: right;'>$monthly_non_taxable</td>
                  <td style='text-align: right;'>$monthly_taxable</td>
                  <td style='text-align: right; font-weight:bold;'>$monthly_paye</td>
                  <td>$date_created</td>
                  <td>$prepared_by</td>
               </tr>";
      }
   } else {
      echo "<tr> <td style='text-align: right;'>No records available </td></tr>";
   }
}

//Load Pension Remittance Records
function getPensionRemittance($filter)
{

   global $conn;

   $sql = "SELECT * from pension_remittance WHERE $filter";
   $result = mysqli_query($conn, $sql);
   $rowCount = mysqli_num_rows($result);

   if ($rowCount > 0) {
      while ($row = mysqli_fetch_array($result)) {

         $generated_id = $row['generated_id'];
         $period_name = $row['period_name'];
         $employee_name = $row['employee_name'];
         $monthly_gross = number_format($row['monthly_gross'], 2);
         $basic = number_format($row['basic'], 2);
         $housing = number_format($row['housing'], 2);
         $transport = number_format($row['transport'], 2);
         $employer_contrib = number_format($row['employer_contrib'], 2);
         $monthly_pension = number_format($row['monthly_pension'], 2);
         $total_pension = number_format($row['total_pension'], 2);
         $pfa = $row['pfa'];
         $date_created = $row['date_created'];
         $prepared_by = $row['prepared_by'];

         echo "<tr>
                  <td style='display:none;'>$generated_id</td>
                  <td>$period_name</td>
                  <td>$employee_name</td>
                  <td style='text-align: right;'>$monthly_gross</td>
                  <td style='text-align: right;'>$basic</td>
                  <td style='text-align: right;'>$housing</td>
                  <td style='text-align: right;'>$transport</td>                  
                  <td style='text-align: right;'>$employer_contrib</td>                  
                  <td style='text-align: right;'>$monthly_pension</td>
                  <td style='text-align: right; font-weight:bold'>$total_pension</td>
                  <td>$pfa</td>
                  <td>$date_created</td>
                  <td>$prepared_by</td>
               </tr>";
      }
   } else {
      echo "<tr> <td style='text-align: right;'>No records available </td></tr>";
   }
}


//Load an employee's Payroll record
function printPayslip($id, $user_id)
{

   global $conn;

   $sql = "SELECT * from print_payslip WHERE payroll_id = $id AND emp_id = $user_id";
   $result =  mysqli_query($conn, $sql);
   $payslip = mysqli_fetch_all($result, MYSQLI_ASSOC);

   return $payslip;
}
