<?php

require_once 'Emailer.php';

function userlogin()
{

   global $conn;

   // $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   $email = mysqli_real_escape_string($conn, $_POST['email']);
   $password = mysqli_real_escape_string($conn, $_POST['password']);

   $user_check_query = "SELECT users.emp_id, `email`, `password`, level_id AS user_level, users.lastname, users.firstname, profpic, employees.branch FROM users JOIN employees ON users.emp_id = employees.emp_id WHERE email = '$email' AND activation_status = 'Y' LIMIT 1";
   $result = mysqli_query($conn, $user_check_query);
   $user = mysqli_fetch_assoc($result);

   if (!$user) { // if user details are incorrect

      $errmsg = mysqli_error($conn);
      // $msg->error('Oops! Username or Password is incorrect.');

      return FALSE;
   } elseif (!password_verify($password, $user['password'])) {
      // $msg->error('Oops! Username or Password is incorrect.');
      return FALSE;
   } else {
      //Load user details into Session if credentials are valid
      $userlevel = $user['user_level'];
      $_SESSION['user_id'] = $user['emp_id'];
      $_SESSION['user_level'] = $user['user_level'];
      $_SESSION['firstname'] = $user['firstname'];
      $_SESSION['full_name'] = $user['firstname'] . " " . $user['lastname'];
      $_SESSION['email'] = $user['email'];
      $_SESSION['profpic'] = $user['profpic'];
      $_SESSION['branch'] = $user['branch'];
      $_SESSION['timeout'] = time();


      $sql = "SELECT DISTINCT page FROM permissions WHERE level_id = $userlevel";
      $result = mysqli_query($conn, $sql);

      while ($row =  mysqli_fetch_assoc($result)) {
         $rows[] = $row;
      }

      $_SESSION['userpermissions'] = $rows;

      // $msg->success('Great! User logged IN.');

      header("Location:my_dashboard.php");
   }
}


function userLogout()
{

   session_start();
   unset($_SESSION["user_id"]);
   unset($_SESSION["user_level"]);
   unset($_SESSION["full_name"]);
   unset($_SESSION["profpic"]);
   unset($_SESSION["branch"]);
   unset($_SESSION["userpermissions"]);
   unset($_SESSION["email"]);

   header("Location:login.php");
}


//Funtion to generate random password
function generateNewPassword($length = 20)
{
   $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
   $charsLength = strlen($chars);

   $randomString = '';

   for ($i = 0; $i < $length; $i++) {
      $randomString .= $chars[rand(0, $charsLength - 1)];
   }

   return $randomString;
}


//Function to get recipient's name
function getUserName($email)
{
   global $conn;

   $sql = "SELECT CONCAT(lastname,' ',firstname) AS recipient_name FROM users WHERE email = '$email'";
   $result = mysqli_query($conn, $sql);
   while ($row = mysqli_fetch_array($result)) {
      $res = $row['recipient_name'];
   }

   return $res;
}


//Reset Password
function resetPwd()
{

   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

   //Get form data
   $email = mysqli_real_escape_string($conn, $_POST['reset_email']);

   //check if email exists in DB
   $sql = "SELECT * FROM users WHERE email = '$email' LIMIT 1 ";
   if ($result = mysqli_query($conn, $sql)) {
      $rowcount = mysqli_num_rows($result);
   }

   if ($rowcount < 1) {
      $msg->error('Oops! Email address [' . $email . '] is not recognised. Please try again.');
      return FALSE;
   } elseif ($rowcount > 0) {

      //Generate Random Password
      $random_password = generateNewPassword(10);
      $password = password_hash($random_password, PASSWORD_DEFAULT);

      //Save new password to DB
      $query = "UPDATE users SET password = '$password', password_change =  0 WHERE email = '$email' ";
      if (mysqli_query($conn, $query)) {

         $empName = getUserName($email);

         $mailMessage = "You requested for a password change on Ace!HRM. Use temporary password to login to the system <b>$random_password</b>. Please note that you will be requested to change the password immediately. Click on the button below to login";
         $mailSubject = "RE: Your New AceHRM Password";
         sendMailerMe($email, $mailSubject,  $empName, $mailMessage);

         $msg->success('Great! Your password has been sent to your mailbox.', 'login.php');
      } else {
         $msg->error('Oops! An error occured. Please contact the IT Dept.');
         return FALSE;
      }
   } else {
      $msg->error('Oops! An error occured. Please contact the IT Dept.');
   }
}


//Change Password
function changePwd($user_id, $user_email)
{

   $msg = new \Plasticbrain\FlashMessages\FlashMessages();

   global $conn;

   $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
   //Get form data
   $current_pwd = mysqli_real_escape_string($conn, $_POST['current_pwd']);
   $password_1 = mysqli_real_escape_string($conn, $_POST['password_1']);
   $password_2 = mysqli_real_escape_string($conn, $_POST['password_2']);

   if (empty($current_pwd)) {
      $msg->error('Oops! Please put in your current password.');
      return FALSE;
   }

   if ($password_1 !== $password_2) {
      $msg->error('Oops! Passwords do not match. Please try again.');
      return FALSE;
   }

   $user_check_query = "SELECT `password` FROM users WHERE emp_id = $user_id AND activation_status = 'Y' LIMIT 1";
   $result = mysqli_query($conn, $user_check_query);
   $user = mysqli_fetch_assoc($result);

   if (!password_verify($current_pwd, $user['password'])) {
      $msg->error('Oops! Password is incorrect. Please try again');
      return FALSE;
   } else {

      $password =  password_hash($password_1, PASSWORD_DEFAULT);

      //Save new password to DB
      $query = "UPDATE users SET password = '$password' WHERE emp_id = $user_id";

      if (mysqli_query($conn, $query)) {

         $empName = getUserName($user_email);

         $mailMessage = "You password has been changed successfully. If this change was not done by you, please contact the IT Department immedately.";
         $mailSubject = "Notice: You have changed your AceHRM Password";
         sendMailerMe($user_email, $mailSubject,  $empName, $mailMessage);

         $msg->success('Great! Password has been changed. Kindly LogIn again', 'logout.php');
      } else {
         $msg->error('Oops! An error occured. Please contact the IT Dept.');
         return FALSE;
      }
   }
}
