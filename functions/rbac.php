<?php
require_once 'FlashMessages.php';


//CHECK IF USER HAS PERMISSION TO PAGE
//Exclude Administrators from this check 
if ($userlevel != -1) {
    $cpage =  basename($_SERVER['REQUEST_URI']);
    $currentpage = explode('?', $cpage, 2)[0];
    checkPermissions($currentpage);
}


//Check User Permissions
function checkPermissions($pagename)
{
    $msg = new \Plasticbrain\FlashMessages\FlashMessages();

    global $conn;
    global $userlevel;

    $sql = "SELECT DISTINCT page FROM user_permissions WHERE level_id = $userlevel AND page = '$pagename'";
    $result = mysqli_query($conn, $sql);
    $rowcount = mysqli_num_rows($result);

    if ($rowcount < 1) {
        $msg->error('Oops! You do not have permission to access this page', 'my_dashboard.php');
    
    }
}

//DELETE LEVEL PERMISSIONS

function deleteLevelPermissions($id)
{
    global $conn;

    $sql = "DELETE FROM user_permissions WHERE page = '$id' ";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo "YES";      
    } else {
        echo "NO";
    }
}