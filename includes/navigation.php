<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="index.php" class="site_title"><i class="fa fa-trophy"></i> <span>Ace! HRM</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="assets/images/<?php echo "$user_profpic"; ?>" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2><?php echo "$userfirstname"; ?></h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <li><a href="index.php"><i class="fa fa-home"></i> Home </a>
          </li>
          <li><a><i class="fa fa-users"></i> Employees <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="employee_records.php">Employee Records</a></li>
              <li><a href="add_employees.php">Add Employee</a></li>
              <li><a href="archived_employees.php">Archived Employee</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-desktop"></i> Leave <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="leaverequest.php">Request Leave</a></li>
              <li><a href="myleave_summary.php">My Leave Summary</a></li>
              <li><a href="leave_review.php">Review LeaveRequest</a></li>
              <li><a href="unconfirmed_leave.php">Leave Confirmation</a></li>
              <li><a href="leaverecords.php">Leave Summary</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-clock-o"></i> Attendance <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="remote_clockin.php">Remote ClockIn</a></li>
              <li><a href="attendance_records.php">Attendance Records</a></li>
              <li><a href="monthly_attendance.php">Monthly Attendance Report</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-money"></i> Payroll <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="payroll_generator.php">Generate Payroll</a></li>
              <li><a href="paye_remittance.php">PAYE Remittance Schedule</a></li>
              <li><a href="pension_remittance.php">Pension Remittance Schedule</a></li>
              <li><a href="my_payslips.php">My Payslips</a></li>
              <li><a href="payroll_summary.php">Payroll Summary</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-graduation-cap"></i> Certifications <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href=".php">Certifications</a></li>
              <li><a href=".php">Add Certifications</a></li>
              <li><a href=".php">My Certifications</a></li>
            </ul>
          </li>
      </div>
      <div class="menu_section">
        <ul class="nav side-menu">
          <li><a><i class="fa fa-lock"></i> Admin <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="manage_users.php">Users</a></li>
              <li><a href="permissions.php">User Permissions</a></li>
            </ul>
          </li>
          <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a></li>
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="logout.php">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="assets/images/<?php echo "$user_profpic"; ?>" alt=""><?php echo "$userfirstname"; ?>
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="change_password.php"> Change Password</a></li>  
            <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->