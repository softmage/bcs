  <!-- jQuery -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="assets/js/fastclick.js"></script>
  <!-- NProgress -->
  <!-- <script src="assets/js/nprogress.js"></script> -->
  <!-- Chart.js -->
  <script src="assets/js/Chart.min.js"></script>
  <!-- bootstrap-progressbar -->
  <script src="assets/js/bootstrap-progressbar.min.js"></script>
  <!-- iCheck -->
  <script src="assets/js/icheck.min.js"></script>
  <!-- Skycons -->
  <!-- <script src="assets/js/skycons.js"></script> -->
 
  <script src="assets/js/moment.min.js"></script>

  <!-- Custom Theme Scripts -->
  <script src="assets/js/custom.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

  <script src="https://unpkg.com/autonumeric"></script>

  <script src="assets/js/jszip.min.js"></script>
 

  <!-- <script>  new AutoNumeric('#annual_sal', { currencySymbol : '₦' }); </script> -->
  <!-- <script>  new AutoNumeric('.input', { currencySymbol : '₦' }); </script> -->