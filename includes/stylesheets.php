  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="assets/css/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="assets/css/green.css" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="assets/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="assets/css/custom.min.css" rel="stylesheet">