<?php

session_start();

//Check if Session already exists
if (empty($_SESSION['user_id'])) {
   header("Location:login.php");
}else {
   header("Location:my_dashboard.php");
}
