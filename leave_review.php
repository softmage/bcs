<?php

if (session_id() == "") session_start();
ob_start();

// $id = $_GET['id'];
// if (!is_numeric($id)) {
//    header('Location:index.php');
//    exit;
// }


require_once 'core.php';

if (isset($_POST['update_request'])) {

   reviewLeaveRequest();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Review Leave Request | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">

            <br />

            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Review Leave Request</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">

                        <div class="col-md-12">
                           <div class="col-md-6">
                              <!-- //Display Feedback Message -->
                              <?php echo $msg->display(); ?>
                           </div>
                           <table id="leaverecords" class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    <th> </th>
                                    <th>Leave ID</th>
                                    <th>Emp ID</th>
                                    <th>Employee</th>
                                    <th>Leave Status</th>
                                    <th>Leave Type</th>
                                    <th>Duration</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Resumption</th>
                                    <th style='display:none;'>Remarks</th>
                                    <th>Relief Offficer</th>
                                    <th>Date Submitted</th>
                                    <th style='display:none;'>Uploads</th>
                                    <th>Line Manager</th>
                              </thead>

                              <tbody id="records_list">
                                 <?php
                                 getUnreviewedLeave($_SESSION['user_id']);
                                 ?>
                              </tbody>
                           </table>
                        </div>

                        <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                           <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel2">View Records</h4>
                                 </div>
                                 <div class="modal-body">
                                    <form class="viewrecords form-horizontal form-label-left input_mask" method="POST" id="viewrecords" name="viewrecords" name="viewrecords">
                                       <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                             <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Leave Details</a>
                                             </li>
                                             <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Leave Approval</a>
                                             </li>
                                          </ul>
                                          <div id="myTabContent" class="tab-content">
                                             <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                <input type="hidden" id="leave_id" name="leave_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                <input type="hidden" id="emp_id" name="emp_id" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                                <table class="table table-bordered table-striped">
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_id">Leave ID <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="leave_id" name="leave_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee_name">Employee <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="employee_name" name="employee_name" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_status">Leave Status <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="leave_status" name="leave_status" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_type">Leave Type <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="leave_type" name="leave_type" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="requested_days">Duration <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="requested_days" name="requested_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="commence_date">Start Date <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="commence_date" name="commence_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_date">End Date <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="end_date" name="end_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumption_date">ResumptionDate <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="resumption_date" class="form-control" name="resumption_date" required="" readonly />

                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remarks">Remarks <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <textarea name="remarks" id="remarks" class="form-control col-md-7 col-xs-12" rows="2" required readonly></textarea>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_name">Line Mgr<span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="linemgr_name" class="form-control" class="form-control" required="" readonly />
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relief_officer_name">Relief Officer<span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="relief_officer_name" class="form-control" class="form-control" required="" readonly />
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_date">Date Submitted <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" id="request_date" name="request_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </div>

                                             <!-- Panel 2 -->
                                             <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                <table class="table table-bordered table-striped">
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_review">Review Status <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <select id="linemgr_review" name="linemgr_review" class="form-control col-md-7 col-xs-12" required="">
                                                                  <option value="">Choose..</option>
                                                                  <option>Approved</option>
                                                                  <option>Rejected</option>
                                                               </select>
                                                            </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_days">Approved Days <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <input type="text" name="linemgr_days" id="linemgr_days" required="required" class="form-control col-md-7 col-xs-12">
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_mgr_rmks">Remarks <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                               <textarea name="line_mgr_rmks" id="line_mgr_rmks" class="form-control col-md-7 col-xs-12" rows="3" required></textarea>
                                                            </div>
                                                         </div>
                                                      </td>
                                                   </tr>
                                                </table>
                                                <div class="col-md-12 col-xs-12 form-group has-feedback">
                                                   <hr>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-md-12 col-xs-12 col-md-offset-3">
                                                      <button type="submit" name="update_request" class="btn btn-primary">Review Leave</button>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         // Initialize form validation on the registration form.
         // It has the name attribute "registration"
         $("#viewrecords").validate({
            ignore: "",
            rules: {
               firstname: {
                  required: true
               },
               annual_sal: {
                  required: true,
                  number: true
               }
            },

            // Specify validation error messages
            messages: {
               firstname: "Please enter your firstname",
               lastname: "Please enter your lastname"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');

         $.ajax({

            url: 'records.php?action=leave&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {
               $('.viewrecords #leave_id').val(values.leave_id);
               $('.viewrecords #emp_id').val(values.emp_id);
               $('.viewrecords #employee_name').val(values.employee_name);
               $('.viewrecords #leave_status').val(values.leave_status);
               $('.viewrecords #leave_type').val(values.leave_type);
               $('.viewrecords #requested_days').val(values.requested_days);
               $('.viewrecords #commence_date').val(values.commence_date);
               $('.viewrecords #end_date').val(values.end_date);
               $('.viewrecords #resumption_date').val(values.resumption_date);
               $('.viewrecords #remarks').val(values.remarks);
               $('.viewrecords #relief_officer').val(values.relief_officer);
               $('.viewrecords #relief_officer_name').val(values.relief_officer_name);
               $('.viewrecords #request_date').val(values.request_date);
               $('.viewrecords #leave_upload').val(values.leave_upload);
               $('.viewrecords #line_mgr_id').val(values.line_mgr_id);
               $('.viewrecords #linemgr_name').val(values.linemgr_name);
               $('.viewrecords #linemgr_review').val(values.linemgr_review);
               $('.viewrecords #linemgr_days').val(values.linemgr_days);
               $('.viewrecords #line_mgr_rmks').val(values.line_mgr_rmks);
               $('.viewrecords #leave_reviewer').val(values.leave_reviewer);
               $('.viewrecords #linemgr_review_date').val(values.linemgr_review_date);
               $('.viewrecords #hr_approval').val(values.hr_approval);
               $('.viewrecords #hr_days').val(values.hr_days);
               $('.viewrecords #hr_leavecommence_date').val(values.hr_leavecommence_date);
               $('.viewrecords #hr_resume_date').val(values.hr_resume_date);
               $('.viewrecords #hr_remarks').val(values.hr_remarks);
               $('.viewrecords #hr_approval_date').val(values.hr_approval_date);
               $('.viewrecords #cancellation_reasons').val(values.cancellation_reasons);
            }
         });
      });
   </script>
</body>

</html>