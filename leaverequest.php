<?php


if (session_id() == "") session_start(); 
ob_start();

require_once 'core.php';

if (isset($_POST['request_leave'])) {
   requestLeave();
}

require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Leave Request | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">

            <br />

            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Leave Request</h2>                       
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">

                        <div class="col-md-8">

                        <?php echo $msg->display(); ?>
                        
                           <form class="requestLeave form-horizontal form-label-left input_mask" method="POST" id="requestLeave" name="requestLeave">

                              <input type="hidden" id="emp_id" name="emp_id" required="required" value="<?php echo "$user_id"; ?>" class="form-control col-md-7 col-xs-12">


                              <table class="table table-bordered table-striped">
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="requested_days">Leave Duration <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <input type="text" id="requested_days" value="<?php if(isset($_POST['requested_days'])) echo $_POST['requested_days']?>" name="requested_days" required="required" class="form-control col-md-7 col-xs-12">
                                          </div>
                                       </div>
 
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="commence_date">Start Date <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <input type="date" id="commence_date" value="<?php if(isset($_POST['commence_date'])) echo $_POST['commence_date']?>" name="commence_date" required="required" class="form-control col-md-7 col-xs-12">
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_date">End Date <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <input type="date" id="end_date" value="<?php if(isset($_POST['end_date'])) echo $_POST['end_date']?>" name="end_date" required="required" class="form-control col-md-7 col-xs-12">
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumption_date">Resumption Date <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <input type="date" id="resumption_date" value="<?php if(isset($_POST['resumption_date'])) echo $_POST['resumption_date']?>" name="resumption_date" required="required" class="form-control col-md-7 col-xs-12">
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relief_officer">Relief Officer <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <select id="relief_officer" name="relief_officer" class="form-control col-md-7 col-xs-12" required="">
                                                <option value="">Choose..</option>
                                                <?php getLineManagers(); ?>>
                                             </select>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_mgr_id">Line Mgr/Supervisor <span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <select id="line_mgr_id" name="line_mgr_id" class="form-control col-md-7 col-xs-12" required="">
                                                <option value="">Choose..</option>
                                                <?php getLineManagers(); ?>>
                                             </select>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remarks">Remarks <span>*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <textarea name="remarks" value="<?php if(isset($_POST['remarks'])) echo $_POST['remarks']?>" id="remarks" class="form-control col-md-7 col-xs-12" rows="3"></textarea>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                              </table>


                              <div class="form-group">
                                 <div class="col-md-6 col-xs-6 col-md-offset-3">
                                    <button type="submit" name="request_leave" class="btn btn-primary">Request Leave</button>

                                 </div>
                              </div>
                           </form>

                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         // Initialize form validation on the registration form.
         // It has the name attribute "registration"
         $("#requestLeave").validate({
            ignore: "",
            // Specify validation rules
            rules: {
               requested_days: {
                  required: true,
                  number: true
               }
            },

            // Specify validation error messages
            messages: {
               requested_days: "Please enter your leave duration in numbers",              
            },
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      new AutoNumeric('#annual_sal');
   </script>
</body>

</html>