<?php

session_start();

//Check if Session already exists
if (!empty($_SESSION['user_id'])) {
    header("Location:index.php");
}

// require_once 'core.php';
require_once 'config/config.php';
require_once 'functions/Users.php';
require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

//CHeck if Form is submitted
if (isset($_POST['login'])) {
    userlogin();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>Login | ACE! HRM </title>
    <?php include 'includes/stylesheets.php'; ?>

</head>

<body class="login">
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <h1>ACE! HRM</h1>
                    <!-- <h1><?php echo APPNAME ?></h1> -->

                    <div class="col-md-9">
                        <?php echo $msg->display(); ?>
                    </div>

                    <form method="POST" action="">

                        <div>
                            <input type="text" class="form-control" placeholder="Username" required="" name="email" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required="" name="password" />
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit" name="login">Log In</button>
                            <a class="reset_pass" href="forgot_password.php">Lost your password?</a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <p>&copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd</p>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>

</html>