<?php

require 'core.php';

if (isset($_POST['clockin'])) {
   clockIn();
}
if (isset($_POST['clockout'])) {
   clockOut();
}



//Get global Daily Attendance rates	
$c_sql = mysqli_query($conn, "SELECT SUM(punct_count) AS c_punctual, SUM(late_count) As c_late, SUM(absent_count) AS c_absent FROM attendance WHERE date = curdate()");

while ($row = mysqli_fetch_array($c_sql)) {
	$c_late = number_format($row['c_late']);
	$c_punctual = number_format($row['c_punctual']);
	$c_absent = number_format($row['c_absent']);
}

//Generate data for Global Daily Attendance DChart
$g_base = $c_late + $c_punctual + $c_absent;
$g_absent = number_format(($c_absent / $g_base) * 100, 1);
$g_late = number_format(($c_late / $g_base) * 100, 1);
$g_punctual = number_format(($c_punctual / $g_base) * 100, 1);


//Generate data for Gender Distribution DChart
$empCount = maleCount() + femaleCount();
$maleSum = number_format((maleCount() / $empCount) * 100, 1);
$femaleSum = number_format((femaleCount() / $empCount) * 100, 1);

//Generate Data for Leave Details - HR and Others
$pendingleavereview = leaveReview($user_id); 
$unconfirmedleave = confirmationReview();
$unusedleave = unusedLeave($user_id);      
$certificationstatus = certificationStatus();
$daysabsent = daysAbsent($user_id, $current_period_id);

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />
   <link rel="stylesheet" type="text/css" href="assets/js/slider-pro/dist/css/slider-pro.min.css" media="screen"/>

   <title>Dashboard | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>
   <script src="assets/js/jquery.min.js"></script>

   <script>
      $(document).ready(function() {

         var geo_options = {
            enableHighAccuracy: true,
            maximumAge: 60,
            timeout: 27000
         };


         if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showLocation);
            navigator.geolocation.getCurrentPosition(showLocation2);
         } else {
            $('#location').html('Geolocation is not supported by this browser.');
         }
      });

      function showLocation(position, geo_options) {
         var lat = position.coords.latitude;
         var longt = position.coords.longitude;

         document.getElementById("lat").value = lat;
         document.getElementById("longt").value = longt;
         document.getElementById("clockout_lat").value = lat;
         document.getElementById("clockout_longt").value = longt;
      }

      function showLocation2(position, geo_options) {
         var clockout_lat = position.coords.latitude;
         var clockout_longt = position.coords.longitude;

         document.getElementById("clockout_lat").value = clockout_lat;
         document.getElementById("clockout_longt").value = clockout_longt;
      }
   </script>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">

            <div class="col-md-9"><?php $msg->display(); ?> </div>
            <div class="clearfix"></div>
            <!-- top tiles -->
            <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="x_panel tile fixed_height_280">

                  <div class='x_title'>
                     <h2>Clock In/Out</h2>
                     <ul class='nav navbar-right panel_toolbox'>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                        </li>
                     </ul>
                     <div class='clearfix'></div>
                  </div>
                  <div class='x_content '>
                     <div class='clockin'>
                        <img src='assets/icon2.svg' />
                     </div>
                     <?php

                     $has_clockedin = getUserClockInStatus($user_id);
                     $has_clockedout = getUserClockOutStatus($user_id);

                     if ($has_clockedin == 'No') {
                        echo "
                        <form method='post' action=''>
                           <input type='hidden' name='lat' id='lat' required>
                           <input type='hidden' name='longt' id='longt' required>
                           <input type='hidden' name='emp_id' id='emp_id' value='$user_id' required>
                           <input type='hidden' name='branch' id='branch' value='$userbranch' required>
                           <button type='submit' name='clockin' class='btn btn-primary btn-lg btn-block'>Clock In!</button>
                        </form> ";
                     } else {
                        echo " 
                        <form method='post' action=''>
                           <input type='hidden' name='clockout_lat' id='clockout_lat' required>
                           <input type='hidden' name='clockout_longt' id='clockout_longt' required>
                           <input type='hidden' name='emp_id' id='emp_id' value='$user_id' required>
                           <button type='submit' name='clockout' class='btn btn-primary btn-lg btn-block'>Clock Out!</button>
                        </form> ";
                     }
                     ?>
                  </div>
               </div>
            </div>

            <!-- Display Section for Staff -->
            <?php if($userlevel == 1) :?>
            <div class='col-md-8'>
               <div class='x_panel tile fixed_height_280'>
                  <div class='x_title'>
                     <h2>Quick Links<small></small></h2>
                     <ul class='nav navbar-right panel_toolbox'>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                        </li>
                     </ul>
                     <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                     <div class='col-md-12 col-sm-12 col-xs-12'>
                        <ul class='stats-overview'>
                           <li class='hidden-phone'>
                              <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$pendingleavereview"; ?></span>
                              <a href='leave_review.php' target='_blank'><span class='name'> Leave Requests to review </span></a>
                           </li>
                           <li class='hidden-phone'>
                              <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$unusedleave" ?> days </span>
                              <a href='' target='_blank'><span class='name'> Unused Annual Leave. </span></a>
                           </li>
                           <li class='hidden-phone'>
                              <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have recorded <?php echo $daysabsent; ?></span>
                              <a href='' target='_blank'><span class='name'> days of absence </span></a>
                           </li>

                        </ul>
                        <br />
                     </div>
                     <!-- start project-detail sidebar -->
                     <div class='col-md-12 col-sm-12 col-xs-12'>
                        <a class='btn btn-app' href='leaverequest.php'> <i class='fa fa-child dark'></i> Request Leave </a>
                        <a class='btn btn-app' href=''><i class='fa fa-graduation-cap dark'></i> Update Certification </a>
                        <a class='btn btn-app' href='attendance_records.php'><i class='fa fa-user dark'></i> My Attendance </a>
                        <a class='btn btn-app' href=''><i class='fa fa-user dark'></i> My Profile </a>
                        <a class='btn btn-app' data-toggle='modal' data-target='.holidayview_modal'> <i class='fa fa-gamepad'></i>View Holidays </a>
                     </div>
                  </div>
               </div>
            </div>
            <?php endif ?>
            <!-- /End Staff Section -->

            <?php if($userlevel == 100 || $userlevel == -1) :?>
            <!-- Attendance Records -->
            <div class='col-md-4 col-sm-4 col-xs-12'>
               <div class='x_panel tile fixed_height_280 overflow_hidden'>
                  <div class='x_title'>
                     <h2>Attendance</h2>
                     <ul class='nav navbar-right panel_toolbox'>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                        </li>
                     </ul>
                     <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                     <table class='' style='width:100%'>
                        <tr>
                           <td>

                              <canvas class='canvasaDoughnut' height='140' width='140' style='margin: 15px 10px 10px 0'></canvas>
                           </td>
                           <td>
                              <table class='tile_info'>
                                 <tr>
                                    <td>
                                       <p><i class='fa fa-square green'></i>Punctual </p>
                                    </td>
                                    <td><?php echo "$g_punctual%";?></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><i class='fa fa-square aero'></i>Late </p>
                                    </td>
                                    <td><?php echo "$g_late%"?></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><i class='fa fa-square red'></i>Absent </p>
                                    </td>
                                    <td><?php echo "$g_absent%"; ?></td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                     <a href='attendance_records.php' class='btn btn-primary'>
                        <i class='fa fa-eye-o'> </i> View </a>
                  </div>
               </div>
            </div>
            <!-- Staff Distrubution -->
            <div class='col-md-4 col-sm-4 col-xs-12'>
               <div class='x_panel tile fixed_height_280 overflow_hidden'>
                  <div class='x_title'>
                     <h2>Gender Distribution</h2>
                     <ul class='nav navbar-right panel_toolbox'>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class=''>&nbsp;</a></li>
                        <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                        </li>
                     </ul>
                     <div class='clearfix'></div>
                  </div>
                  <div class='x_content'>
                     <table class='' style='width:100%'>
                        <tr>
                           <td>
                              <canvas class='staffdistribution' height='140' width='140' style='margin: 15px 10px 10px 0'></canvas>
                           </td>
                           <td>
                              <table class='tile_info'>
                                 <tr>
                                    <td>
                                       <p><i class='fa fa-square blue'></i>Male </p>
                                    </td>
                                    <td><?php echo "$maleSum%";?></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p><i class='fa fa-square green'></i>Female </p>
                                    </td>
                                    <td><?php echo "$femaleSum%"?></td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>

            <div class='row'>
               <div class='col-md-12'>
                  <div class='x_panel'>
                     <div class='x_title'>
                        <h2>Quick Links<small></small></h2>
                        <ul class='nav navbar-right panel_toolbox'>
                           <li><a class=''>&nbsp;</a></li>
                           <li><a class=''>&nbsp;</a></li>
                           <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                           </li>
                        </ul>
                        <div class='clearfix'></div>
                     </div>
                     <div class='x_content'>
                        <div class='col-md-8 col-sm-8 col-xs-12'>
                           <ul class='stats-overview'>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$pendingleavereview";?></span>
                                 <a href='leave_review.php' target='_blank'><span class='name'> Leave Requests to review </span></a>
                              </li>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$unconfirmedleave";?></span>
                                 <a href='unconfirmed_leave.php' target='_blank'><span class='name'> Leave Confirmation to grant </span></a>
                              </li>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$unusedleave";?> days </span>
                                 <a href='' target='_blank'><span class='name'> Unused Annual Leave. </span></a>
                              </li>

                           </ul>
                           <!-- HR Section Begins -->
                           <ul class='stats-overview'>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have recorded <?php echo $daysabsent; ?></span>
                                 <a href='' target='_blank'><span class='name'> days of absence </span></a>
                              </li>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have <?php echo "$certificationstatus";?> </span>
                                 <a href='' target='_blank'><span class='name'> Certification Updates to approve </span></a>
                              </li>
                              <li class='hidden-phone'>
                                 <span class='value text-success'><span style='font-size:20px;'> <i class='fa fa-bell-o'></i></span> You have 0 </span>
                                 <a href='' target='_blank'><span class='name'> Employee Profile modifications to approve </span></a>
                              </li>
                           </ul>
                           <br />
                        </div>
                        <!-- start project-detail sidebar -->
                        <div class='col-md-4 col-sm-4 col-xs-12'>
                           <a class='btn btn-app' href='add_employees.php'><i class='fa fa-users dark'></i> Add Employee </a>
                           <a class='btn btn-app' href='attendance_records.php'><i class='fa fa-play dark'></i> Attendance</a>
                           <a class='btn btn-app' href=''><i class='fa fa-graduation-cap dark'></i> Certifications</a>
                           <a class='btn btn-app' href='leaverequest.php'> <i class='fa fa-child dark'></i> Request Leave </a>
                           <a class='btn btn-app' data-toggle='modal' data-target='.addholiday_modal'><i class='fa fa-gamepad'></i>Add Holidays </a>
                           <a class='btn btn-app' href=''><i class='fa fa-user dark'></i> My Profile </a>

                           <!-- HR Section Ends -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <?php endif ?>


            <div class="row">
               <div class="col-md-7 col-sm-12 col-xs-12">
                  <div class='x_panel tile fixed_height_350 overflow_hidden'>
                     <div class='x_title'>
                        <h2>Weekly Attendance Trend</h2>
                        <ul class='nav navbar-right panel_toolbox'>
                           <li><a class=''>&nbsp;</a></li>
                           <li><a class=''>&nbsp;</a></li>
                           <li><a class='collapse-link'><i class='fa fa-chevron-up'></i></a>
                           </li>
                        </ul>
                        <div class='clearfix'></div>
                     </div>
                     <div class='x_content'>
                        <canvas id='attendance_barChart'></canvas>
                     </div>
                  </div>
               </div>
               <!-- Birthday Panel Starts -->
						<div class="col-md-5 col-sm-12 col-xs-12">
							<div class="x_panel tile fixed_height_350 overflow_hidden">
								<div class="x_title">
									<h2>Birthdays this week</h2>
									<ul class="nav navbar-right panel_toolbox">
										<li><a class="">&nbsp;</a></li>
										<li><a class="">&nbsp;</a></li>
										<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="profile_details">
										<!-- Birthday Loop starts -->
										<div class="col-md-12 well profile_view">
											<div id="week_birthdays" class="slider-pro">
												<div class="sp-slides">
													<?php $birthday = getBirthdays() ?>
													<?php foreach ($birthday as $birthday) : ?>
														<?php $b_id = $birthday['emp_id'];
														$b_empname = $birthday['empname'];
														$b_branch = $birthday['branch'];
														$b_email = $birthday['email'];
														$b_cug = $birthday['cug'];
														$b_dob = $birthday['dob'];
														$b_designation = $birthday['designation']; 	 
														$b_hire_date = $birthday['hire_date']; 	  ?>
														<?php echo "
															<div class='sp-slide'>
																<div class='col-md-12'>
																	<h2 class='brief'><i>$b_empname</i></h2>
																	<div class='left col-xs-7'>
																	 <h5></h5><i class='fa fa-briefcase'></i> : $b_designation</h5>
																	<p><strong><i class='fa fa-building-o'></i> : </strong> $b_branch </p>
																	<ul class='list-unstyled'>
																		<li><strong><i class='fa fa-envelope'></i> : </strong> $b_email</li>
																		<li><strong><i class='fa fa-phone'></i> : </strong> $b_cug</li>
																	</ul>
																</div>
																<div class='right col-xs-5 text-center'>
																	<img src='assets/images/$user_profpic' alt='' class='img-circle img-responsive'>
															   </div>
																 		<p> &nbsp; </p>
																</div>
																
																<div class='clearfix'></div>
																		<div class='col-xs-12 bottom text-center'>
																			<div class='col-xs-12 col-sm-6 emphasis'>
																				<p class='ratings'>
																			   <strong></strong><i class='fa fa-birthday-cake'></i> : </strong> $b_dob
																				</p>
																			</div>
																			<div class='col-xs-12 col-sm-6 emphasis'>
																				<b>Employ Date:</b> $b_hire_date
																			</div>
																		</div>															
																		
															</div>"; ?>
													<?php endforeach ?>
												</div>
											</div>										
										</div>
										<!-- Birthday Loop Ends -->
									</div>
								</div>
							</div>
						</div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>
   <script src="assets/js/slider-pro/dist/js/jquery.sliderPro.min.js"></script>

   <script src="assets/js/slider-pro/fancybox/jquery.fancybox.pack.js"></script>	
   
   <script type="text/javascript">
		$(document).ready(function($) {
			$('#week_birthdays').sliderPro({
				autoHeight: true,
				fade: true,
				autoplay: true,
				updateHash: true
			});
		});
	</script>
	<!-- Birthday Slider Script -->
	<script type="text/javascript">
		$(document).ready(function($) {
			$('#week_birthdays').sliderPro({
				autoHeight: true,
				fade: true,
				autoplay: true,
				updateHash: true
			});
		});
	</script>

   <script>
		function init_chart_doughnut() {
			if ("undefined" != typeof Chart && (console.log("init_chart_doughnut"), $(".canvasaDoughnut").length)) {
				var a = {
					type: "doughnut",
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: ["Punctual", "Late", "Absent"],
						datasets: [{
							data: [<?php echo "$g_punctual"; ?>, <?php echo "$g_late"; ?>,
								<?php echo "$g_absent"; ?>
							],
							backgroundColor: ["#1ABB9C", "#BDC3C7", "#E74C3C", "#26B99A", "#3498DB"],
							hoverBackgroundColor: ["#B370CF", "#34495E", "#34495E"]
						}]
					},
					options: {
						legend: !1,
						responsive: !1
					}
				};
				$(".canvasaDoughnut").each(function() {
					var b = $(this);
					new Chart(b, a)
				})
			}


			if ("undefined" != typeof Chart && (console.log("init_chart_doughnut"), $(".staffdistribution").length)) {
				var a = {
					type: "doughnut",
					tooltipFillColor: "rgba(51, 51, 51, 0.55)",
					data: {
						labels: ["Male", "Female"],
						datasets: [{
							data: [<?php echo "$maleSum"; ?>, <?php echo "$femaleSum "; ?>],
							backgroundColor: ["#9B59B6", "#1ABB9C", "#E74C3C", "#26B99A", "#3498DB"],
							hoverBackgroundColor: ["#B370CF", "#36CAAB", "#1ABB9C"]
						}]
					},
					options: {
						legend: !1,
						responsive: !1
					}
				};
				$(".staffdistribution").each(function() {
					var b = $(this);
					new Chart(b, a)
				})
			}

		}
	</script>
	//Include Bar Charts
	<script>
		function init_charts() {

			console.log('run_charts  typeof [' + typeof(Chart) + ']');
			if (typeof(Chart) === 'undefined') {
				return;
			}
			console.log('init_charts');
			Chart.defaults.global.legend = {
				enabled: true
			};

			// Bar chart            	  
			if ($('#attendance_barChart').length) {
				var ctx = document.getElementById("attendance_barChart");
				var mybarChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: [<?php echo "$month_name"; ?>],
						datasets: [{
								label: "# of Punctual",
								backgroundColor: "#9B59B6",
								data: [<?php echo "$punctual_bar"; ?>]
							},
							{
								label: "# of Late",
								backgroundColor: "#1ABB9C",
								data: [<?php echo "$late_bar"; ?>]
							},
							{
								label: "# of Absent",
								backgroundColor: "#E74C3C",
								data: [<?php echo "$absent_bar"; ?>]
							}
						]
					},
					options: {
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: !0
								}
							}]
						}
					}
				});

			}
		}
	</script>
</body>

</html>