<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>My Leave Summary | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>My Leave Records</h2>                        
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div class="row"></div>
                     </div>

                     <div class="col-md-12">
                        <div class="col-md-6">
                           <!-- //Display Feedback Message -->
                           <?php echo $msg->display(); ?>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th> </th>
                                 <th>Leave ID</th>
                                 <th>Employee</th>
                                 <th>Leave Status</th>
                                 <th>Leave Type</th>
                                 <th>Duration</th>
                                 <th>Start Date</th>
                                 <th style='display:none;'>End Date</th>
                                 <th>Resumption</th>
                                 <th style='display:none;'>Remarks</th>
                                 <th>Relief Offficer</th>
                                 <th>Date Submitted</th>
                                 <th style='display:none;'>Uploads</th>
                                 <th>Line Manager</th>
                                 <th>Review Status</th>
                                 <th>Approved Days</th>
                                 <th style='display:none;'>Remarks</th>
                                 <th>Reviewed By</th>
                                 <th>Review Date</th>
                                 <th>Confirmation Status</th>
                                 <th>Confirmed Duration</th>
                                 <th style='display:none;'>Confirmed StartDate</th>
                                 <th style='display:none;'>Confirmed ResumeDate</th>
                                 <th style='display:none;'>HR Remarks</th>
                                 <th>Date Confirmed</th>
                                 <th style='display:none;'>Cancellation Reasons</th>
                           </thead>

                           <tbody id="records_list">
                              <?php
                              myLeaveSummary($user_id);
                              ?>
                           </tbody>
                        </table>
                     </div>


                     <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                 </button>
                                 <h4 class="modal-title" id="myModalLabel2">View Records</h4>
                              </div>
                              <div class="modal-body">

                                
                                 <form class="viewrecords form-horizontal form-label-left input_mask" method="POST" id="viewrecords" name="viewrecords" name="viewrecords">
                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                       <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Leave Details</a>
                                          </li>
                                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Leave Approval</a>
                                          </li>
                                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Leave Confirmation</a>
                                          </li>
                                       </ul>
                                       <div id="myTabContent" class="tab-content">
                                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                             <input type="hidden" id="leave_id" name="leave_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                             <input type="hidden" id="emp_id" name="emp_id" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_id">Leave ID <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="leave_id" name="leave_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee_name">Employee <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="employee_name" name="employee_name" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_status">Leave Status <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="leave_status" name="leave_status" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_type">Leave Type <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="leave_type" name="leave_type" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="requested_days">Duration <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="requested_days" name="requested_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="commence_date">Start Date <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="commence_date" name="commence_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_date">End Date <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="end_date" name="end_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumption_date">ResumptionDate <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="resumption_date" class="form-control" name="resumption_date" required="" readonly />

                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="remarks">Remarks <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="remarks" id="remarks" class="form-control col-md-7 col-xs-12" rows="2" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_name">Line Mgr<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="linemgr_name" class="form-control" class="form-control" required="" readonly />
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relief_officer_name">Relief Officer<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="relief_officer_name" class="form-control" class="form-control" required="" readonly />
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_date">Date Submitted <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" id="request_date" name="request_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                          </div>

                                          <!-- Panel 2 -->
                                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_review">Review Status <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="linemgr_review" id="linemgr_review" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_days">Approved Days <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="linemgr_days" id="linemgr_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="line_mgr_rmks">Remarks <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="line_mgr_rmks" id="line_mgr_rmks" class="form-control col-md-7 col-xs-12" rows="3" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_reviewer">Reviewer<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="leave_reviewer" id="leave_reviewer" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linemgr_review_date">Review Date <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="linemgr_review_date" id="linemgr_review_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>
                                          </div>
                                          <!-- Panel 3 -->
                                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                             <table class="table table-bordered table-striped">
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_approval">Confirmation Status<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">                                                           
                                                            <input type="text"  name="hr_approval" id="hr_approval" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_days">Confirmed Duration <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">                                                           
                                                            <input type="text" name="hr_days" id="hr_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_leavecommence_date">Confirmed StartDate <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="hr_leavecommence_date" id="hr_leavecommence_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>

                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_resume_date">Confirmed ResumeDate <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" name="hr_resume_date" id="hr_resume_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cancellation_reasons">Cancellation Reasons<span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="cancellation_reasons" id="cancellation_reasons" class="form-control col-md-7 col-xs-12" rows="2" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_remarks">HR Remarks <span class="required">*</span>
                                                         </label>
                                                         <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <textarea name="hr_remarks" id="hr_remarks" class="form-control col-md-7 col-xs-12" rows="2" required readonly></textarea>
                                                         </div>
                                                      </div>
                                                   </td>
                                                </tr>
                                             </table>                                             
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         // Initialize form validation on the registration form.
         // It has the name attribute "registration"
         $("#addEmployee").validate({
            ignore: "",
            // $("form[name='addEmployee']").validate({ ignore: "" 
            // Specify validation rules
            rules: {
               firstname: {
                  required: true
               },
               lastname: {
                  required: true
               },
               nationality: {
                  required: true
               },
               dob: {
                  required: true
               },
               gender: {
                  required: true
               },
               marital_status: {
                  required: true
               }

            },

            // Specify validation error messages
            messages: {
               firstname: "Please enter your firstname",
               lastname: "Please enter your lastname",
               // objective: {
               //     required: "Please enter Objective",
               //     maxlength: "Description should not be more than 500 characters"
               // },
               // role_obj_id: {
               //     required: "Please select Role/Designation Objective"
               // }
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');


         $.ajax({

            url: 'records.php?action=leave&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {

               $('.viewrecords #leave_id').val(values.leave_id);
               $('.viewrecords #emp_id').val(values.emp_id);
               $('.viewrecords #employee_name').val(values.employee_name);
               $('.viewrecords #leave_status').val(values.leave_status);
               $('.viewrecords #leave_type').val(values.leave_type);
               $('.viewrecords #requested_days').val(values.requested_days);
               $('.viewrecords #commence_date').val(values.commence_date);
               $('.viewrecords #end_date').val(values.end_date);
               $('.viewrecords #resumption_date').val(values.resumption_date);
               $('.viewrecords #remarks').val(values.remarks);
               $('.viewrecords #relief_officer').val(values.relief_officer);
               $('.viewrecords #relief_officer_name').val(values.relief_officer_name);
               $('.viewrecords #request_date').val(values.request_date);
               $('.viewrecords #leave_upload').val(values.leave_upload);
               $('.viewrecords #line_mgr_id').val(values.line_mgr_id);
               $('.viewrecords #linemgr_name').val(values.linemgr_name);
               $('.viewrecords #linemgr_review').val(values.linemgr_review);
               $('.viewrecords #linemgr_days').val(values.linemgr_days);
               $('.viewrecords #line_mgr_rmks').val(values.line_mgr_rmks);
               $('.viewrecords #leave_reviewer').val(values.leave_reviewer);
               $('.viewrecords #linemgr_review_date').val(values.linemgr_review_date);
               $('.viewrecords #hr_approval').val(values.hr_approval);
               $('.viewrecords #hr_days').val(values.hr_days);
               $('.viewrecords #hr_leavecommence_date').val(values.hr_leavecommence_date);
               $('.viewrecords #hr_resume_date').val(values.hr_resume_date);
               $('.viewrecords #hr_remarks').val(values.hr_remarks);
               $('.viewrecords #hr_approval_date').val(values.hr_approval_date);
               $('.viewrecords #cancellation_reasons').val(values.cancellation_reasons);
            }
         });
      });
   </script>

   <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
   <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
   <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

</body>

</html>