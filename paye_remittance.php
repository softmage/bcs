<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

//Get ID and redirect id ID is not an integer
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    if (!is_numeric($id)) {
        header('Location:paye_remittance.php');
        exit;
    }
    //Set Filter to current ID
    $filter = "period_id = $id";
} else {
    //Set Filter to 0
    $filter = "period_id > 0";
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

    <title>PAYE Remittance Schedule | <?php echo APPNAME; ?> </title>

    <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <?php include_once 'includes/navigation.php'; ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <br />
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel tile">
                            <div class="x_title">
                                <h2>PAYE Remittance Schedule</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="col-md-12">
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <select class="filter_period select2_group form-control" name="filter_period" id="filter_period" required>
                                            <option value="" selected>Payroll Period</option>
                                            <?php
                                            getAllPeriods();
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <?php if(!empty($id)) :?>
                                        <button type="button" class="btn btn-primary" id="print_report"> <i class="fa fa-print m-left-xs"></i> Print</button>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="mt-20 col-md-12">


                                <div class="col-md-6">
                                    <!-- //Display Feedback Message -->
                                    <?php echo $msg->display(); ?>
                                </div>
                                <table id="datatable-buttons" class="table table-striped table-bordered currency">
                                <thead>
                                        <tr>
                                            <th style='display:none;'>generated_id</th>
                                            <th>Payroll Period</th>
                                            <th>Employee</th>
                                            <th>Gross Salary</th>
                                            <th>Pension</th>
                                            <th>CRA</th>
                                            <th>Non-Taxable </th>
                                            <th>Taxable Income</th>
                                            <th>PAYE</th>
                                            <th>Date Prepared</th>
                                            <th>Prepared By</th>
                                    </thead>

                                    <tbody id="records_list">
                                        <?php
                                        getPayeRemittance($filter);
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
    </div>

    <?php include 'includes/scripts.php'; ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#filter_period').on('change', function() {
                var period = $(this).val();
                if (period) {
                    var myurl = 'paye_remittance.php';
                    window.location.replace(myurl + '?id=' + period);
                } else {
                    alert('Error! Something went wrong');
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $("#print_report").on('click', function() {
                var myid = <?php echo "$id"; ?>;
                     var purl = 'reports/paye_remittance_report.php';
                    // window.location.replace(purl + '?id=' + myid);
                    window.open(purl + '?id=' + myid,'popUpWindow','height=533,width=800,left=10,top=10,,scrollbars=yes,menubar=no'); return false;
                
            });
        });
    </script>

    <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
</body>

</html>