<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

if (isset($_POST['generate_payroll'])) {
   generatePayroll();
}

require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Generate Payroll | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">

            <br />

            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Generate Payroll</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">

                        <div class="col-md-6">

                           <?php echo $msg->display();
                           if (!empty($current_period_id)) : ?>
                              <form class="requestLeave form-horizontal form-label-left input_mask" method="POST" id="requestLeave" name="requestLeave">

                                 <input type="hidden" id="current_period_id" name="current_period_id" required="required" value="<?php echo "$current_period_id"; ?>" class="form-control col-md-7 col-xs-12">


                                 <table class="table table-bordered table-striped">
                                    <tr>
                                       <td>
                                          <div class="form-group">
                                             <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_workdays">Total Workdays <span class="required">*</span>
                                             </label>
                                             <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" id="total_workdays" name="total_workdays" required="required" class="form-control col-md-7 col-xs-12">
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 <div class="form-group">
                                    <div class="col-md-6 col-xs-6 col-md-offset-3">
                                       <button type="submit" name="generate_payroll" class="btn btn-primary">Generate Payroll</button>

                                    </div>
                                 </div>
                              </form>
                           <?php endif ?>

                           <?php if (empty($current_period_id)) : ?>
                              <blockquote>
                                 <p>Payroll Schedule has already been generated</p>
                              </blockquote>
                           <?php endif ?>
                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         // Initialize form validation on the registration form.
         // It has the name attribute "registration"
         $("#requestLeave").validate({
            ignore: "",
            // Specify validation rules
            rules: {
               requested_days: {
                  required: true,
                  number: true
               }
            },

            // Specify validation error messages
            messages: {
               requested_days: "Please enter your leave duration in numbers",
            },
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      // $(document).on('click', '.view', function() {
      $(document).ready(function() {
         //  var dataID = $(this).attr('id');
         var dataID = 10;

         $.ajax({
            url: 'records.php?action=view&id=<?php echo "$id"; ?>',
            //   url: 'records.php?action=editkpi&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {
               $('.editEmployee #emp_id').val(values.emp_id);
               $('.editEmployee #staff_no').val(values.staff_no);
               $('.editEmployee #lastname').val(values.lastname);
               $('.editEmployee #firstname').val(values.firstname);
               $('.editEmployee #gender').val(values.gender);
               $('.editEmployee #nationality').val(values.nationality);
               $('.editEmployee #dob').val(values.dob);
               $('.editEmployee #marital_status').val(values.marital_status);
               $('.editEmployee #no_of_kids').val(values.no_of_kids);
               $('.editEmployee #home_address').val(values.home_address);
               $('.editEmployee #private_phone').val(values.private_phone);
               $('.editEmployee #private_email').val(values.private_email);
               $('.editEmployee #next_of_kin').val(values.next_of_kin);
               $('.editEmployee #kin_relationship').val(values.kin_relationship);
               $('.editEmployee #nok_phone').val(values.nok_phone);
               $('.editEmployee #nok_address').val(values.nok_address);
               $('.editEmployee #qualification').val(values.qualification);
               $('.editEmployee #discipline').val(values.discipline);
               $('.editEmployee #other_qual').val(values.other_qual);
               $('.editEmployee #employ_status').val(values.employ_status);
               $('.editEmployee #hire_date').val(values.hire_date);
               $('.editEmployee #official_email').val(values.official_email);
               $('.editEmployee #cug_line').val(values.cug_line);
               $('.editEmployee #department').val(values.department);
               $('.editEmployee #designation').val(values.designation);
               $('.editEmployee #job_level').val(values.job_level);
               $('.editEmployee #isline_mgr').val(values.isline_mgr);
               $('.editEmployee #line_mgr').val(values.line_mgr);
               $('.editEmployee #location').val(values.location);
               $('.editEmployee #employment_type').val(values.employment_type);
               $('.editEmployee #confirmation_status').val(values.confirmation_status);
               $('.editEmployee #confirmation_date').val(values.confirmation_date);
               $('.editEmployee #annual_sal').val(values.annual_sal);
               $('.editEmployee #tin').val(values.tin);
               $('.editEmployee #bank_name').val(values.bank_name);
               $('.editEmployee #account_no').val(values.account_no);
               $('.editEmployee #pfa').val(values.pfa);
               $('.editEmployee #pfa_pin').val(values.pfa_pin);
               $('.editEmployee #leave_status').val(values.leave_status);
               $('.editEmployee #exit_type').val(values.exit_type);
               $('.editEmployee #exit_date').val(values.exit_date);
               $('.editEmployee #date_added').val(values.date_added);
               $('.editEmployee #annual_leave_taken').val(values.annual_leave_taken);
               $('.editEmployee #compassionate_leave_taken').val(values.compassionate_leave_taken);
            }
         });
      });
   </script>
   <script>
      new AutoNumeric('#annual_sal');
   </script>
</body>

</html>