<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';


//Get ID and redirect id ID is not an integer
if (isset($_GET['id'])) {
   $id = $_GET['id'];

   if (!is_numeric($id)) {
      header('Location:index.php');
      exit;
   }
   //Set Filter to current ID
   $filter = "period_id = $id";
} else {
   //Set Filter to 0
   $filter = "period_id > 0";
}



?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Payroll Summary | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Payroll Summary</h2>
                        <div class="clearfix"></div>
                     </div>

                     <div class="x_content">
                        <div class="col-md-12">
                           <div class="col-md-5 col-sm-6 col-xs-12">
                              <select class="filter_period select2_group form-control" name="filter_period" id="filter_period" required>
                                 <option value="" selected>Payroll Period</option>
                                 <?php
                                 getAllPeriods();
                                 ?>
                              </select>
                           </div>
                        </div>
                        <br>
                     </div>
                     <div class="mt-20 col-md-12">
                        <!-- //Display Feedback Message -->
                        <?php echo $msg->display(); ?>
                     </div>
                     <table id="datatable-buttons" class="table table-striped table-bordered currency">
                        <thead>
                           <tr>
                              <th> </th>
                              <th>ID</th>

                              <th>Payroll Period</th>
                              <th>Total Workdays</th>
                              <th>Employee</th>
                              <th>Daily Rate</th>
                              <th>Days Worked</th>
                              <th>Days Absent</th>
                              <th>Gross Salary</th>
                              <th style='display:none;'>CRA</th>
                              <th style='display:none;'>Non-Taxable</th>
                              <th style='display:none;'>Taxable </th>
                              <th style='display:none;'>Employer Contribution</th>
                              <th>PAYE</th>
                              <th>Pension</th>
                              <th style='display:none;'>IOU</th>
                              <th style='display:none;'>Loan</th>
                              <th style='display:none;'>Lateness </th>
                              <th style='display:none;'>Absenteeism</th>
                              <th style='display:none;'>Suspension</th>
                              <th style='display:none;'>Overtime</th>
                              <th style='display:none;'>Overtime Days</th>
                              <th style='display:none;'>Other Deduction</th>
                              <th>Total Stat Deductions</th>
                              <th>Total Other Deductions</th>
                              <th>Net Salary</th>
                              <th>Date Prepared</th>
                              <th>Prepared By</th>
                              <th>Review Status</th>
                              <th>Date Reviewed</th>
                              <th>Revieweed By</th>
                              <th style='display:none;'>Payroll Cycle</th>
                              <th style='display:none;'>Basic</th>
                              <th style='display:none;'>Housing</th>
                              <th style='display:none;'>Transport</th>
                              <th style='display:none;'>Entertainment</th>
                              <th style='display:none;'>Welfare</th>
                              <th style='display:none;'>Others</th>
                              <th style='display:none;'>Leave Allowance</th>
                        </thead>

                        <tbody id="records_list">
                           <?php
                           getAllPayrollSummary($filter);
                           ?>
                        </tbody>
                     </table>
                  </div>


                  <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                     <div class="modal-dialog modal-md">
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                              </button>
                              <h4 class="modal-title" id="myModalLabel2">View Records</h4>
                           </div>
                           <div class="modal-body">

                              <form class="viewrecords form-horizontal form-label-left input_mask" method="POST" id="viewrecords" name="viewrecords" name="viewrecords">
                                 <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                       <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Earnings</a>
                                       </li>
                                       <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Deductions</a>
                                       </li>
                                       <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Summary</a>
                                       </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                       <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                          <input type="hidden" id="payroll_id" name="payroll_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                          <input type="hidden" id="emp_id" name="emp_id" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                          <table class="table table-bordered table-striped">
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payroll_id">Payroll ID <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="payroll_id" name="payroll_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee_name">Employee <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="employee_name" name="employee_name" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="days_worked">Days Worked <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="days_worked" name="days_worked" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="days_absent">Days Absent <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="days_absent" name="days_absent" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monthly_gross">Gross Pay <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_gross" name="monthly_gross" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="basic">Basic <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="basic" name="requested_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="housing">Housing <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="housing" name="housing" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="transport">Transport <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="transport" name="transport" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="entertainment">Entertainment <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="entertainment" class="form-control" name="entertainment" required="" readonly />

                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="welfare">Welfare <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="welfare" class="form-control" name="welfare" required="" readonly />
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="others">Others<span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="others" name="others" class="form-control" class="form-control" required="" readonly />
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </div>

                                       <!-- Panel 2 -->
                                       <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                          <table class="table table-bordered table-striped">
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paye">PAYE <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_paye" name="monthly_paye" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monthly_pension">Employee <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_pension" name="monthly_pension" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="iou">IOU <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="iou" name="iou" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="loan">Loan <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="loan" name="loan" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lateness">Lateness <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="lateness" name="lateness" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="absenteeism">Absenteeism <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="absenteeism" name="absenteeism" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="suspension">Suspension <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="suspension" name="suspension" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="other_deduction">Other Deduction <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="other_deduction" name="other_deduction" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </div>
                                       <!-- Panel 3 -->
                                       <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                          <table class="table table-bordered table-striped">
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paye">PAYE <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_paye" name="monthly_paye" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monthly_cra">Consolidated Reliefs <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_cra" name="monthly_cra" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monthly_non_taxable"> NonTaxable Income<span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_non_taxable" name="monthly_non_taxable" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="monthly_taxable"> Taxable Income<span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="monthly_taxable" name="monthly_taxable" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_stat_deductions">Total Stat Deductions <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="total_stat_deductions" name="lateness" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_other_deduc">Total Other Deductions <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="total_other_deduc" name="total_other_deduc" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_salary">Net Salary <span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="total_salary" name="total_salary" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_created">Date Prepared<span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="date_created" name="date_created" required="required" class="form-control col-md-7 col-xs-12" readonly> 
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>
                                                   <div class="form-group">
                                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prepared_by">Prepared By<span class="required">*</span>
                                                      </label>
                                                      <div class="col-md-9 col-sm-9 col-xs-12">
                                                         <input type="text" id="prepared_by" name="prepared_by" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                      </div>
                                                   </div>
                                                </td>
                                             </tr>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>




               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /page content -->

   <!-- footer content -->
   <footer>
      <div class="pull-right">
         &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
      </div>
      <div class="clearfix"></div>
   </footer>
   <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script type="text/javascript">
      $(document).ready(function() {
         $('#filter_period').on('change', function() {
            var period = $(this).val();
            if (period) {
               var myurl = 'payroll_summary.php';
               window.location.replace(myurl + '?id=' + period);
            } else {
               alert('Error! Something went wrong');
            }
         });
      });
   </script>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');


         $.ajax({

            url: 'records.php?action=payroll&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {

               $('.viewrecords #payroll_id').val(values.payroll_id);
               $('.viewrecords #generated_id').val(values.generated_id);
               $('.viewrecords #period_id').val(values.period_id);
               $('.viewrecords #period_name').val(values.period_name);
               $('.viewrecords #total_workdays').val(values.total_workdays);
               $('.viewrecords #employee_name').val(values.employee_name);
               $('.viewrecords #lastname').val(values.lastname);
               $('.viewrecords #firstname').val(values.firstname);
               $('.viewrecords #monthly_gross').val(values.monthly_gross);
               $('.viewrecords #daily_rate').val(values.daily_rate);
               $('.viewrecords #days_worked').val(values.days_worked);
               $('.viewrecords #days_absent').val(values.days_absent);
               $('.viewrecords #employer_contrib').val(values.employer_contrib);
               $('.viewrecords #monthly_paye').val(values.monthly_paye);
               $('.viewrecords #monthly_pension').val(values.monthly_pension);
               $('.viewrecords #monthly_cra').val(values.monthly_cra);
               $('.viewrecords #monthly_non_taxable').val(values.monthly_non_taxable);
               $('.viewrecords #monthly_taxable').val(values.monthly_taxable);
               $('.viewrecords #iou').val(values.iou);
               $('.viewrecords #loan').val(values.loan);
               $('.viewrecords #lateness').val(values.lateness);
               $('.viewrecords #absenteeism').val(values.absenteeism);
               $('.viewrecords #suspension').val(values.suspension);
               $('.viewrecords #overtime').val(values.overtime);
               $('.viewrecords #overtime_days').val(values.overtime_days);
               $('.viewrecords #other_deduction').val(values.other_deduction);
               $('.viewrecords #total_stat_deductions').val(values.total_stat_deductions);
               $('.viewrecords #total_other_deduc').val(values.total_other_deduc);
               $('.viewrecords #total_salary').val(values.total_salary);
               $('.viewrecords #date_created').val(values.date_created);
               $('.viewrecords #prepared_by').val(values.prepared_by);
               $('.viewrecords #review_status').val(values.review_status);
               $('.viewrecords #review_date').val(values.review_date);
               $('.viewrecords #reviewed_by').val(values.reviewed_by);
               $('.viewrecords #payroll_cycle').val(values.payroll_cycle);
               $('.viewrecords #days_late').val(values.days_late);
               $('.viewrecords #basic').val(values.basic);
               $('.viewrecords #housing').val(values.housing);
               $('.viewrecords #transport').val(values.transport);
               $('.viewrecords #entertainment').val(values.entertainment);
               $('.viewrecords #welfare').val(values.welfare);
               $('.viewrecords #others').val(values.others);
               $('.viewrecords #leave_allowance').val(values.leave_allowance);

            }
         });
      });
   </script>

   <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
   <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
   <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

   <script>
      const anElement = AutoNumeric.multiple('#td', 0, {
         suffixText: "%"
      });
   </script>
</body>

</html>