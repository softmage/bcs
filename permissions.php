<?php

if (session_id() == "") session_start(); // Init session data
ob_start(); // Turn on output buffering

include 'core.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();

// A. Get all Pages from Directory
$pages = fetchPages();
function fetchPages()
{
    $all_pages = getcwd();
    $files = array_slice(scandir($all_pages), 0);
    $all_pages = scandir($all_pages);
    $arr = array_diff($all_pages, array("login.php", "logout.php","change_password.php","core.php","index.php","records.php","forgot_password.php"));
    $pages =  preg_grep("/" . preg_quote(".php") . "/", $arr);
    sort($pages);

    return $pages;
}


// B. Get Level ID
if (isset($_GET['token'])) {

    $id = $_GET['token'];
    //Ensure token is a string - prevent xss attack and sql Injection
    if (!is_numeric($id)) {
        header('Location:index.php');
        exit;
    }    
    $levelid = mysqli_real_escape_string($conn, $_GET['token']);
    
    //Get Level Name of Token
    $sql = "SELECT level_name FROM user_levels WHERE level_id = $levelid";
    $result = mysqli_query($conn, $sql);
    $lev_name = mysqli_fetch_all($result, MYSQLI_ASSOC);
    foreach ($lev_name as $lev_name) {
        $level_name = $lev_name['level_name'];
    }
}


// C. Add New Permission for UserLevel
if (isset($_POST['grant_permission'])) {
    foreach ($_POST['tname'] as $selected) {

        $sql1 = "INSERT INTO user_permissions(page, level_id) VALUES ('$selected',$levelid) ";
        $result1 = mysqli_query($conn, $sql1);
    }
    if ($result1) {

        $msg->success('Great! Permission Added.', 'permissions.php?token='.$levelid.'');       
    }
}


// D. Fetch All UserLevels from Userlevel table to populate Level elect Form
$row = fetchLevels();

function fetchLevels()
{
    global $conn;

    $sql = "SELECT DISTINCT level_id, user_role from users";
    $result = mysqli_query($conn, $sql);
    //$row = mysqli_fetch_assoc($result);
    $row = mysqli_fetch_all($result, MYSQLI_ASSOC);

    return $row;
}


// E. Display existing Permissions for Selected UserLevel
function permissionsTable($id)
{
    global $conn;
    $sql = "SELECT DISTINCT page as tablename from user_permissions WHERE level_id = $id";
    $result = mysqli_query($conn, $sql);
    $permissions = mysqli_fetch_all($result, MYSQLI_ASSOC);

    return $permissions;
}

if (isset($_GET['token'])) {
    $id = $_GET['token'];
    $permissions = permissionsTable($id);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

    <title>User Permissions | <?php echo APPNAME; ?> </title>

    <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <?php include_once 'includes/navigation.php'; ?>

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel tile">
                            <div class="x_title">
                                <h2>Permissions |<small><?php if(!empty($level_name)) :?> <?php echo $level_name; ?> <?php endif ?></small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="">&nbsp;</a></li>
                                    <li><a class="">&nbsp;</a></li>
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content ">
                                <div class="col-md-12">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <select class="select2_group form-control" name="user_level" required id="user_level">
                                            <option value="" selected>Select Level</option>
                                            <?php
                                            foreach ($row as $ulevel) {
                                                $level = $ulevel['level_id'];
                                                $user_role = $ulevel['user_role'];

                                                echo " <option value='$level'>$user_role</option>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                       <?php if(!empty($level_name)) :?> <button class='btn btn-primary' data-toggle="modal" data-target=".resetstatus_modal">Add Permissions</button> <?php endif ?>
                                    </div>
                                </div>

                                <div class="mt-20 col-md-12 ">
                                    <div class="col-md-5 col-sm-6 col-xs-12">
                                        <?php echo $msg->display(); ?>
                                        <form class="form-horizontal form-label-left input_mask" method="POST">
                                            <div class="form-group">
                                                <div class="gcheckbox">
                                                    <table class='table table-striped mt-20'>
                                                        <?php
                                                        if (!empty($_GET['token'])) {
                                                            foreach ($permissions as $permission) {
                                                                $tablename = $permission['tablename'];
                                                                echo "<tr>
                                                        <td> $tablename </td>
                                                        <td> <button class='trash btn btn-xs btn-danger' type='button' data-toggle='tooltip' title='Delete Permission' id='$tablename' ><i class='fa fa-trash'></i></button>
                                                        </td>
                                                        </tr>";
                                                            }
                                                        }

                                                        ?></table>
                                                </div>
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>

                                <div class="modal fade resetstatus_modal" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel2">Grant Permission | <small><?php echo $level_name; ?> </small></h4>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal form-label-left input_mask" method="POST">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select permissions</label>
                                                        <div class="col-md-9 col-sm-9 col-xs-12"><table>
                                                            <?php
                                                            foreach ($pages as $page) {
                                                                echo " <tr><td><label> $page </label></td><td>&nbsp;</td> <td><input type='checkbox' name='tname[]' id='tname' value='$page' /></td></tr>";
                                                            }
                                                            ?></table> 
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <br>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <button type="submit" class="btn btn-success" name="grant_permission">Grant</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
    </div>

    <?php include 'includes/scripts.php'; ?>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>

    <!-- LOAD EMployee's records from DB -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#user_level').on('change', function() {
                var user_levelID = $(this).val();
                if (user_levelID) {
                    var myurl = 'permissions.php';
                    window.location.replace(myurl + '?token=' + user_levelID);
                }
            });

        });
    </script>

    <script>
        $(function() {
            $(document).on('click', '.trash', function() {
                if (confirm('Sure To Remove This Record ?')) {

                    var del_id = $(this).attr('id');
                    var $ele = $(this).parent().parent();
                    $.ajax({
                        //type: 'POST',
                        url: 'records.php?action=delete&id=' + del_id,
                        type: 'GET',
                        success: function(data) {
                            if (data === "YES") {
                                $ele.fadeOut().remove();
                            } else {
                                alert("can't delete the row")

                            }
                        }

                    });
                }
            });
        });
    </script>
</body>

</html>