<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';
$filter = "emp_id = $user_id";

//Get ID and redirect id ID is not an integer
if (isset($_GET['id'])) {
   $id = $_GET['id'];

   if (!is_numeric($id)) {
      header('Location:pension_remittance.php');
      exit;
   }

   $payslip = printPayslip($id, $user_id);

   foreach ($payslip as $data) {

      $payroll_id =  $data['payroll_id'];
      $period_name =  $data['period_name'];
      $emp_id =  $data['emp_id'];
      $employee =  $data['employee'];
      $monthly_gross =  number_format($data['monthly_gross'], 2);
      $daily_rate =  number_format($data['daily_rate'], 2);
      $days_worked =  $data['days_worked'];
      $monthly_paye =  number_format($data['monthly_paye'], 2);
      $monthly_pension =  number_format($data['monthly_pension'], 2);
      $loan =  number_format($data['loan'], 2);
      $lateness =  number_format($data['lateness'], 2);
      $absenteeism =  number_format($data['absenteeism'], 2);
      $other_deduction =  number_format($data['other_deduction'], 2);
      $total_stat_deductions =  number_format($data['total_stat_deductions'], 2);
      $total_other_deduc =  number_format($data['total_other_deduc'], 2);
      $total_salary =  number_format($data['total_salary'], 2);
      $date_created =  $data['date_created'];
      $prepared_by =  $data['prepared_by'];
      $bank_name =  $data['bank_name'];
      $account_no =  $data['account_no'];
      $staff_no =  $data['staff_no'];
      $department =  $data['department'];
   }
} else {
   header('Location:my_payslips.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>My Payslips | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Print Pay Slip</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div class="row"></div>
                     </div>

                     <div class="col-md-12">
                        <div class="col-md-9">

                           <style type="text/css">
                              table.tableizer-table {
                                 /* font-size: 12px; */
                                 /* border: 1px solid #CCC; */
                                 font-family: Arial, Helvetica, sans-serif;
                              }

                              table.tableizer-table td,
                              tr {
                                 border: 0px #fff;
                              }

                              .headtitle {
                                 text-align: right;
                                 font-size: 20px;
                                 height: 70px;
                              }


                              .addresstext {
                                 background-color: #104E8B;
                                 color: #FFF;
                                 text-align: center;
                                 font-size: 9px;
                              }

                              .topborder {
                                 border-top: 2px solid #CCC !important;
                              }

                              .normaltitle {
                                 text-align: left;
                                 font-weight: bold;
                                 background: #ccc;
                                 color: #fff;
                                 font-size: 14px !important;
                              }

                              .noborder_padding>td {
                                 border: 0px #fff !important;
                                 padding: 0px;

                              }

                              .tableizer-table td {
                                 padding: 4px;
                                 margin: 3px;
                                 border: 1px solid #CCC;
                              }

                              .tableizer-table th {
                                 background-color: #104E8B;
                                 color: #FFF;
                                 font-weight: bold;
                              }
                           </style>
                           <table class="tableizer-table" id="viewrecords">
                              <tr>
                                 <th style="text-align:center;font-size:20px" ; colspan="5"> <i class="fa fa-trophy"></i> 3Aces Consulting Ltd </span></th>
                              </tr>
                              <tr>
                                 <td class="addresstext" colspan="5">Km 12, Lekki-Epe Expressway, Lagos, Nigeria<br></td>
                              </tr>
                              <tbody>
                                 <tr class="headtitle">
                                    <td colspan="5">Pay Slip <br></td>
                                 </tr>

                                 <tr class="normaltitle topborder">
                                    <td>Staff Name</td>
                                    <td>Staff No</td>
                                    <td>Department</td>
                                    <td>Period</td>
                                    <td>Date Prepared</td>
                                 </tr>
                                 <tr>
                                    <td><?php echo "$employee"; ?></td>
                                    <td><?php echo "$staff_no"; ?></td>
                                    <td><?php echo "$department"; ?></td>
                                    <td><?php echo "$period_name"; ?></td>
                                    <td><?php echo "$date_created"; ?></td>
                                 </tr>
                                 <tr class="noborder_padding">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                                 <tr class="normaltitle topborder">
                                    <td>Description</td>
                                    <td>Type &nbsp;&nbsp;</td>
                                    <td align="right">Rate</td>
                                    <td>Days Worked &nbsp;&nbsp;</td>
                                    <td align="right">Amount</td>
                                 </tr>
                                 <tr>
                                    <td>Ordinary Pay</td>
                                    <td>Salary</td>
                                    <td align='right'>&#8358;<?php echo "$daily_rate"; ?></td>
                                    <td align='center'><?php echo "$days_worked"; ?></td>
                                    <td align='right'>&#8358;<?php echo "$monthly_gross"; ?></td>
                                 </tr>
                                 <tr>
                                    <td>Deductions A</td>
                                    <td>Statutory</td>
                                    <td align='right'></td>
                                    <td align='center'></td>
                                    <td align='right'>&#8358;<?php echo "$total_stat_deductions"; ?></td>
                                 </tr>
                                 <tr>
                                    <td>Deductions B</td>
                                    <td>Non-Statutory&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td align='right'></td>
                                    <td align='center'></td>
                                    <td align='right'>&#8358;<?php echo "$total_other_deduc"; ?></td>
                                 </tr>
                                 <tr class="noborder_padding">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                                 <tr class="normaltitle topborder">
                                    <td>Details</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td style="font-weight:bold;" colspan="3">Earnings</td>
                                    <td style="font-weight:bold;" colspan="2">Deductions</td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Taxable Allowances</td>
                                    <td align='center'><?php echo "$monthly_gross"; ?></td>
                                    <td>PAYE</td>
                                    <td align='right'><?php echo "$monthly_paye"; ?></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">One-off Payement</td>
                                    <td align='center'> - </td>
                                    <td>Pension</td>
                                    <td align='right'><?php echo "$monthly_pension"; ?></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Termination Pay</td>
                                    <td align='center'> - </td>
                                    <td>Loan</td>
                                    <td align='right'><?php echo "$loan"; ?></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Refunds</td>
                                    <td align='center'> - </td>
                                    <td>IOU</td>
                                    <td align='right'><?php echo "$lateness"; ?></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2"></td>
                                    <td align='center'> - </td>
                                    <td>Absenteeism</td>
                                    <td align='right'><?php echo "$absenteeism"; ?></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2"></td>
                                    <td align='center'> - </td>
                                    <td>Others</td>
                                    <td align='right'><?php echo "$other_deduction"; ?></td>
                                 </tr>
                                 <tr class="normaltitle topborder">
                                    <td colspan="3">&nbsp;</td>
                                    <td>Net Pay</td>
                                    <td align='right'>&#8358;<?php echo "$total_salary"; ?></td>
                                 </tr>
                                 <tr class="noborder_padding">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                                 <tr class="noborder_padding">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                                 <tr class="normaltitle topborder">
                                    <td colspan="5">Direct Credit Details</td>
                                 </tr>
                                 <tr>
                                    <td colspan="2"><strong>Bank Name:</strong> <?php echo "$bank_name"; ?></td>
                                    <td colspan="2"><strong>Account No: </strong><?php echo "$account_no"; ?></td>
                                    <td align="right" id="total_salary">&#8358;<?php echo "$total_salary"; ?></td>
                                 </tr>
                              </tbody>
                           </table>

                           <div class="row no-print">
                           <div class="col-md-8">
                              <br> &nbsp;
                              <button class="btn btn-primary pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                           </div>
                        </div>

                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /page content -->

         <!-- footer content -->
         <footer>
            <div class="pull-right">
               &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
            </div>
            <div class="clearfix"></div>
         </footer>
         <!-- /footer content -->
      </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(function() {
         // Initialize form validation on the registration form.
         // It has the name attribute "registration"
         $("#addEmployee").validate({
            ignore: "",
            // $("form[name='addEmployee']").validate({ ignore: "" 
            // Specify validation rules
            rules: {
               firstname: {
                  required: true
               },
               lastname: {
                  required: true
               },
               nationality: {
                  required: true
               },
               dob: {
                  required: true
               },
               gender: {
                  required: true
               },
               marital_status: {
                  required: true
               }

            },

            // Specify validation error messages
            messages: {
               firstname: "Please enter your firstname",
               lastname: "Please enter your lastname",
               // objective: {
               //     required: "Please enter Objective",
               //     maxlength: "Description should not be more than 500 characters"
               // },
               // role_obj_id: {
               //     required: "Please select Role/Designation Objective"
               // }
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
               form.submit();
            }
         });
      });
   </script>

   <script>
      // $(document).on('click', '.view', function() {
      $(document).ready(function() {
         var dataID = < ? php echo "$id"; ? > ;


         $.ajax({

            url: 'records.php?action=payroll&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {

               $('.viewrecords #payroll_id').val(values.payroll_id);
               $('.viewrecords #generated_id').val(values.generated_id);
               $('.viewrecords #period_id').val(values.period_id);
               $('.viewrecords #period_name').val(values.period_name);
               $('.viewrecords #total_workdays').val(values.total_workdays);
               $('.viewrecords #employee_name').val(values.employee_name);
               $('.viewrecords #lastname').val(values.lastname);
               $('.viewrecords #firstname').val(values.firstname);
               $('.viewrecords #monthly_gross').val(values.monthly_gross);
               $('.viewrecords #daily_rate').val(values.daily_rate);
               $('.viewrecords #days_worked').val(values.days_worked);
               $('.viewrecords #days_absent').val(values.days_absent);
               $('.viewrecords #employer_contrib').val(values.employer_contrib);
               $('.viewrecords #monthly_paye').val(values.monthly_paye);
               $('.viewrecords #monthly_pension').val(values.monthly_pension);
               $('.viewrecords #monthly_cra').val(values.monthly_cra);
               $('.viewrecords #monthly_non_taxable').val(values.monthly_non_taxable);
               $('.viewrecords #monthly_taxable').val(values.monthly_taxable);
               $('.viewrecords #iou').val(values.iou);
               $('.viewrecords #loan').val(values.loan);
               $('.viewrecords #lateness').val(values.lateness);
               $('.viewrecords #absenteeism').val(values.absenteeism);
               $('.viewrecords #suspension').val(values.suspension);
               $('.viewrecords #overtime').val(values.overtime);
               $('.viewrecords #overtime_days').val(values.overtime_days);
               $('.viewrecords #other_deduction').val(values.other_deduction);
               $('.viewrecords #total_stat_deductions').val(values.total_stat_deductions);
               $('.viewrecords #total_other_deduc').val(values.total_other_deduc);
               $('.viewrecords #total_salary').val(values.total_salary);
               $('.viewrecords #date_created').val(values.date_created);
               $('.viewrecords #prepared_by').val(values.prepared_by);
               $('.viewrecords #review_status').val(values.review_status);
               $('.viewrecords #review_date').val(values.review_date);
               $('.viewrecords #reviewed_by').val(values.reviewed_by);
               $('.viewrecords #payroll_cycle').val(values.payroll_cycle);
               $('.viewrecords #days_late').val(values.days_late);
               $('.viewrecords #basic').val(values.basic);
               $('.viewrecords #housing').val(values.housing);
               $('.viewrecords #transport').val(values.transport);
               $('.viewrecords #entertainment').val(values.entertainment);
               $('.viewrecords #welfare').val(values.welfare);
               $('.viewrecords #others').val(values.others);
               $('.viewrecords #leave_allowance').val(values.leave_allowance);

            }
         });
      });
   </script>
</body>

</html>