<?php
require_once 'core.php';

$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

//Load Employee Record
if (isset($_GET['id']) && $_GET['action'] == 'view') {
   $id = $_GET['id'];

   viewEmployee($id);
}

//Load LeaveRequest Record
if (isset($_GET['id']) && $_GET['action'] == 'leave') {
   $id = $_GET['id'];

   viewLeaveRequest($id);
}

//Load Attendance Record
if (isset($_GET['id']) && $_GET['action'] == 'attendance') {
   $id = $_GET['id'];

   viewSingleAttendance($id);
}

//Load Payroll Record
if (isset($_GET['id']) && $_GET['action'] == 'payroll') {
   $id = $_GET['id'];

   getSinglePayrollData($id);
}

//Delete User Permission
if (!empty($_GET['action'])) {
   if ($_GET['action'] == 'delete' && isset($_GET['id'])) {

       $id = $_GET['id'];

       deleteLevelPermissions($id);
   }
}

//Fetch Level Name 
function getLevelName($id)
{
    global $conn;

    $sql = "DELETE FROM appraisal_permissions WHERE page = '$id' ";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo "YES";
    } else {
        echo "NO";
    }
}