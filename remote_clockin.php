<?php


if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

if (isset($_POST['remote_clockin'])) {
   remoteClockIn();
}

require_once 'functions/FlashMessages.php';
$msg = new \Plasticbrain\FlashMessages\FlashMessages();
?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Remote ClockIn | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">

            <br />

            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Remote ClockIn</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">

                        <div class="col-md-6">

                           <?php echo $msg->display(); ?>

                           <blockquote>
                              <p>Only use this page if you are working outside the Office/Branch and you want to ClockIn from a remote Location/Branch.</p>

                           </blockquote>

                           <form class="requestLeave form-horizontal form-label-left input_mask" method="POST" id="requestLeave" name="requestLeave">

                              <input type="hidden" id="emp_id" name="emp_id" required="required" value="<?php echo "$user_id"; ?>" class="form-control col-md-7 col-xs-12">
                              <input type='hidden' name='lat' id='lat' required>
                              <input type='hidden' name='longt' id='longt' required>

                              <table class="table table-bordered table-striped">
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relief_officer">Branch/Location<span class="required">*</span>
                                          </label>
                                          <div class="col-md-9 col-sm-9 col-xs-12">
                                             <select id="branch" name="branch" class="form-control col-md-7 col-xs-12" required="">
                                                <option value="">Choose..</option>
                                                <?php getallBranches(); ?>>
                                             </select>
                                          </div>
                                       </div>
                                    </td>
                                 </tr>
                              </table>

                              <div class="form-group">
                                 <div class="col-md-6 col-xs-6 col-md-offset-3">
                                    <button type="submit" name="remote_clockin" class="btn btn-primary">ClockIn</button>

                                 </div>
                              </div>
                           </form>

                        </div>


                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(document).ready(function() {

         var geo_options = {
            enableHighAccuracy: true,
            maximumAge: 60,
            timeout: 27000
         };


         if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showLocation);
         } else {
            $('#location').html('Geolocation is not supported by this browser.');
         }
      });

      function showLocation(position, geo_options) {
         var lat = position.coords.latitude;
         var longt = position.coords.longitude;

         document.getElementById("lat").value = lat;
         document.getElementById("longt").value = longt;
      }
   </script>
</body>

</html>