<?php

require_once __DIR__ . '../../vendor/autoload.php';

include("../config/config.php");


if (isset($_GET['id'])) {
   $id = $_GET['id'];

   if (!is_numeric($id)) {
       header('Location:../pension_remittance.php');
       exit;
   }

   $sql = mysqli_query($conn, 'SELECT period_name FROM period WHERE id = '.$id.'');
   while($row = mysqli_fetch_array($sql)){
      $pname = $row['period_name'];
   }

   //Set Filter to current ID
   $filter = "period_id = $id";
} else {
   //Set Filter to 0
   $filter = "period_id > 0";
}

$mpdf = new \Mpdf\Mpdf([
'mode' => 'utf-8',
'orientation' => 'L',
'aliasNbPg' => '[pagetotal]'
]);
$mpdf->SetHeader(''. $pname. ' PAYE Remittance Schedule {PAGENO}/{nbpg}');
$mpdf->WriteHTML('<h2>Ace!HRM - '. $pname. ' PAYE Remittance Schedule</h2>');
$result=mysqli_query($conn,'SELECT period_name, employee_name, monthly_gross, monthly_pension, monthly_cra, monthly_non_taxable, monthly_taxable, monthly_paye, date_created, prepared_by FROM paye_remittance WHERE  '.$filter . ' ORDER by period_id, employee_name');

$html = '<table style="border: 1px solid black; border-collapse:collapse;" >
<thead>
    <tr>		
		<th style="border: 1px solid black">Period</th>
		<th style="border: 1px solid black">Employee</th>
		<th style="border: 1px solid black">Gross Sal </th>
		<th style="border: 1px solid black">Pension</th>
		<th style="border: 1px solid black">CRA</th>
		<th style="border: 1px solid black">Non-Taxable Inc</th>
		<th style="border: 1px solid black">Taxable Inc</th>
		<th style="border: 1px solid black">PAYE</th>
		<th style="border: 1px solid black">Date Prepared</th>
		<th style="border: 1px solid black">Prepared by</th>
	</tr>
</thead>';
while($rows = mysqli_fetch_array($result))
{
 
 $html .= '
    <tr> 
    <td style="border: 1px solid black;">' . $rows['period_name'] . '</td>
    <td style="border: 1px solid black;">' . $rows['employee_name'] . '</td>
    <td style="border: 1px solid black;text-align:right">' . number_format($rows['monthly_gross'],2) . '</td>
    <td style="border: 1px solid black;text-align:right">' . number_format($rows['monthly_pension'],2) . '</td>
	<td style="border: 1px solid black;text-align:right">' . number_format($rows['monthly_cra'],2) . '</td>
	<td style="border: 1px solid black;text-align:right">' . number_format($rows['monthly_non_taxable'],2) . '</td>
	<td style="border: 1px solid black;text-align:right">' . number_format($rows['monthly_taxable'],2) . '</td>
	<td style="border: 1px solid black;text-align:right; font-weight:bold">' . number_format($rows['monthly_paye'],2) . '</td>
	<td style="border: 1px solid black;text-align:right">' . $rows['date_created'] . '</td>
	<td style="border: 1px solid black">' . $rows['prepared_by'] . '</td>
    </tr>';     
     
}
		
$html .= '</table>';
$mpdf->WriteHTML($html);



$mpdf->Output();