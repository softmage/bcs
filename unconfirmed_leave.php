<?php

if (session_id() == "") session_start();
ob_start();

require_once 'core.php';

if (isset($_POST['cancel_leave'])) {

   cancelLeaveRequest();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <!-- Meta, title, CSS, favicons, etc. -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="assets/images/favicon.ico" type="image/ico" />

   <title>Unconfirmed Leave | <?php echo APPNAME; ?> </title>

   <?php include_once 'includes/stylesheets.php'; ?>

</head>

<body class="nav-md">
   <div class="container body">
      <div class="main_container">
         <?php include_once 'includes/navigation.php'; ?>

         <!-- page content -->
         <div class="right_col" role="main">
            <br />
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel tile">
                     <div class="x_title">
                        <h2>Unconfirmed Leave</h2>
                        <div class="clearfix"></div>
                     </div>
                     <div class="x_content">
                        <div class="row"></div>
                     </div>

                     <div class="col-md-12">
                        <div class="col-md-6">
                           <!-- //Display Feedback Message -->
                           <?php echo $msg->display(); ?>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                           <thead>
                              <tr>
                                 <th> </th>
                                 <th>Leave ID</th>
                                 <th>Emp ID</th>
                                 <th>Employee</th>
                                 <th>Leave Status</th>
                                 <th>Leave Type</th>
                                 <th>Duration</th>
                                 <th>Start Date</th>
                                 <th style='display:none;'>End Date</th>
                                 <th>Resumption</th>
                                 <th style='display:none;'>Remarks</th>
                                 <th>Relief Offficer</th>
                                 <th>Date Submitted</th>
                                 <th style='display:none;'>Uploads</th>
                                 <th>Line Manager</th>
                                 <th>Review Status</th>
                                 <th>Approved Days</th>
                                 <th style='display:none;'>Remarks</th>
                                 <th>Reviewed By</th>
                                 <th>Review Date</th>
                           </thead>

                           <tbody id="records_list">
                              <?php
                              getUnconfirmedLeave();
                              ?>
                           </tbody>
                        </table>
                     </div>

                     <div class="modal fade view_report" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                 </button>
                                 <h4 class="modal-title" id="myModalLabel2">Cancel Leave Request</h4>
                              </div>
                              <div class="modal-body">

                                 <form class="viewrecords form-horizontal form-label-left input_mask" method="POST" id="viewrecords" name="viewrecords" name="viewrecords">
                                    <input type="hidden" id="leave_id" name="leave_id" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                    <input type="hidden" id="emp_id" name="emp_id" required="required" class="form-control col-md-7 col-xs-12" readonly>

                                    <table class="table table-bordered table-striped">
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee_name">Employee <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="employee_name" name="employee_name" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="leave_type">Leave Type <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="leave_type" name="leave_type" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="requested_days">Duration <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="requested_days" name="requested_days" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="request_date">Date Submitted <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <input type="text" id="request_date" name="request_date" required="required" class="form-control col-md-7 col-xs-12" readonly>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_approval">Confirmation Status<span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <select name="hr_approval" id="hr_approval" class="form-control col-md-7 col-xs-12" required="">
                                                      <option value="">Choose..</option>
                                                      <option>Cancelled</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hr_remarks">Cancellation Reasons <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                   <textarea name="cancellation_reasons" id="cancellation_reasons" class="form-control col-md-7 col-xs-12" rows="2" required></textarea>
                                                </div>
                                             </div>
                                          </td>
                                       </tr>
                                    </table>
                                    <div class="col-md-12 col-xs-12 form-group has-feedback">
                                       <hr>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-md-12 col-xs-12 col-md-offset-3">
                                          <button type="submit" name="cancel_leave" class="btn btn-primary">Cancel Leave</button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
      <footer>
         <div class="pull-right">
            &copy; <?php echo date("Y"); ?> All Rights Reserved. 3Aces Consulting Ltd
         </div>
         <div class="clearfix"></div>
      </footer>
      <!-- /footer content -->
   </div>
   </div>

   <?php include 'includes/scripts.php'; ?>

   <script>
      $(document).on('click', '.view', function() {
         // $(document).ready(function() {
         var dataID = $(this).attr('id');


         $.ajax({

            url: 'records.php?action=leave&id=' + dataID,
            type: 'GET',
            dataType: 'json',
            context: this,
            success: function(values) {

               $('.viewrecords #leave_id').val(values.leave_id);
               $('.viewrecords #emp_id').val(values.emp_id);
               $('.viewrecords #employee_name').val(values.employee_name);
               $('.viewrecords #leave_status').val(values.leave_status);
               $('.viewrecords #leave_type').val(values.leave_type);
               $('.viewrecords #requested_days').val(values.requested_days);
               $('.viewrecords #commence_date').val(values.commence_date);
               $('.viewrecords #end_date').val(values.end_date);
               $('.viewrecords #resumption_date').val(values.resumption_date);
               $('.viewrecords #remarks').val(values.remarks);
               $('.viewrecords #relief_officer').val(values.relief_officer);
               $('.viewrecords #relief_officer_name').val(values.relief_officer_name);
               $('.viewrecords #request_date').val(values.request_date);
               $('.viewrecords #leave_upload').val(values.leave_upload);
               $('.viewrecords #line_mgr_id').val(values.line_mgr_id);
               $('.viewrecords #linemgr_name').val(values.linemgr_name);
               $('.viewrecords #linemgr_review').val(values.linemgr_review);
               $('.viewrecords #linemgr_days').val(values.linemgr_days);
               $('.viewrecords #line_mgr_rmks').val(values.line_mgr_rmks);
               $('.viewrecords #leave_reviewer').val(values.leave_reviewer);
               $('.viewrecords #linemgr_review_date').val(values.linemgr_review_date);
               $('.viewrecords #hr_approval').val(values.hr_approval);
               $('.viewrecords #hr_days').val(values.hr_days);
               $('.viewrecords #hr_leavecommence_date').val(values.hr_leavecommence_date);
               $('.viewrecords #hr_resume_date').val(values.hr_resume_date);
               $('.viewrecords #hr_remarks').val(values.hr_remarks);
               $('.viewrecords #hr_approval_date').val(values.hr_approval_date);
               $('.viewrecords #cancellation_reasons').val(values.cancellation_reasons);
            }
         });
      });
   </script>

   <script src="assets/js/datatables/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="assets/js/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.flash.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.html5.min.js"></script>
   <script src="assets/js/datatables/datatables.net-buttons/js/buttons.print.min.js"></script>
   <script src="assets/js/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
   <script src="assets/js/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
   <script src="assets/js/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
   <script src="assets/js/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"></script>


</body>

</html>